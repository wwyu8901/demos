#coding=utf-8
#!/usr/bin/env jython
import os
from dbexts import dbexts

#jdbc.ini是数据库的链接信息
dbcfg = 'D:/workspace/demos/jython-demo/src/test/resources/jdbc.ini'
#dbexts类是jython自己的
d = dbexts(cfg=dbcfg)
#在执行isql时会自动打印出查询到的数据，所有CRUD的语句都由isql()方法执行
d.isql("select * from tb_user")
d.close()
for row in d.results:
    print(row[0]) #打印出第一列的数据