package org.jython;

import org.python.util.PythonInterpreter;

public class JavaPythonFile {

    public static void main(String[] args) {
        PythonInterpreter interpreter = new PythonInterpreter();
        interpreter.execfile("D:\\workspace\\demos\\jython-demo\\src\\test\\resources\\a.py");
    }
}