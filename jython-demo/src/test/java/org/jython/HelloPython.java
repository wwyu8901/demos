package org.jython;

import org.python.core.Py;
import org.python.core.PyString;
import org.python.util.PythonInterpreter;

import org.python.core.PyFunction;
import org.python.core.PyObject;

import java.util.Date;

public class HelloPython {

    public static void main(String[] args) {

        System.out.println(new Date());

        PythonInterpreter interpreter = PythonInterpreter.threadLocalStateInterpreter(null);
        System.out.println(new Date());
        interpreter.execfile("D:\\workspace\\demos\\jython-demo\\src\\test\\resources\\hello.py");
//        interpreter.set("a",Py.newString("abc"));
        System.out.println(new Date());

        PyFunction pyFunction = interpreter.get("hello", PyFunction.class); // 第一个参数为期望获得的函数（变量）的名字，第二个参数为期望返回的对象类型
        System.out.println(new Date());
        PyString params1 = Py.newString("aaaa");
        PyString params2 = Py.newString("bbb");
        PyObject pyObject = pyFunction.__call__(params1,params2); // 调用函数
        System.out.println(new Date());


        System.out.println(pyObject.getType());
        System.out.println(new Date());

    }
}