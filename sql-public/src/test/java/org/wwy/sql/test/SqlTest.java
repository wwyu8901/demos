package org.wwy.sql.test;

import org.wwy.sql.SqlFileHandler;
import org.wwy.sql.SqlMerger;

import java.io.File;
import java.io.IOException;

public class SqlTest {

    public static void main(String[] args) throws IOException {

        File baseFile = new File("D:/workspace/xc/update");
        SqlFileHandler sqlFileHandler = new SqlFileHandler(baseFile);
        sqlFileHandler.merge("D:/workspace/xc/metadata/db/v1.0.17/all","metadata");
        sqlFileHandler.merge("D:/workspace/xc/data-quality/db/v1.0.27/all","data-quality");
        sqlFileHandler.merge("D:/workspace/xc/indicator/db/v1.0.20/all","indicator");
        sqlFileHandler.merge("D:/workspace/xc/xc-osp/db/Oracle/v1.0.32/all","osp");
        sqlFileHandler.merge("D:/workspace/xc/XHE/db/v1.0.1/all","xhe");
        sqlFileHandler.merge("D:/workspace/xc/XMG/db/v1.0.1","xmg");
        sqlFileHandler.mergeBData("D:/workspace/xc/xc-computation-engine/db/v1.3.4/all/inceptor",
                "computation-engine-inceptor-level1",
                "level1");
        sqlFileHandler.mergeBData("D:/workspace/xc/xc-computation-engine/db/v1.3.4/all/inceptor",
                "computation-engine-inceptor-level2",
                "level2");
        sqlFileHandler.mergeBDataOracle("D:/workspace/xc/xc-computation-engine/db/v1.3.4/all/oracle","computation-engine-oracle");
//        sqlFileHandler.merge("D:/workspace/xc/xc-computation-engine/db/v1.3.4/all/inceptor/ddl/level1","inceptor-level1");


    }
}
