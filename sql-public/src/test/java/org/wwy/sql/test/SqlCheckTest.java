package org.wwy.sql.test;

import org.junit.Test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SqlCheckTest {
    private static final Pattern SQL_PATTERN = Pattern.compile("(insert)[ ]+(into)[ ]+(.)+(values)", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);

    private static final Pattern SQL_PATTERN2 = Pattern.compile("[(]+(.)+[)]+", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);

    @Test
    public void test(){
        String s = "INSERT  Into  abc (*) values ('1',2,4)";
        Matcher matcher = SQL_PATTERN.matcher(s);
        if (matcher.find()) {
            System.out.println(matcher.groupCount());
            int count = matcher.groupCount();
            for(int i=0;i<count;i++){
                System.out.println(matcher.start(i+1)+"==="+matcher.end(i+1));
                System.out.println(s.substring(matcher.end(i+1)));
            }
            String substring = s.substring(matcher.end(2), matcher.end(3));
            System.out.println("==="+substring);
            Matcher matcher2 = SQL_PATTERN2.matcher(substring);
            if(!matcher2.find()){
                System.out.println("====insert语句["+s+"]不符合规范!========");
            }
        }
    }
}
