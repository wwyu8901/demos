package org.wwy.sql.test;

import org.wwy.sql.SqlFileHandler;

import java.io.File;
import java.io.IOException;

public class SqlUpdate {

    public static void main(String[] args) throws IOException {

        File baseFile = new File("D:/workspace/xc/update");
        SqlFileHandler sqlFileHandler = new SqlFileHandler(baseFile);
        /*sqlFileHandler.mergeUpdate("D:/workspace/xc/metadata/db",
                "metadata","v1.0.1","v1.0.17");
        sqlFileHandler.mergeUpdate("D:/workspace/xc/data-quality/db",
                "data-quality","V1.0.10","v1.0.26");
        sqlFileHandler.mergeUpdate("D:/workspace/xc/indicator/db",
                "indicator","v1.0.10","v1.0.13");

        sqlFileHandler.mergeUpdate("D:/workspace/xc/xc-osp/db/Oracle",
                "osp","v1.0.10","v1.0.31");*/
        sqlFileHandler.mergeUpdate("D:/workspace/xc/XHE/db",
                "XHE","v1.0.1","v1.0.1");
        sqlFileHandler.mergeUpdate("D:/workspace/xc/XMG/db",
                "XMG","v1.0.4","v1.0.36");

    }
}
