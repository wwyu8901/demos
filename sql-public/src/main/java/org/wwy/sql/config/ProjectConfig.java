package org.wwy.sql.config;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import java.util.List;

@XStreamAlias("project")
public class ProjectConfig {

    private List<ModuleConfig> moduleConfigList;

    public List<ModuleConfig> getModuleConfigList() {
        return moduleConfigList;
    }

    public void setModuleConfigList(List<ModuleConfig> moduleConfigList) {
        this.moduleConfigList = moduleConfigList;
    }
}
