package org.wwy.sql.config;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("module")
public class ModuleConfig {
    /**
     * 项目名称
     */
    private String name;

    private String version;

    /**
     * 是否存在方言目录
     */
    private boolean existsDialectDir;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isExistsDialectDir() {
        return existsDialectDir;
    }

    public void setExistsDialectDir(boolean existsDialectDir) {
        this.existsDialectDir = existsDialectDir;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}
