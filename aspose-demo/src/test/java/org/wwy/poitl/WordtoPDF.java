package org.wwy.poitl;


import com.aspose.words.License;
import com.spire.doc.Document;
import com.spire.doc.FileFormat;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

public class WordtoPDF {

    public static void main(String[] args) throws Exception {
        getLicense();
         //加载word示例文档

        Document document = new Document();

        document.loadFromFile("d:/CreateWordXDDFChart2.docx");

        //保存为PDF格式

         document.saveToFile("d:/toPDF3.pdf", FileFormat.PDF);

    }

    private static boolean getLicense() throws Exception {
        boolean result = false;
            InputStream is = new FileInputStream(new File((
                    "D:/workspace/demos/aspose-demo/src/test/resources/license.xml"
                    )));
            License aposeLic = new License();
            aposeLic.setLicense(is);
            result = true;
        return result;
    }
}