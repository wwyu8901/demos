package org.wwy.poitl;

import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.xmlbeans.XmlException;
import org.junit.Test;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTbl;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTblGrid;
import org.wwy.demo.MainStart;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.util.List;

public class ATest {

    @Test
    public void test() throws Exception {
        String filePaths="D:/ChromeCoreDownloads/易方达中债3-5年期国债指数证券投资基金_2020年02月投资月报.docx";
        XWPFDocument document = new XWPFDocument(new FileInputStream(filePaths));
        List<XWPFTable> tables = document.getTables();
        CTTbl ctTbl = tables.get(0).getCTTbl();
        CTTblGrid tblGrid = ctTbl.getTblGrid();
        BigInteger w1 = tblGrid.getGridColArray(1).getW();
        BigInteger w2 = tblGrid.getGridColArray(2).getW();
        tblGrid.getGridColArray(0).setW(new BigInteger(tables.get(0).getWidth()+"").subtract(w1).subtract(w2));
        System.out.println(tables);
        String result = "e:/test.docx";
        FileOutputStream out = new FileOutputStream(new File(result));
        document.write(out);
        out.close();

        MainStart.doc2pdf(filePaths,"e:/abcabc1.pdf");
        MainStart.doc2pdf(result,"e:/abcabc2.pdf");
    }
}
