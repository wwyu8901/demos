package org.wwy.poitl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

import com.aspose.words.*;

/**
* Word或Excel 转Pdf 帮助类
* @author lenovo
* 备注:需要引入 aspose-words-15.8.0-jdk16.jar / aspose-cells-8.5.2.jar
*/
public class PdfUtil {

    private static boolean getLicense() {
        boolean result = false;
       try {
           InputStream is = PdfUtil.class.getClassLoader().getResourceAsStream("license.xml"); // license.xml应放在..\WebRoot\WEB-INF\classes路径下
           License aposeLic = new License();
          aposeLic.setLicense(is);
          result = true;
       } catch (Exception e) {
         e.printStackTrace();
      }
     return result;
   }

   /**
  * @param wordPath 需要被转换的word全路径带文件名
  * @param pdfPath 转换之后pdf的全路径带文件名
  */
  public static void doc2pdf(String wordPath, String pdfPath) throws Exception {
     if (!getLicense()) { // 验证License 若不验证则转化出的pdf文档会有水印产生
        return;
     }
        long old = System.currentTimeMillis();
        File file = new File(pdfPath); //新建一个pdf文档
        FileOutputStream os = new FileOutputStream(file);
        Document doc = new Document(wordPath); //Address是将要被转化的word文档
      Shape chartShape = null;
      for(int i=0;i<100;i++){
          try {
              chartShape = (Shape) doc.getChild(NodeType.SHAPE, i, true);
              Chart chart = chartShape.getChart();
              ChartAxis axisY = chart.getAxisY();
              ChartSeriesCollection series = chartShape.getChart().getSeries();
              ChartSeries chartSeries = series.get(0);
              ChartDataLabelCollection dataLabels = chartSeries.getDataLabels();
              ChartDataPointCollection dataPoints = chartSeries.getDataPoints();
              System.out.println(dataLabels);
          }catch (Exception e){
              e.printStackTrace();
          }

      }

      ChartDataPointCollection dataPoints = chartShape.getChart().getSeries().get(0).getDataPoints();
      ChartDataPoint chartDataPoint = dataPoints.get(0);
      ChartMarker marker = chartDataPoint.getMarker();

      doc.save(os, com.aspose.words.SaveFormat.PDF);//全面支持DOC, DOCX, OOXML, RTF HTML, OpenDocument, PDF, EPUB, XPS, SWF 相互转换
        long now = System.currentTimeMillis();
        os.close();
        System.out.println("共耗时：" + ((now - old) / 1000.0) + "秒"); //转化用时

     }

    /**
     * word转html文本
     * @param doc 需要转换的doc文件对象
     * @return
     */
    public static void parseWord2Html(Document doc) throws Exception {
        HtmlSaveOptions saveOptions = new HtmlSaveOptions();
        saveOptions.setExportHeadersFootersMode(ExportHeadersFootersMode.NONE); // HtmlSaveOptions的其他设置信息请参考相关API
//        ByteArrayOutputStream htmlStream = new ByteArrayOutputStream();
        String htmlText = "";
        doc.save("e:/aaa.html", saveOptions);
    }


    /**
   * @param excelPath 需要被转换的excel全路径带文件名
   * @param pdfPath 转换之后pdf的全路径带文件名
   */





      public static void main(String[] args) throws Exception {

          //word 和excel 转为pdf
          String filePaths="e:/易方达中债3-5年期国债指数周报(机构)_20200228(1).docx";
          String fileName="易方达中债3-5年期国债指数周报(机构)_20200228(1)";
         String pdfPath="e:/"+fileName+".pdf";

          Document doc = new Document(filePaths); //Address是将要被转化的word文档
//          parseWord2Html(doc);
         //doc2pdf(filePaths, pdfPath);//filePaths需要转换的文件位置 pdfPath为存储位置
//         String excel2pdf="D:/logs/excelToPdf.xlsx";
//          doc2pdf(filePaths,pdfPath);
      } 
 }