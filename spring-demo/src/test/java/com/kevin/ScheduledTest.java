package com.kevin;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class ScheduledTest  {

    @Scheduled(cron="*/3 * * * * ?") //每3秒执行1次
    public void start() {
        System.out.println("每3秒执行一次");
    }



}
