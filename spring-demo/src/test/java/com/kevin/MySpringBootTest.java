package com.kevin;

import org.example.kevin.SpringDemoApplication;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.sql.DataSource;
import java.sql.*;

@SpringBootTest(classes = SpringDemoApplication.class)
public class MySpringBootTest {

    Logger logger = LoggerFactory.getLogger(MySpringBootTest.class);
    @Autowired
    private DataSource dataSource;

    @Test
    public void test(){
        Connection connection = null;
        try {
            logger.info("开始获取连接");
            connection = dataSource.getConnection();
            logger.info("获取到连接");
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement
                    .executeQuery("SELECT * from TEST_T_USER");
            logger.info("获取到resultset");
            while (resultSet.next()){
                String string = resultSet.getString(1);
//                logger.info(string);
            }
            logger.info("访问结束");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }finally {
            if(connection!=null){
                try {
                    connection.close();
                } catch (SQLException e) {

                }
            }
        }
    }
}
