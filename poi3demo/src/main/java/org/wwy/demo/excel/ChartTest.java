package org.wwy.demo.excel;

import org.jfree.chart.*;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.CategoryLabelPosition;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import org.junit.Test;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;

public class ChartTest {

    public static void main(String args[]) {
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        dataset.addValue(100, "北京", "苹果");
        dataset.addValue(200, "上海", "苹果");
        dataset.addValue(300, "广州", "苹果");
        dataset.addValue(400, "北京", "梨子");
        dataset.addValue(500, "上海", "梨子");
        dataset.addValue(600, "广州", "梨子");
        dataset.addValue(700, "北京", "葡萄");
        dataset.addValue(800, "上海", "葡萄");
        dataset.addValue(900, "广州", "葡萄");
        dataset.addValue(800, "北京", "香蕉");
        dataset.addValue(700, "上海", "香蕉");
        dataset.addValue(600, "广州", "香蕉");
        dataset.addValue(500, "北京", "荔枝");
        dataset.addValue(400, "上海", "荔枝");
        dataset.addValue(300, "广州", "荔枝");
        JFreeChart chart = ChartFactory.createLineChart("水果产量图 ", "水果", "产量",
                dataset, PlotOrientation.VERTICAL, true, true, true);
        CategoryPlot cp = chart.getCategoryPlot();
        cp.setBackgroundPaint(ChartColor.WHITE); // 背景色设置
        cp.setRangeGridlinePaint(ChartColor.GRAY); // 网格线色设置
        cp.getDomainAxis().setCategoryLabelPositions(CategoryLabelPositions.UP_45);
        CategoryAxis domainAxis = cp.getDomainAxis();
        domainAxis.setTickLabelFont(new Font("sans-serif", Font.PLAIN, 11));
        domainAxis.setLabelFont(new Font("宋体", Font.PLAIN, 12));
        ChartFrame frame=new ChartFrame ("水果产量图 ",chart,true);
        frame.setFont(new Font("宋体",Font.PLAIN,12));
         frame.pack();
         frame.setVisible(true);

        try {
            ChartUtils.saveChartAsPNG(new File("D:\\test\\LineChart.png"),
                    chart, 800, 500);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test() throws IOException {

//默认的类别数据集
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();

        int[] cols = new int[]{200,390,550,700,1200};
        String[] rows = new String[]{"2010","2011","2012","2013","2014"};
        //填充数据集
        for(int i=0;i<cols.length;i++){
            dataset.setValue(cols[i], "公司业绩", rows[i]+"");
        }

        //创建图表
        JFreeChart chart = ChartFactory.createBarChart("2010-2014年来公司业绩图", "", "", dataset, PlotOrientation.VERTICAL, true, true, false);
        chart.setBackgroundPaint(Color.white);//图表的背景色(通常为白色)

        //获得plot(此对象通常用于绘制图表区域属性)
        CategoryPlot plot = chart.getCategoryPlot();

        //Axis对象通常用于设置X轴
        CategoryAxis axis = plot.getDomainAxis();
        //axis.setCategoryLabelPositions(CategoryLabelPositions.UP_90);
		axis.setCategoryLabelPositions(CategoryLabelPositions.UP_45); //此处设置倾斜45度
        axis.setLabelFont(new Font("宋体", Font.PLAIN, 12));
                //设置图表区域属性
                plot.setBackgroundPaint(Color.LIGHT_GRAY);
        plot.setRangeGridlinePaint(Color.white);
        plot.setDomainGridlinePaint(Color.white);
        plot.setDomainGridlinesVisible(true);


        ChartUtils.saveChartAsPNG(new File("D:\\test\\LineChart.png"), chart, 700, 400);

    }


}
