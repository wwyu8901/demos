package org.wwy.demo.excel.test.grouping2;

import org.junit.Test;

import java.math.BigDecimal;
import java.util.*;
import java.util.function.BinaryOperator;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StudentTest {

    List<Student> menu = Arrays.asList(
            new Student("刘一", 721, true, Student.GradeType.THREE),
            new Student("陈二", 637, true, Student.GradeType.THREE),
            new Student("张三", 666, true, Student.GradeType.THREE),
            new Student("李四", 531, true, Student.GradeType.TWO),
            new Student("王五", 483, false, Student.GradeType.THREE),
            new Student("赵六", 367, true, Student.GradeType.THREE),
            new Student("孙七", 499, false, Student.GradeType.ONE));

    @Test
    public void testAveragingDouble() {
        Double averagingDouble = menu.stream().collect(Collectors.averagingDouble(Student::getTotalScore));
        System.out.println(averagingDouble);

        List<Student> studentList = menu.stream().collect(
                Collectors.collectingAndThen(Collectors.toList(), Collections::unmodifiableList));
        System.out.println(studentList);

    }

    @Test
    public void testCollectingAndThen() {
        String collect = menu.stream().collect(
                Collectors.collectingAndThen(
                        Collectors.averagingInt(Student::getTotalScore), a -> "The Average totalScore is->" + a)
        );
        System.out.println(collect);
    }
// The Average totalScore is->557.7142857142857

    @Test
    public void testCounting() {
        Long collect = menu.stream().collect(Collectors.counting());
        System.out.println(collect);
    }
// 7

    @Test
    public void testGroupingByFunction() {
        Map<Student.GradeType, List<Student>> collect = menu.stream()
                .collect(Collectors.groupingBy(Student::getGradeType));

        Optional.ofNullable(collect).ifPresent(System.out::println);
    }
// {TWO=[Student{name='李四', totalScore=531, local=true, gradeType=TWO}], THREE=[Student{name='刘一', totalScore=721, local=true, gradeType=THREE}, Student{name='陈二', totalScore=637, local=true, gradeType=THREE}, Student{name='张三', totalScore=666, local=true, gradeType=THREE}, Student{name='王五', totalScore=483, local=false, gradeType=THREE}, Student{name='赵六', totalScore=367, local=true, gradeType=THREE}], ONE=[Student{name='孙七', totalScore=499, local=false, gradeType=ONE}]}

    @Test
    public void testGroupingByFunctionAndCollector() {
        Map<Student.GradeType, Long> collect = menu.stream()
                .collect(Collectors.groupingBy(Student::getGradeType, Collectors.counting()));
        System.out.println(collect);

        Map<Student.GradeType, Double> collect1 = menu.stream()
                .collect(Collectors.groupingBy(Student::getGradeType, Collectors.averagingDouble(Student::getTotalScore)));
        System.out.println(collect1);
    }
// {THREE=5, ONE=1, TWO=1}

    @Test
    public void testGroupingByFunctionAndSupplierAndCollector() {
        Map<Student.GradeType, Double> map = menu.stream()
                .collect(Collectors.groupingBy(
                        Student::getGradeType,
                        TreeMap::new,
                        Collectors.averagingInt(Student::getTotalScore)));

        Optional.of(map.getClass()).ifPresent(System.out::println);
        Optional.of(map).ifPresent(System.out::println);
    }
// class java.util.TreeMap
// {ONE=499.0, TWO=531.0, THREE=574.8}

    @Test
    public void testJoining() {
        Optional.of(menu.stream().map(Student::getName).collect(Collectors.joining(",")))
                .ifPresent(System.out::println);
    }
// 刘一陈二张三李四王五赵六孙七

    @Test
    public void testJoiningWithDelimiterAndPrefixAndSuffix() {
        Optional.of(menu.stream().map(Student::getName).collect(Collectors.joining(",", "Names[", "]")))
                .ifPresent(System.out::println);
    }
// Names[刘一,陈二,张三,李四,王五,赵六,孙七]


    @Test
    public void testMapping() {
        Optional.of(menu.stream().collect(Collectors.mapping(Student::getName, Collectors.joining(","))))
                .ifPresent(System.out::println);
    }
// 刘一,陈二,张三,李四,王五,赵六,孙七

    @Test
    public void testMaxBy() {
        menu.stream().collect(Collectors.maxBy(Comparator.comparingInt(Student::getTotalScore)))
                .ifPresent(System.out::println);
    }
// Student{name='刘一', totalScore=721, local=true, gradeType=THREE}

    @Test
    public void testPartitioningByWithPredicate() {
        Map<Boolean, List<Student>> collect = menu.stream()
                .collect(Collectors.partitioningBy(Student::isLocal));
        Optional.of(collect).ifPresent(System.out::println);
    }
// {false=[Student{name='王五', totalScore=483, local=false, gradeType=THREE}, Student{name='孙七', totalScore=499, local=false, gradeType=ONE}], true=[Student{name='刘一', totalScore=721, local=true, gradeType=THREE}, Student{name='陈二', totalScore=637, local=true, gradeType=THREE}, Student{name='张三', totalScore=666, local=true, gradeType=THREE}, Student{name='李四', totalScore=531, local=true, gradeType=TWO}, Student{name='赵六', totalScore=367, local=true, gradeType=THREE}]}

    @Test
    public void testPartitioningByWithPredicateAndCollector() {
        Map<Boolean, Double> collect = menu.stream()
                .collect(Collectors.partitioningBy(
                        Student::isLocal,
                        Collectors.averagingInt(Student::getTotalScore)));
        Optional.of(collect).ifPresent(System.out::println);
    }
// {false=491.0, true=584.4}
@Test
public void testReducingBinaryOperator() {
    menu.stream().collect(Collectors
                    .reducing(BinaryOperator
                            .maxBy(Comparator
                                    .comparingInt(Student::getTotalScore))))
            .ifPresent(System.out::println);
}
// Student{name='刘一', totalScore=721, local=true, gradeType=THREE}


    @Test
    public void testReducingBinaryOperator2() {
        menu.stream().reduce(BinaryOperator
                        .maxBy(Comparator
                                .comparingInt(Student::getTotalScore)))
                .ifPresent(System.out::println);
    }
// Student{name='刘一', totalScore=721, local=true, gradeType=THREE}

    @Test
    public void testReducingBinaryOperatorAndIdentiyAndFunction() {
        Integer result = menu.stream()
                .collect(Collectors.reducing(0, Student::getTotalScore, (d1, d2) -> d1 + d2));
        System.out.println(result);
    }
// 3904

    @Test
    public void testReducingBinaryOperatorAndIdentiyAndFunction2() {
        Integer result = menu.stream()
                .map(Student::getTotalScore)
                .reduce(0, (d1, d2) -> d1 + d2);
        System.out.println(result);
    }

    @Test
    public void testSummingDouble() {
        Optional.of(menu.stream().collect(Collectors.summingDouble(Student::getTotalScore)))
                .ifPresent(System.out::println);
    }
// 3904.0
// 3904
// 3904

    /**
     * 示例：统计总分大于600的所有学生的信息放入LinkedList中
     */
    @Test
    public void testToCollection() {
        LinkedList<Student> collect = menu.stream().filter(d -> d.getTotalScore() > 600)
                .collect(Collectors.toCollection(LinkedList::new));
        System.out.println(collect);
    }
// class java.util.LinkedList
// [Student{name='刘一', totalScore=721, local=true, gradeType=THREE}, Student{name='陈二', totalScore=637, local=true, gradeType=THREE}, Student{name='张三', totalScore=666, local=true, gradeType=THREE}]

    /**
     * 示例：查出本地学生的信息并放入ArrayList中
     */
    @Test
    public void testToList() {
        List<Student> collect = menu.stream().filter(Student::isLocal).collect(Collectors.toList());
        System.out.println(collect);
    }
// class java.util.ArrayList
// [Student{name='刘一', totalScore=721, local=true, gradeType=THREE}, Student{name='陈二', totalScore=637, local=true, gradeType=THREE}, Student{name='张三', totalScore=666, local=true, gradeType=THREE}, Student{name='李四', totalScore=531, local=true, gradeType=TWO}, Student{name='赵六', totalScore=367, local=true, gradeType=THREE}]

    @Test
    public void testToList2() {
        menu.stream().filter(Student::isLocal).collect(Collectors.groupingBy(Student::getGradeType));
    }
}
