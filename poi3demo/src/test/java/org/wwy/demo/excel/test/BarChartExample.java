package org.wwy.demo.excel.test;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtils;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.StandardChartTheme;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;

import java.awt.*;
import java.io.File;
import java.io.IOException;

public class BarChartExample
{                                   
    /**       
     * 创建BarChart       
     * @author 马建新（mervin）       
     * @return 生成Chart图片的地址       
     */
    public String createBarChart()       
    {       
        String filePath = "D:\\testJfreeChart.jpg";       
        try
        {       
            //设置Chart主题       
            setChartTheme();       
      
            DefaultCategoryDataset dataset = getBarDataSet();
            JFreeChart chart = ChartFactory.createBarChart("水果产量图-复合数据", "水果",
                        "产量", dataset, PlotOrientation.VERTICAL, true, true, false);

            ChartUtils.saveChartAsJPEG(new File(filePath), chart, 400, 300);
        } catch (IOException e)       
        {       
            e.printStackTrace();       
        }       
        return filePath;       
    }       
                                         
    /**       
     * 设置ChartFactory主题，可解决乱码问题       
     */
    private void setChartTheme()       
    {       
        // 创建主题样式       
        StandardChartTheme standardChartTheme = new StandardChartTheme("CN");
        // 设置标题字体       
        standardChartTheme.setExtraLargeFont(new Font("宋书", Font.BOLD, 20));
        // 设置图例的字体       
        standardChartTheme.setRegularFont(new Font("宋书", Font.PLAIN, 15));       
        // 设置轴向的字体       
        standardChartTheme.setLargeFont(new Font("宋书", Font.PLAIN, 15));       
        // 应用主题样式       
        ChartFactory.setChartTheme(standardChartTheme);       
    }          
    /**       
     * 获得复杂（组合）BarChart的数据集       
     * @return BarChart数据集       
     */
    private DefaultCategoryDataset getBarDataSet()       
    {       
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();       
        dataset.addValue(100, "北京", "苹果");       
        dataset.addValue(100, "上海", "苹果");       
        dataset.addValue(100, "广州", "苹果");       
        dataset.addValue(200, "北京", "梨子");       
        dataset.addValue(200, "上海", "梨子");       
        dataset.addValue(200, "广州", "梨子");       
        dataset.addValue(300, "北京", "葡萄");       
        dataset.addValue(300, "上海", "葡萄");       
        dataset.addValue(300, "广州", "葡萄");       
        dataset.addValue(400, "北京", "香蕉");       
        dataset.addValue(400, "上海", "香蕉");       
        dataset.addValue(400, "广州", "香蕉");       
        dataset.addValue(500, "北京", "荔枝");       
        dataset.addValue(500, "上海", "荔枝");       
        dataset.addValue(500, "广州", "荔枝");       
        return dataset;       
    }       
                                         
    /**       
     * @param args       
     */
    public static void main(String[] args)       
    {       
        BarChartExample piechart = new BarChartExample();       
        String filePath = piechart.createBarChart();       
        System.out.println("PieChart file path : " + filePath);       
        try
        {       
            Runtime.getRuntime().exec(       
                    "rundll32 url.dll FileProtocolHandler " + filePath);       
        } catch (IOException e)
        {       
            e.printStackTrace();       
        }       
    }                                   
}