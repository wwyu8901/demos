package org.wwy.demo.excel.test.grouping;

import java.math.BigDecimal;

public class Item {

    private String name;

    private int qty;

    private BigDecimal price;

    private String itemCode;

    private String enumDescription;

    private String enumCode;

    public Item() {
    }

    /*public Item(String name, int qty, BigDecimal price) {
        this.name = name;
        this.qty = qty;
        this.price = price;
    }*/

    public Item(String itemCode, String enumDescription, String enumCode) {
        this.itemCode = itemCode;
        this.enumDescription = enumDescription;
        this.enumCode = enumCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getEnumDescription() {
        return enumDescription;
    }

    public void setEnumDescription(String enumDescription) {
        this.enumDescription = enumDescription;
    }

    public String getEnumCode() {
        return enumCode;
    }

    public void setEnumCode(String enumCode) {
        this.enumCode = enumCode;
    }

    @Override
    public String toString() {
        return "Item{" +
                "name='" + name + '\'' +
                ", qty=" + qty +
                ", price=" + price +
                '}';
    }


}