package org.wwy.demo.excel.test;

import java.awt.Color;
import java.awt.GradientPaint;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtils;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.DatasetRenderingOrder;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.LineAndShapeRenderer;
import org.jfree.chart.ui.ApplicationFrame;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;

/**
 * 日期 2007-4-20
 * @author xuquanxing
 */
public class MixChart extends ApplicationFrame
{
 public MixChart(String arg0)
 {
  super(arg0);
/*        ChartPanel chartpanel = new ChartPanel(createChart());
        chartpanel.setPreferredSize(new Dimension(500, 270));
        setContentPane(chartpanel);*/
 }

 /**
  * 
  */
 private static final long serialVersionUID = 1L;

 /**
  * 日期 2007-4-18
  * @author xuquanxing 
  * @param args void
  */
 public static void main(String[] args)
 {
        JFreeChart  freechat =createChart();
  FileOutputStream fos_jpg = null;
  try
  {
   try
   {
    fos_jpg = new FileOutputStream("D://bar.jpg");
    ChartUtils.writeChartAsJPEG(fos_jpg, 1, freechat, 400, 400, null);
   } catch (FileNotFoundException e)
   {
    e.printStackTrace();
   } catch (IOException e)
   {
    e.printStackTrace();
   }
  } finally
  {
   try
   {
    fos_jpg.close();
   } catch (Exception e)
   {
   }
  }
 }
 
 public static JFreeChart createChart()
 {
  //创建数据集
  CategoryDataset categorydataset  = createDataset1();
       //创建数据集1
        CategoryDataset categorydataset1 = createDataset2();
      
        JFreeChart jfreechart = ChartFactory.createLineChart(
                                                        "kpi",
                                                           "index", 
                                                           "concrete", 
                                                           categorydataset1,
                                                           PlotOrientation.VERTICAL, 
                                                           true,
                                                           true, 
                                                           false
                                                           );
       
        //设置图片背景色
        GradientPaint bgGP = new GradientPaint(0, 1000, Color.cyan, 0, 0,
             Color.WHITE, false);
        jfreechart.setBackgroundPaint(bgGP);
        
        CategoryPlot categoryplot3 = (CategoryPlot) jfreechart.getPlot();
  //      NumberAxis numberaxisbar = (NumberAxis) categoryplot3.getRangeAxis();
//      设置最高的一个 Item 与图片顶端的距离
 //       numberaxisbar.setUpperBound(0.15);
 //       numberaxisbar.setLowerMargin(1000.00);
  //      categoryplot3.setRangeAxis(numberaxisbar);
        //System.out.println("categoryplot3.getRangeAxis()"+categoryplot3.getRangeAxis(0).getRange().getLength());
       
        CategoryAxis categoryaxis = categoryplot3.getDomainAxis();
       // CategoryLabelPositions.DOWN_45 表示label样式 倾斜角度
        categoryaxis.setCategoryLabelPositions(CategoryLabelPositions.DOWN_45);
        //设置标签宽度
        categoryaxis.setMaximumCategoryLabelWidthRatio(20F);
        
        NumberAxis numberaxis3 = new NumberAxis("value");
        categoryplot3.setRangeAxis(1, numberaxis3);
        categoryplot3.setDataset(1, categorydataset);//设置数据集索引
        categoryplot3.mapDatasetToRangeAxis(1,1);//将该索引映射到axis 第一个参数指数据集的索引,第二个参数为坐标轴的索引
        LineAndShapeRenderer lineandshaperenderer = new LineAndShapeRenderer();
        lineandshaperenderer.setDefaultShapesFilled(true);

        //设置某坐标轴索引上数据集的显示样式
        categoryplot3.setRenderer(1, lineandshaperenderer);
        //设置两个图的前后顺序 ，DatasetRenderingOrder.FORWARD表示后面的图在前者上面 ，DatasetRenderingOrder.REVERSE表示 表示后面的图在前者后面
        categoryplot3.setDatasetRenderingOrder(DatasetRenderingOrder.FORWARD);
        return jfreechart;
 }
 
  /**
  * 日期 2007-4-20
  * @author xuquanxing 
  * @return CategoryDataset  折线的数据集
  */
 public static CategoryDataset createDataset1()
     {
         DefaultCategoryDataset defaultcategorydataset = new DefaultCategoryDataset();
         //改成具体指标
         String s  = "First";
         String s1 = "Second";
         //改为对应的时间
         String s2 = "Type 1";
         String s3 = "Type 2";
         String s4 = "Type 3";
         String s5 = "Type 4";
         String s6 = "Type 5";
         String s7 = "Type 6";
         String s8 = "Type 7";
         String s9 = "Type 8";
         defaultcategorydataset.addValue(1.0D, s,s2);
         defaultcategorydataset.addValue(4D, s, s3);
         defaultcategorydataset.addValue(3D, s, s4);
         defaultcategorydataset.addValue(5D, s, s5);
         defaultcategorydataset.addValue(5D, s, s6);
         defaultcategorydataset.addValue(7D, s, s7);
         defaultcategorydataset.addValue(7D, s, s8);
         defaultcategorydataset.addValue(8D, s, s9);
         //分别给每个指标 赋值 
         defaultcategorydataset.addValue(5D, s1, s2);
         defaultcategorydataset.addValue(7D, s1, s3);
         defaultcategorydataset.addValue(6D, s1, s4);
         defaultcategorydataset.addValue(8D, s1, s5);
         defaultcategorydataset.addValue(4D, s1, s6);
         defaultcategorydataset.addValue(4D, s1, s7);
         defaultcategorydataset.addValue(2D, s1, s8);
         defaultcategorydataset.addValue(1.0D, s1, s9);
         return defaultcategorydataset;
     }
 
  /**
  * 日期 2007-4-20
  * @author xuquanxing 
  * @return CategoryDataset 柱状数据集
  */
 public static CategoryDataset createDataset2()
     {
         DefaultCategoryDataset defaultcategorydataset = new DefaultCategoryDataset();
         //改成具体指标
         String s  = "Thrid";
         String s1 = "Fourth";
         //改为对应的时间
         String s2 = "Type 1";
         String s3 = "Type 2";
         String s4 = "Type 3";
         String s5 = "Type 4";
         String s6 = "Type 5";
         String s7 = "Type 6";
         String s8 = "Type 7";
         String s9 = "Type 8";
         defaultcategorydataset.addValue(11D, s, s2);
         defaultcategorydataset.addValue(14D, s, s3);
         defaultcategorydataset.addValue(13D, s, s4);
         defaultcategorydataset.addValue(15D, s, s5);
         defaultcategorydataset.addValue(15D, s, s6);
         defaultcategorydataset.addValue(17D, s, s7);
         defaultcategorydataset.addValue(17D, s, s8);
         defaultcategorydataset.addValue(18D, s, s9);
         defaultcategorydataset.addValue(15D, s1, s2);
         defaultcategorydataset.addValue(17D, s1, s3);
         defaultcategorydataset.addValue(16D, s1, s4);
         defaultcategorydataset.addValue(18D, s1, s5);
         defaultcategorydataset.addValue(14D, s1, s6);
         defaultcategorydataset.addValue(14D, s1, s7);
         defaultcategorydataset.addValue(12D, s1, s8);
         defaultcategorydataset.addValue(11D, s1, s9);
         return defaultcategorydataset;
     }
}