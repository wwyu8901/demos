package org.wwy.demo.excel.test;

import com.alibaba.fastjson.JSONObject;
import org.jfree.chart.*;
import org.jfree.chart.axis.*;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.category.LineAndShapeRenderer;
import org.jfree.chart.title.LegendTitle;
import org.jfree.chart.ui.RectangleEdge;
import org.jfree.chart.ui.TextAnchor;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.SeriesException;
import org.jfree.data.time.Second;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;
import org.junit.Test;

import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Html2ImageTest2 {


    @Test
    public void test2() throws IOException {
        DefaultCategoryDataset line_chart_dataset = new DefaultCategoryDataset();
        line_chart_dataset.addValue( 15 , "LineOne" , "1970-02-01" );
        line_chart_dataset.addValue( 30 , "LineOne" , "1980-02-01" );
        line_chart_dataset.addValue( 60 , "LineOne" , "1990-02-01" );
        line_chart_dataset.addValue( 120 , "LineOne" , "2000-02-01" );
        line_chart_dataset.addValue( 240 , "LineOne" , "2010-02-01" );
        line_chart_dataset.addValue( 300 , "LineOne" , "2014-02-01" );

        line_chart_dataset.addValue( 65 , "LineTwo" , "1970-02-01" );
        line_chart_dataset.addValue( 80 , "LineTwo" , "1980-02-01" );
        line_chart_dataset.addValue( 110 , "LineTwo" , "1990-02-01" );
        line_chart_dataset.addValue( 170 , "LineTwo" , "2000-02-01" );
        line_chart_dataset.addValue( 200 , "LineTwo" , "2010-02-01" );
        line_chart_dataset.addValue( 240 , "LineTwo" , "2014-02-01" );

        StandardChartTheme mChartTheme = new StandardChartTheme("CN");
        mChartTheme.setLargeFont(new Font("黑体", Font.BOLD, 20));
        mChartTheme.setExtraLargeFont(new Font("宋体", Font.PLAIN, 15));
        mChartTheme.setRegularFont(new Font("宋体", Font.PLAIN, 12));
        ChartFactory.setChartTheme(mChartTheme);
        // 使当前主题马上生效

        //设置支持中文的字体
        Font FONT = new Font("宋体", Font.PLAIN, 12);


        JFreeChart lineChartObject = ChartFactory.createLineChart(
                "图表标题","X轴标题",
                "Y轴标题",
                line_chart_dataset, PlotOrientation.VERTICAL,
                true,true,false);
        LegendTitle legend = lineChartObject.getLegend();
        legend.setPosition(RectangleEdge.TOP);
        CategoryPlot plot = lineChartObject.getCategoryPlot();
        plot.setBackgroundPaint(Color.LIGHT_GRAY); // 设置绘图区背景色
        plot.setRangeGridlinePaint(Color.WHITE); // 设置水平方向背景线颜色
        plot.setRangeGridlinesVisible(true);// 设置是否显示水平方向背景线,默认值为true
        plot.setDomainGridlinePaint(Color.WHITE); // 设置垂直方向背景线颜色
        plot.setDomainGridlinesVisible(true); // 设置是否显示垂直方向背景线,默认值为false
        LineAndShapeRenderer renderer = (LineAndShapeRenderer) plot.getRenderer();
        BasicStroke realLine = new BasicStroke(1.8f); // 设置实线
        // 设置虚线
        float dashes[] = { 5.0f };
        BasicStroke brokenLine = new BasicStroke(2.2f, // 线条粗细
                BasicStroke.CAP_ROUND, // 端点风格
                BasicStroke.JOIN_ROUND, // 折点风格
                8f, dashes, 0.6f);
        for (int i = 0; i < line_chart_dataset.getRowCount(); i++) {
            if (i % 2 == 0)
                renderer.setSeriesStroke(i, realLine); // 利用实线绘制
            else
                renderer.setSeriesStroke(i, brokenLine); // 利用虚线绘制
        }
        //ChartUtils.applyCurrentTheme(lineChartObject);
        int width = 640; /* Width of the image */
        int height = 480; /* Height of the image */
        File lineChart = new File( "D:\\LineChart.jpeg" );
        ChartUtils.saveChartAsJPEG(lineChart ,lineChartObject, width ,height);//原文出自【易百教程】，商业转载请联系作者获得授权，非商业请保留原文链接：https://www.yiibai.com/jfreechart/jfreechart_line_chart.html
    }


    @Test
    public void test4() throws IOException, InterruptedException {
        final TimeSeries series = new TimeSeries( "Random Data" );
        Second current = new Second();
        double value = 100.0;
        for ( int i = 0 ; i < 4000 ; i++ )
        {
            try
            {
                value = value + Math.random() - 0.5;
                series.add( current , new Double( value ) );
                current = ( Second ) current.next( );
            }
            catch ( SeriesException e )
            {
                System.err.println("Error adding to series" );
            }
        }
        final XYDataset dataset=( XYDataset )new TimeSeriesCollection(series);
        JFreeChart timechart = ChartFactory.createTimeSeriesChart(
                "Computing Test",
                "Seconds",
                "Value",
                dataset,
                false,
                false,
                false);

        int width = 560; /* Width of the image */
        int height = 300; /* Height of the image */
        /*CategoryAxis domainAxis = timechart.getCategoryPlot().getDomainAxis();
        domainAxis.setCategoryLabelPositions(CategoryLabelPositions.UP_45);*/
//        timechart.getXYPlot().getDomainAxis().setVerticalTickLabels(true);
        DateAxis dateAxis = new DateAxis("  ") {
            @Override
            protected List refreshTicksHorizontal(Graphics2D g2, Rectangle2D dataArea, RectangleEdge edge) {
                List ticks = super.refreshTicksHorizontal(g2, dataArea, edge);
                List<DateTick> newTicks = new ArrayList<DateTick>();
                for (Iterator it = ticks.iterator(); it.hasNext();) {
                    DateTick tick = (DateTick) it.next();
                    System.out.println(JSONObject.toJSON(tick));
                    newTicks.add(new DateTick(tick.getDate(), tick.getText(),
                            TextAnchor.TOP_RIGHT, TextAnchor.TOP_RIGHT,
                            -45));
                }
                return newTicks;
            }
        };
        XYPlot xyplot = timechart.getXYPlot();

        xyplot.setDomainAxis(dateAxis);
//        xyplot.getRangeAxis().setTickMarkOutsideLength(0);
        xyplot.setOutlineVisible(false);
        xyplot.getDomainAxis().setTickLabelFont(new Font("宋体",Font.PLAIN,21));

//        ChartFrame frame=new ChartFrame ("时序图",timechart,true);
//        frame.pack();
//        frame.setVisible(true);
        File file = new File( "D:\\TimeChart.png" );
        ChartUtils.saveChartAsJPEG( file, timechart, width, height );//原文出自【易百教程】，商业转载请联系作者获得授权，非商业请保留原文链接：https://www.yiibai.com/jfreechart/jfreechart_timeseries_chart.html
    }





//        int temp[][]={ {1,2,3,6}};
//        for(int i=0;i<temp.length;i++){
//            for(int j=0;j<temp[i].length;j++){
//                System.out.print(temp[i][j]+"\t");
//            }
//            System.out.println();
//        }
//        int dst=0;
//        int [][]temp_=new int[temp[0].length][temp.length];
//        for(int i=0;i<temp.length;i++,dst--){
//            for(int j=0;j<temp[0].length;j++){
//                int k=temp[i][j];
////                temp[i][j]=temp[j][i];
//                temp_[j][i]=k;
//            }
//        }
//        for(int i=0;i<temp_.length;i++){
//            for(int j=0;j<temp_[i].length;j++){
//                System.out.print(temp_[i][j]+"\t");
//            }
//            System.out.println();
//        }
}


