package org.wwy.demo.excel.test;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.io.IOUtils;
import org.jfree.chart.*;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.DateTick;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.labels.StandardXYItemLabelGenerator;
import org.jfree.chart.labels.XYItemLabelGenerator;
import org.jfree.chart.plot.*;
import org.jfree.chart.renderer.category.CategoryItemRenderer;
import org.jfree.chart.renderer.category.LineAndShapeRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.chart.title.LegendTitle;
import org.jfree.chart.ui.*;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.SeriesException;
import org.jfree.data.time.Day;
import org.jfree.data.time.Second;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;

public class Html2ImageTest {

    @Test
    public void test2() throws IOException {
        DefaultCategoryDataset line_chart_dataset = new DefaultCategoryDataset();
        line_chart_dataset.addValue( 15 , "LineOne" , "1970-02-01" );
        line_chart_dataset.addValue( 30 , "LineOne" , "1980-02-01" );
        line_chart_dataset.addValue( 60 , "LineOne" , "1990-02-01" );
        line_chart_dataset.addValue( 120 , "LineOne" , "2000-02-01" );
        line_chart_dataset.addValue( 240 , "LineOne" , "2010-02-01" );
        line_chart_dataset.addValue( 300 , "LineOne" , "2014-02-01" );

        line_chart_dataset.addValue( 65 , "LineTwo" , "1970-02-01" );
        line_chart_dataset.addValue( 80 , "LineTwo" , "1980-02-01" );
        line_chart_dataset.addValue( 110 , "LineTwo" , "1990-02-01" );
        line_chart_dataset.addValue( 170 , "LineTwo" , "2000-02-01" );
        line_chart_dataset.addValue( 200 , "LineTwo" , "2010-02-01" );
        line_chart_dataset.addValue( 240 , "LineTwo" , "2014-02-01" );

        StandardChartTheme mChartTheme = new StandardChartTheme("CN");
        mChartTheme.setLargeFont(new Font("黑体", Font.BOLD, 20));
        mChartTheme.setExtraLargeFont(new Font("宋体", Font.PLAIN, 15));
        mChartTheme.setRegularFont(new Font("宋体", Font.PLAIN, 12));
        ChartFactory.setChartTheme(mChartTheme);
        // 使当前主题马上生效

        //设置支持中文的字体
        Font FONT = new Font("宋体", Font.PLAIN, 12);


        JFreeChart lineChartObject = ChartFactory.createLineChart(
                "图表标题","X轴标题",
                "Y轴标题",
                line_chart_dataset, PlotOrientation.VERTICAL,
                true,true,false);
        LegendTitle legend = lineChartObject.getLegend();
        legend.setPosition(RectangleEdge.TOP);
        CategoryPlot plot = lineChartObject.getCategoryPlot();
        plot.setBackgroundPaint(Color.LIGHT_GRAY); // 设置绘图区背景色
        plot.setRangeGridlinePaint(Color.WHITE); // 设置水平方向背景线颜色
        plot.setRangeGridlinesVisible(true);// 设置是否显示水平方向背景线,默认值为true
        plot.setDomainGridlinePaint(Color.WHITE); // 设置垂直方向背景线颜色
        plot.setDomainGridlinesVisible(true); // 设置是否显示垂直方向背景线,默认值为false
        LineAndShapeRenderer renderer = (LineAndShapeRenderer) plot.getRenderer();
        BasicStroke realLine = new BasicStroke(1.8f); // 设置实线
        // 设置虚线
        float dashes[] = { 5.0f };
        BasicStroke brokenLine = new BasicStroke(2.2f, // 线条粗细
                BasicStroke.CAP_ROUND, // 端点风格
                BasicStroke.JOIN_ROUND, // 折点风格
                8f, dashes, 0.6f);
        for (int i = 0; i < line_chart_dataset.getRowCount(); i++) {
            if (i % 2 == 0)
                renderer.setSeriesStroke(i, realLine); // 利用实线绘制
            else
                renderer.setSeriesStroke(i, brokenLine); // 利用虚线绘制
        }
        //ChartUtils.applyCurrentTheme(lineChartObject);
        int width = 640; /* Width of the image */
        int height = 480; /* Height of the image */
        File lineChart = new File( "D:\\LineChart.jpeg" );
        ChartUtils.saveChartAsJPEG(lineChart ,lineChartObject, width ,height);//原文出自【易百教程】，商业转载请联系作者获得授权，非商业请保留原文链接：https://www.yiibai.com/jfreechart/jfreechart_line_chart.html
    }

    @Test
    public void test3() throws IOException {
        StandardChartTheme mChartTheme = new StandardChartTheme("CN");
        mChartTheme.setLargeFont(new Font("黑体", Font.BOLD, 10));
        mChartTheme.setExtraLargeFont(new Font("宋体", Font.PLAIN, 15));
        mChartTheme.setRegularFont(new Font("宋体", Font.PLAIN, 12));
        ChartFactory.setChartTheme(mChartTheme);
        TimeSeriesCollection dataset = new TimeSeriesCollection();
        TimeSeriesCollection dataset2 = new TimeSeriesCollection();
        TimeSeries seriesOne =new TimeSeries("上证50");
        TimeSeries seriesTwo =new TimeSeries("易方达中债3-5年期国债指数证券投资基金test");
        TimeSeries series3 =new TimeSeries("易方达中债3-5年期国债指数");
        TimeSeries series4 =new TimeSeries("易方达中债3-5年期国债指数证券投资基金");
        TimeSeries series5 =new TimeSeries("易方达中债3-5年期国债指数证券投资");
        TimeSeries series6 =new TimeSeries("易方达中债3-5年期国债");
        JSONObject jsonObject = readWeekReportData("singleTableData.json");
        JSONArray jsonArray = jsonObject.getJSONArray("上证50");
        for (Object o : jsonArray) {
            JSONObject object = (JSONObject) o;
            String xValue = object.getString("xValue");
            int year = Integer.parseInt(xValue.substring(0, 4));
            int month = Integer.parseInt(xValue.substring(4, 6));
            int d = Integer.parseInt(xValue.substring(6, xValue.length()));
            Day day = new Day(d, month, year);
            seriesOne.addOrUpdate(day,object.getDoubleValue("yValue"));
            seriesTwo.addOrUpdate(day,object.getDoubleValue("yValue")+10);
            series3.addOrUpdate(day,object.getDoubleValue("yValue")+3);
            series4.addOrUpdate(day,object.getDoubleValue("yValue")+4);
            series5.addOrUpdate(day,object.getDoubleValue("yValue")+5);
            series6.addOrUpdate(day,object.getDoubleValue("yValue")+6);
        }

        dataset.addSeries(seriesOne);
        dataset.addSeries(seriesTwo);
        dataset2.addSeries(series3);
        dataset2.addSeries(series4);
        dataset2.addSeries(series5);
        dataset2.addSeries(series6);
        JFreeChart timeSeriesChart = ChartFactory.createTimeSeriesChart(
                "", "", "Y", dataset, true,
                true, false);
        timeSeriesChart.setBorderVisible(false);
        timeSeriesChart.setBorderPaint(Color.white);
        timeSeriesChart.setBorderStroke(new BasicStroke(0f));
        LegendTitle legend = timeSeriesChart.getLegend();

        legend.setPosition(RectangleEdge.TOP);
//        legend.setPadding(5D, 15D, 5D, 25D);
//        legend.setLegendItemGraphicLocation(RectangleAnchor.BOTTOM_RIGHT);
//        legend.setLegendItemGraphicEdge(RectangleEdge.TOP);
        legend.setHorizontalAlignment(HorizontalAlignment.CENTER);
        legend.setLegendItemGraphicPadding(new RectangleInsets(5D, 50D, 5D, 0D));
        //CategoryPlot plot = timeSeriesChart.getCategoryPlot();
        XYPlot plot = timeSeriesChart.getXYPlot();
        NumberAxis numberaxis2 = new NumberAxis("Y2");
        plot.setRangeAxis(1, numberaxis2);
        plot.setDataset(1, dataset2);
        plot.mapDatasetToRangeAxis(1, 1);

        plot.setBackgroundPaint(Color.WHITE); // 设置绘图区背景色
        Color color = new Color(232,234,244);
        plot.setRangeGridlinePaint(color); // 设置水平方向背景线颜色
        plot.setRangeGridlineStroke(new BasicStroke());
        plot.setRangeGridlinesVisible(true);// 设置是否显示水平方向背景线,默认值为true
        plot.setDrawingSupplier(getSupplier());
        plot.setOutlineVisible(false);
//        Color color = new Color(232,234,244);
//        plot.setBackgroundPaint(color); // 设置绘图区背景色
//        plot.setRangeGridlineStroke(new BasicStroke());
//        plot.setRangeGridlinePaint(Color.WHITE); // 设置水平方向背景线颜色
//        plot.setRangeGridlinesVisible(true);// 设置是否显示水平方向背景线,默认值为true
//        plot.setDomainGridlinePaint(Color.WHITE); // 设置垂直方向背景线颜色
//        plot.setDomainGridlineStroke(new BasicStroke());
//        plot.setDomainGridlinesVisible(true); // 设置是否显示垂直方向背景线,默认值为false
        plot.setDatasetRenderingOrder(DatasetRenderingOrder.REVERSE);
//        ValueAxis domainAxis = plot.getDomainAxis();
        DateAxis domainAxis = new DateAxis() {
            @Override
            protected List refreshTicksHorizontal(Graphics2D g2, Rectangle2D dataArea, RectangleEdge edge) {
                List ticks = super.refreshTicksHorizontal(g2, dataArea, edge);
                List<DateTick> newTicks = new ArrayList<DateTick>();
                for (Iterator it = ticks.iterator(); it.hasNext();) {
                    DateTick tick = (DateTick) it.next();
                    newTicks.add(new DateTick(tick.getDate(), tick.getText(),
                            TextAnchor.TOP_RIGHT, TextAnchor.TOP_RIGHT,
                            -45));
                }
                return newTicks;
            }
        };
        domainAxis.setTickLabelFont(new Font("宋体",Font.PLAIN,21));
        domainAxis.setVerticalTickLabels(true);

//        domainAxis.setLabelFont(new Font("宋体", Font.PLAIN, 5));
//        domainAxis.setLabelAngle(20);
        //已知是时序图可以强转为DateAxis类型
        ((DateAxis) domainAxis).setDateFormatOverride(new SimpleDateFormat("yyyy-MM-dd"));
        // 设置距离图片左端距离
        domainAxis.setLowerMargin(0.1);
        // 设置距离图片右端距离
        domainAxis.setUpperMargin(0.1);
        plot.setDomainAxis(domainAxis);
        XYLineAndShapeRenderer renderer = (XYLineAndShapeRenderer) plot.getRenderer();
        renderer.setDefaultOutlineStroke(new BasicStroke(0f));
        renderer.setUseOutlinePaint(false);
        renderer.setDefaultOutlinePaint(Color.white);
        renderer.setDrawOutlines(false);
        BasicStroke realLine = new BasicStroke(1.8f); // 设置实线
        // 设置虚线
        float dashes[] = { 5.0f };
        BasicStroke brokenLine = new BasicStroke(2.2f, // 线条粗细
                BasicStroke.CAP_ROUND, // 端点风格
                BasicStroke.JOIN_ROUND, // 折点风格
                8f, dashes, 0.6f);
//        for (int i = 0; i < dataset.getSeriesCount(); i++) {
//            renderer.setSeriesStroke(i, new BasicStroke(1.8f));
////            if (i % 2 == 0)
////                renderer.setSeriesStroke(i, realLine); // 利用实线绘制
////            else
////                renderer.setSeriesStroke(i, brokenLine); // 利用虚线绘制
//        }
        int width = 560;  //Width of the image
        int height = 400;  //Height of the image
        File timeChart = new File( "D:\\TimeChart.png" );
        ChartUtils.saveChartAsPNG( timeChart, timeSeriesChart, width, height );
    }

    public static DefaultDrawingSupplier getSupplier(){
        return new DefaultDrawingSupplier(
                new Paint[] {
                        Color.green,
                        Color.red,
                        Color.orange,
                        Color.magenta,
                        Color.pink,
                        Color.blue,
//                        ChartColor.DARK_RED,
//                        ChartColor.DARK_GREEN,
//                        ChartColor.LIGHT_RED,
//                        ChartColor.DARK_YELLOW,
//                        ChartColor.DARK_MAGENTA,
//                        ChartColor.DARK_CYAN,
//                        ChartColor.LIGHT_RED,
//                        ChartColor.LIGHT_BLUE,
//                        ChartColor.LIGHT_GREEN,
//                        ChartColor.LIGHT_MAGENTA,
//                        ChartColor.LIGHT_CYAN
                },
                DefaultDrawingSupplier.DEFAULT_OUTLINE_PAINT_SEQUENCE,
                DefaultDrawingSupplier.DEFAULT_STROKE_SEQUENCE,
                DefaultDrawingSupplier.DEFAULT_OUTLINE_STROKE_SEQUENCE,
                DefaultDrawingSupplier.DEFAULT_SHAPE_SEQUENCE
        );
    }

    @Test
    public void test3_() throws IOException {
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        dataset.addValue(610, "GuangZhou", "2013-01");
        dataset.addValue(220, "GuangZhou", "2013-02");
        dataset.addValue(300, "GuangZhou", "2013-03");
        dataset.addValue(340, "GuangZhou", "2013-04");
        dataset.addValue(640, "GuangZhou", "2013-05");
        dataset.addValue(540, "GuangZhou", "2013-06");
        DefaultCategoryDataset dataset2 = new DefaultCategoryDataset();
        dataset2.addValue(0.1, "A", "2013-01");
        dataset2.addValue(0.2, "A", "2013-02");
        dataset2.addValue(0.3, "A", "2013-03");
        dataset2.addValue(0.2, "A", "2013-09");
        dataset2.addValue(0.6, "A", "2013-10");
        dataset2.addValue(0.4, "A", "2013-11");
        JFreeChart chart = ChartFactory.createLineChart("MEAT Sell",
                "Meat",
                "Y1",
                dataset,
                PlotOrientation.VERTICAL,
                true,//底部是否显示 GuangZhou、ShangHai 的theme
                false,
                false);
        CategoryPlot plot=chart.getCategoryPlot();
//Y2轴的设置
        NumberAxis numberaxis2 = new NumberAxis("Y2");
        plot.setRangeAxis(1, numberaxis2);
        plot.setDataset(1, dataset2);
        plot.mapDatasetToRangeAxis(1, 1);
        File timeChart = new File( "D:\\TimeChart.jpeg" );
        ChartUtils.saveChartAsJPEG(timeChart, chart,900, 600);//直接输出图片流
    }


    @Test
    public void test4() throws IOException {
        final TimeSeries series = new TimeSeries( "Random Data" );
        Second current = new Second();
        double value = 100.0;
        for ( int i = 0 ; i < 4000 ; i++ )
        {
            try
            {
                value = value + Math.random( ) - 0.5;
                series.add( current , new Double( value ) );
                current = ( Second ) current.next( );
            }
            catch ( SeriesException e )
            {
                System.err.println( "Error adding to series" );
            }
        }
        final XYDataset dataset=( XYDataset )new TimeSeriesCollection(series);
        JFreeChart timechart = ChartFactory.createTimeSeriesChart(
                "Computing Test",
                "Seconds",
                "Value",
                dataset,
                false,
                false,
                false);

        int width = 560; /* Width of the image */
        int height = 370; /* Height of the image */
        File timeChart = new File( "D:\\TimeChart.jpeg" );
        ChartUtils.saveChartAsJPEG( timeChart, timechart, width, height );//原文出自【易百教程】，商业转载请联系作者获得授权，非商业请保留原文链接：https://www.yiibai.com/jfreechart/jfreechart_timeseries_chart.html
    }

    public static JSONObject readWeekReportData(String fileName) throws IOException {
        InputStream inputStream = Html2ImageTest.class.getResourceAsStream("/templates/" + fileName);
        String content= IOUtils.toString(inputStream,"UTF-8");
        JSONObject jsonObject = JSON.parseObject(content);
        return jsonObject;
    }



    @Test
    public void test6(){
        String name = null;
        Optional<String> opt = Optional.ofNullable(name);
        System.out.println(opt.isPresent());
//        BigDecimal add = new BigDecimal(691597.675008).add(new BigDecimal(52809.257952));
//        BigDecimal divide = add.divide(new BigDecimal(10004953.4),2,BigDecimal.ROUND_HALF_UP);
//        System.out.println(divide.doubleValue());
//        List<Map<String, Object>> dataList = Lists.newArrayList();
//        Map<String, Object> map= Maps.newHashMap();
//        map.put("T_WGHT_PRC_NAV",null);
//        Map<String, Object> map1= Maps.newHashMap();
//        map1.put("T_WGHT_PRC_NAV","2");
//        dataList.add(map);
//        dataList.add(map1);
//        dataList.sort(new MapComparatorDesc());
//        dataList.size();

    }

    @Test
    public void test07(){
        Second current = new Second();
        double value = 100.0;
        for ( int i = 0 ; i < 10 ; i++ )
        {
            try
            {
                value = value + Math.random( ) - 0.5;
                current = ( Second ) current.next( );
                System.out.println(value);
                if (i==3 ) {
                   throw new RuntimeException("test");
                }
            }
            catch ( Exception e )
            {
                System.out.println( "Error adding to series" );
                continue;
            }
        }
    }

    @Test
    public void testaaa(){
        System.out.println(1.297/1.000);
    }
}


