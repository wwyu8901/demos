package org.wwy.demo.excel.test.grouping;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class Java8Example2 {

    public static void main(String[] args) {

        List<Item> items = Arrays.asList(
                new Item("apple", "desc", "1"),
                new Item("apple", "desc", "1"),
                new Item("orange", "desc", "1"),
                new Item("orange", "desc", "1"),
                new Item("orange", "desc", "1"),
                new Item("orange", "desc", "1"),
                new Item("peach", "desc", "1"),
                new Item("peach", "desc", "1"),
                new Item("peach", "desc", "1"),
                new Item("peach", "desc", "1")
        );

        // 分组，计数
        /*Map<String, Long> counting = items.stream()
                .collect(Collectors.groupingBy(Item::getName, Collectors.counting()));
        System.out.println(counting);

        // 分组，计数，数量
        Map<String, Integer> sum = items.stream()
                .collect(Collectors.groupingBy(Item::getName, Collectors.summingInt(Item::getQty)));
        System.out.println(sum);
        Collectors.summingInt(Item::getQty);*/

        Map<String, List<Item>> collect = items.stream().collect(Collectors.groupingBy(Item::getItemCode));

    }
}