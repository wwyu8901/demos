package org.wwy.demo.excel.test;
import java.awt.*;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Iterator;

import org.jfree.chart.*;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.NumberTickUnit;
import org.jfree.chart.labels.StandardXYItemLabelGenerator;
import org.jfree.chart.labels.XYItemLabelGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.CategoryItemRenderer;
import org.jfree.chart.title.LegendTitle;
import org.jfree.chart.ui.HorizontalAlignment;
import org.jfree.chart.ui.RectangleEdge;
import org.jfree.data.Range;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
public class TimeChart {
    public static void main(String[] args) {
        CategoryDataset mDataset = GetDataset();
        JFreeChart mChart = ChartFactory.createLineChart(
                "折线图",//图名字
                "年份",//横坐标
                "数量",//纵坐标
                mDataset,//数据集
                PlotOrientation.VERTICAL,
                true, // 显示图例
                true, // 采用标准生成器
                false);// 是否生成超链接
        LegendTitle legendTitle = mChart.getLegend();
        legendTitle.setHorizontalAlignment(HorizontalAlignment.RIGHT);
        legendTitle.setPosition(RectangleEdge.TOP);
        legendTitle.setItemFont(new Font("宋体",Font.PLAIN,30));

        CategoryPlot mPlot = (CategoryPlot)mChart.getPlot();
        mPlot.getDomainAxis().setTickLabelFont(new Font("宋体",Font.PLAIN,30));
        mPlot.getRangeAxis().setTickLabelFont(new Font("宋体",Font.PLAIN,30));
//        mPlot.getRangeAxis().setAutoTickUnitSelection(false);
        mPlot.setBackgroundPaint(Color.LIGHT_GRAY);
        mPlot.getRangeAxis().setRange(1,1.003);

        NumberAxis rangeAxis = (NumberAxis)mPlot.getRangeAxis();
//        rangeAxis.setTickUnit(new NumberTickUnit(0.0001));
//        mPlot.setRangeGridlinePaint(Color.BLUE);//背景底部横虚线
//        mPlot.setDomainGridlinePaint(Color.BLUE);
        mPlot.setDomainGridlineStroke(new BasicStroke());
        mPlot.setDomainGridlinesVisible(true);
        mPlot.setRangeGridlineStroke(new BasicStroke());
        mPlot.setOutlinePaint(Color.RED);//边界线

        LegendItemCollection legendItems = mPlot.getLegendItems();

        Iterator iterator = legendItems.iterator();
        while (iterator.hasNext()){
            LegendItem legendItem = (LegendItem)iterator.next();
        }
        rangeAxis.setRange(new Range(1,1.0002));
        NumberTickUnit tickUnit = rangeAxis.getTickUnit();
        DecimalFormat df = new DecimalFormat("#0.00000");
        rangeAxis.setNumberFormatOverride(df); // 数据轴数据标签的显示格式
//        new NumberTickUnit(1,);
        rangeAxis.setTickUnit(new NumberTickUnit(0.00002));
//        rangeAxis.setTickUnit(new NumberTickUnit(0.0002));
//        rangeAxis.setTickUnit(new NumberTickUnit(0.0002));

        ChartFrame mChartFrame = new ChartFrame("折线图", mChart);
        mChartFrame.pack();
        mChartFrame.setVisible(true);
 
    }
    public static CategoryDataset GetDataset()
    {
        DefaultCategoryDataset mDataset = new DefaultCategoryDataset();
        mDataset.addValue(1.0001, "First", "2013");
        mDataset.addValue(1.0002, "First", "2014");
        mDataset.addValue(1, "First", "2015");
        mDataset.addValue(1.0001, "First", "2016");
        mDataset.addValue(1.0002, "First", "2017");
        mDataset.addValue(1.0001, "First", "2018");
        mDataset.addValue(1.0002, "Second", "2013");
        mDataset.addValue(1, "Second", "2014");
        mDataset.addValue(1.0001, "Second", "2015");
        mDataset.addValue(1.0002, "Second", "2016");
        mDataset.addValue(1, "Second", "2017");
        mDataset.addValue(1.0001, "Second", "2018");
        return mDataset;
    }
}