package org.wwy.demo.excel.test.sample1;

import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.util.Units;
import org.apache.poi.xddf.usermodel.PresetLineDash;
import org.apache.poi.xddf.usermodel.XDDFLineProperties;
import org.apache.poi.xddf.usermodel.XDDFPresetLineDash;
import org.apache.poi.xddf.usermodel.chart.*;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.xwpf.usermodel.XWPFChart;
import org.apache.poi.xwpf.usermodel.XWPFDocument;

import java.io.FileOutputStream;

public class CreateWordXDDFChart3 {
 
	// Methode to set title in the data sheet without creating a Table but using the sheet data only.
	// Creating a Table is not really necessary.
	static CellReference setTitleInDataSheet(XWPFChart chart, String title, int column) throws Exception {
		XSSFWorkbook workbook = chart.getWorkbook();
		XSSFSheet sheet = workbook.getSheetAt(0);
		XSSFRow row = sheet.getRow(0);
		if (row == null)
			row = sheet.createRow(0);
		XSSFCell cell = row.getCell(column);
		if (cell == null)
			cell = row.createCell(column);
		cell.setCellValue(title);
		return new CellReference(sheet.getSheetName(), 0, column, true, true);
	}
 
	public static void main(String[] args) throws Exception {
		try (XWPFDocument document = new XWPFDocument()) {
 
			// create the data
			String[] categories = new String[] { "2019-06-17", "2019-07-01", "2019-07-15","2019-07-29" };
			Double[] valuesA = new Double[] { 1.00, 1.05, 1.055,1.04 };
			Double[] valuesB = new Double[] { 1.00, 1.045, 1.045,1.03 };
 
			// create the chart

			XWPFChart chart = document.createChart(15 * Units.EMU_PER_CENTIMETER, 10 * Units.EMU_PER_CENTIMETER);
 
			// create data sources
			int numOfPoints = categories.length;
			String categoryDataRange = chart.formatRange(new CellRangeAddress(1, numOfPoints, 0, 0));


			// create axis
			XDDFCategoryAxis bottomAxis = chart.createCategoryAxis(AxisPosition.BOTTOM);
			XDDFValueAxis leftAxis = chart.createValueAxis(AxisPosition.LEFT);
			leftAxis.setCrosses(AxisCrosses.AUTO_ZERO);

			leftAxis.setCrossBetween(AxisCrossBetween.BETWEEN);

			// create chart data
			XDDFChartData data = chart.createData(ChartTypes.BAR, bottomAxis, leftAxis);
			XDDFDataSource<String> categoriesData = XDDFDataSourcesFactory.fromArray(categories, categoryDataRange, 0);

			XDDFLineProperties line = new XDDFLineProperties();
			line.setPresetDash(new XDDFPresetLineDash(PresetLineDash.DOT));

			String valuesDataRangeA = chart.formatRange(new CellRangeAddress(1, numOfPoints, 1, 1));
			XDDFNumericalDataSource<Double> valuesDataA = XDDFDataSourcesFactory.fromArray(valuesA, valuesDataRangeA, 1);
			XDDFChartData.Series series = data.addSeries(categoriesData, valuesDataA);
			series.setLineProperties(line);

			String valuesDataRangeB = chart.formatRange(new CellRangeAddress(1, numOfPoints, 2, 2));
			XDDFNumericalDataSource<Double> valuesDataB = XDDFDataSourcesFactory.fromArray(valuesB, valuesDataRangeB, 2);

			data.setVaryColors(false);

			series.setTitle("a", setTitleInDataSheet(chart, "a", 1));
			XDDFChartData.Series series2 = data.addSeries(categoriesData, valuesDataB);

			series2.setTitle("b", setTitleInDataSheet(chart, "b", 2));
 

			chart.plot(data);
 
			// create legend
			XDDFChartLegend legend = chart.getOrAddLegend();
			legend.setPosition(LegendPosition.LEFT);
			legend.setOverlay(false);
 
			// Write the output to a file
			try (FileOutputStream fileOut = new FileOutputStream("d:/CreateWordXDDFChart3.docx")) {
				document.write(fileOut);
			}
		}
	}
}