package org.wwy.demo.excel.test;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.RegionUtil;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Test;

import java.io.*;
import java.util.List;

import static org.apache.poi.ss.usermodel.HorizontalAlignment.CENTER;

public class TmpReaderTest {

    public static void main(String[] args) throws IOException {
        String filePath = "d:/mytmp.xls";
        FileInputStream fileInputStream = new FileInputStream(new File(filePath));
        HSSFWorkbook workbook = new HSSFWorkbook(fileInputStream);
        HSSFSheet sheet = workbook.getSheetAt(0);
        System.out.println(sheet.getLastRowNum());

    }

    @Test
    public void testReadXml() throws IOException {
        String filePath = "d:/106.xlsx";
        FileInputStream fileInputStream = new FileInputStream(new File(filePath));
        XSSFWorkbook workbook = new XSSFWorkbook(fileInputStream);
        XSSFSheet sheet = workbook.getSheetAt(0);
        System.out.println(sheet.getLastRowNum());
    }

    @Test
    public void testa() throws IOException {
        String data = "{\n" +
                "\t\"name\":\"一级分类\",\n" +
                "\t\"subTitles\":[\n" +
                "\t\t{\n" +
                "\t\t\t\"name\":\"二级分类1\",\n" +
                "\t\t\t\"subTitles\":[{\n" +
                "\t\t\t\t\"name\":\"三级分类11\"\n" +
                "\t\t\t},{\n" +
                "\t\t\t\t\"name\":\"三级分类12\"\n" +
                "\t\t\t},{\n" +
                "\t\t\t\t\"name\":\"三级分类13\"\n" +
                "\t\t\t}]\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"name\":\"二级分类2\",\n" +
                "\t\t\t\"subTitles\":[{\n" +
                "\t\t\t\t\"name\":\"三级分类21\"\n" +
                "\t\t\t},{\n" +
                "\t\t\t\t\"name\":\"三级分类22\"\n" +
                "\t\t\t},{\n" +
                "\t\t\t\t\"name\":\"三级分类23\"\n" +
                "\t\t\t},{\n" +
                "\t\t\t\t\"name\":\"三级分类24\"\n" +
                "\t\t\t}]\n" +
                "\t\t},\n" +
                "\t]\n" +
                "}";
        TitleEntity headTitle = JSON.parseObject(data, new TypeReference<TitleEntity>() {
        });
        int rowStartIndex = 5;
        int columnStartIndex = 3;
        int level = 3;//级数
        int currentLevel = 1;
        int rowIndex = rowStartIndex;
        String sheetName = "sheet";
        //当前列索引
        int columnIndex = columnStartIndex;
        HSSFWorkbook workbook = new HSSFWorkbook();

        HSSFCellStyle style = workbook.createCellStyle();
        style.setAlignment(CENTER);//居中
        style.setVerticalAlignment(VerticalAlignment.CENTER);//居中
        style.setLocked(false);

        HSSFSheet sheet = workbook.createSheet(sheetName);
        HSSFRow firstRow = sheet.createRow(rowIndex);
        HSSFCell firstCell = firstRow.createCell(columnIndex);
        columnIndex++;
        firstCell.setCellStyle(style);
        firstCell.setCellValue(headTitle.getName());
        //叶子节点统计
        int leafColCount = 0;
        HSSFRow leafRow = null;
        //二级分类
        List<TitleEntity> secondTitles = headTitle.getSubTitles();
        for (TitleEntity secondTitle : secondTitles) {
            int currentLevelStart = columnIndex;
            while (currentLevel + 1 < level){
                columnIndex = currentLevelStart;//columnIndex重置
                HSSFRow row0 = sheet.createRow(rowIndex);
                HSSFCell cell_01 = row0.createCell(columnIndex);
                currentLevel++;
                //除了开始行,都要加1
                rowIndex++;
                columnIndex++;
                cell_01.setCellStyle(style);
                cell_01.setCellValue(secondTitle.getName());
            }
            if (leafRow == null) {
                leafRow = sheet.createRow(rowIndex);
            }
            List<TitleEntity> leafTitles = secondTitle.getSubTitles();
            for (int j = 0; j < leafTitles.size(); j++) {
                TitleEntity thirdTitle = leafTitles.get(j);
                if (j > 0) {
                    columnIndex++;
                }
                HSSFCell leafCell = leafRow.createCell(columnIndex);
                leafColCount++;
                HSSFCellStyle leafStyle = workbook.createCellStyle();
                leafCell.setCellStyle(leafStyle);
                String content = thirdTitle.getName();
                leafCell.setCellValue(content);
                //计算列宽
                sheet.setColumnWidth(columnIndex, (int) (content.getBytes().length * 1.1d * 256));
            }
        }
        // 合并日期占两行(4个参数，分别为起始行，结束行，起始列，结束列)
        // 行和列都是从0开始计数，且起始结束都会合并
        /*CellRangeAddress region = new CellRangeAddress(rowStartIndex, rowStartIndex + level - 2, columnStartIndex, columnStartIndex);
//        sheet.addMergedRegion(region);

        CellRangeAddress headerRegion = new CellRangeAddress(rowStartIndex, rowStartIndex + level - 2, columnStartIndex, columnIndex);
        initRegionBorder(sheet, headerRegion);*/

        File file = new File("D:\\head.xls");
        FileOutputStream fout = new FileOutputStream(file);
        workbook.write(fout);
        fout.close();
    }

    private void initRegionBorder(HSSFSheet sheet, CellRangeAddress region) {
        //先设置前景色
        for (int i = region.getFirstRow(); i <= region.getLastColumn(); i++) {
            HSSFRow row = sheet.getRow(i);
            for (int j = region.getFirstColumn(); j <= region.getLastColumn(); j++) {
                //为空很有可能被合并了
                if (row == null) {
                    continue;
                }
                HSSFCell cell = row.getCell(j);
                //为空很有可能被合并了
                if (cell == null) {
                    continue;
                }
                HSSFCellStyle cellStyle = cell.getCellStyle();
                initCellStyle(cellStyle);
            }
        }
        RegionUtil.setBorderBottom(BorderStyle.THICK, region, sheet);
        RegionUtil.setBorderLeft(BorderStyle.THICK, region, sheet);
        RegionUtil.setBorderTop(BorderStyle.THICK, region, sheet);
        RegionUtil.setBorderRight(BorderStyle.THICK, region, sheet);
    }

    private void initCellStyle(HSSFCellStyle style) {
        style.setBorderBottom(BorderStyle.MEDIUM); //下边框
        style.setBorderLeft(BorderStyle.MEDIUM);//左边框
        style.setBorderTop(BorderStyle.MEDIUM);//上边框
        style.setBorderRight(BorderStyle.MEDIUM);//右边框
        style.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
    }
}
