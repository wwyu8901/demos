package org.wwy.demo.excel.test;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 行业配置/板块配置/风格代码/券种配置
 */

public class ContrastFundAllocationVO implements Serializable {
    private static final long serialVersionUID = -5859978644281574331L;

    /**
     * 基金编码
     */
    private String secuId;
    /**
     * 基金代码
     */
    private String trdCode;
    /**
     * 基金名称
     */
    private String secuName;
    /**
     * 行业代码/板块代码/风格配置/券种代码/资产代码
     */
    private String clasCode;

    // 行业名称/板块名称/风格名称/券种名称/资产名称
    public String name;

    private String date;

    /**
     * 类型
     */
    private String clasTypCode;

    public String getSecuId() {
        return secuId;
    }

    public void setSecuId(String secuId) {
        this.secuId = secuId;
    }

    public String getTrdCode() {
        return trdCode;
    }

    public void setTrdCode(String trdCode) {
        this.trdCode = trdCode;
    }

    public String getSecuName() {
        return secuName;
    }

    public void setSecuName(String secuName) {
        this.secuName = secuName;
    }

    public String getClasCode() {
        return clasCode;
    }

    public void setClasCode(String clasCode) {
        this.clasCode = clasCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getClasTypCode() {
        return clasTypCode;
    }

    public void setClasTypCode(String clasTypCode) {
        this.clasTypCode = clasTypCode;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
