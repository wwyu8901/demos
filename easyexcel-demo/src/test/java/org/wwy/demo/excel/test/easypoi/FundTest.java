package org.wwy.demo.excel.test.easypoi;

import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import org.apache.commons.io.IOUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class FundTest {
    @Test
    public void test() throws IOException {
        export();
    }

    public void export() throws IOException {
//        从数据库中查询所有的用户
        List<FundInfo> fundInfos = exportAll();
//        定义文件名
//        处理下载中文乱码
//            使用easyPOI的生成workbook的方法
        Workbook workbook = ExcelExportUtil.exportExcel(new ExportParams("基金导出", "基金导出"), FundInfo.class, fundInfos);

        File file = new File("D:\\export.xlsx");
        FileOutputStream fout = null;
        try {
            fout = new FileOutputStream(file);
            workbook.write(fout);
        } finally {
            if (fout != null) {
                fout.close();
            }
        }

    }

    private List<FundInfo> exportAll() throws IOException {
        ClassPathResource classPathResource = new ClassPathResource("fund.json");
        String str = IOUtils.toString(classPathResource.getURI(), "UTF-8");
        ResponseData<DataObj> fundInfos = JSONObject.parseObject(str,
                new TypeReference<ResponseData<DataObj>>() {
                });
        return fundInfos.getData().getRecords();
    }
}
