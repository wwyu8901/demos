package org.wwy.demo.excel.test.graph;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class XssTest {

    @Test
    public void test() throws IOException {
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet();
        XSSFUtils.testForBarChart(sheet);
        File file = new File("D:\\aa.xlsx");
        FileOutputStream fout = null;
        try {
            fout = new FileOutputStream(file);
            workbook.write(fout);
        } finally {
            if (fout != null) {
                fout.close();
            }
        }
    }
}
