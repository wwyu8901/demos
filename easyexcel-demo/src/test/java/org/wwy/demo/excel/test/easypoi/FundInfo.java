package org.wwy.demo.excel.test.easypoi;

import cn.afterturn.easypoi.excel.annotation.Excel;

import java.math.BigDecimal;

public class FundInfo {

    @Excel(name = "证券代码")
    private String secuId;
    @Excel(name = "交易代码")
    private String trdCode;
    @Excel(name = "交易代码")
    private String secuName;

    public String getSecuId() {
        return secuId;
    }

    public void setSecuId(String secuId) {
        this.secuId = secuId;
    }

    public String getTrdCode() {
        return trdCode;
    }

    public void setTrdCode(String trdCode) {
        this.trdCode = trdCode;
    }

    public String getSecuName() {
        return secuName;
    }

    public void setSecuName(String secuName) {
        this.secuName = secuName;
    }
}
