package org.wwy.demo.excel.test.easypoi;

import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import cn.afterturn.easypoi.excel.entity.enmus.ExcelType;
import org.apache.poi.ss.usermodel.Workbook;
import org.junit.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class EasyTest {
    @Test
    public void test() throws IOException {
        export();
    }

    public void export() throws IOException {
//        从数据库中查询所有的用户
        List<User> users = exportAll();
//        定义文件名
        String filenName = "用户报表（" + new SimpleDateFormat("yyyy-MM-dd").format(new Date()) + "）.xls";
//        处理下载中文乱码

//            使用easyPOI的生成workbook的方法
            Workbook workbook = ExcelExportUtil.exportExcel(new ExportParams("用户详细信息表","用户信息"), User.class, users);

            File file = new File("D:\\easypoi.xlsx");
            FileOutputStream fout = null;
            try {
                fout = new FileOutputStream(file);
                workbook.write(fout);
            } finally {
                if (fout != null) {
                    fout.close();
                }
            }

    }

    private List<User> exportAll() {
        List<User> users = new ArrayList<>();
        User user = new User();
        user.setId("111");
        user.setMobile("13222222222");
        user.setUsername("zhangsan");
        user.setAge(11);
        user.setMoney(new BigDecimal(10002.021));
        users.add(user);
        return users;
    }

    /**
     * 导出Excel
     * @author HCY
     * @since 2020-10-14
     */
    @Test
    public void contextLoads2() throws Exception {
        //查询全部
        List<User> list = exportAll();
        //设置Excel的描述文件
        ExportParams exportParams = new ExportParams("用户列表的所有数据", "用户信息" , ExcelType.XSSF);
        exportParams.setStyle(CustomExcelExportStylerImpl.class);
        //进行导出的基本操作
        Workbook workbook = ExcelExportUtil.exportExcel(exportParams, User.class, list);
        //输入输出流地址
        FileOutputStream fileOutputStream = new FileOutputStream("d:\\users.xlsx");
        //进行输出流
        workbook.write(fileOutputStream);
        //关流
        fileOutputStream.close();
        workbook.close();
    }
}
