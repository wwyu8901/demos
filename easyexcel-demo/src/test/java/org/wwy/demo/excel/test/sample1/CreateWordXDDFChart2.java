package org.wwy.demo.excel.test.sample1;

import org.apache.poi.ooxml.POIXMLDocumentPart;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.util.Units;
import org.apache.poi.xddf.usermodel.chart.*;
import org.apache.poi.xssf.usermodel.*;
import org.apache.poi.xwpf.usermodel.XWPFChart;
import org.apache.poi.xwpf.usermodel.XWPFDocument;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

public class CreateWordXDDFChart2 {

    // Methode to set title in the data sheet without creating a Table but using the sheet data only.
    // Creating a Table is not really necessary.
    static CellReference setTitleInDataSheet(XWPFChart chart, String title, int column) throws Exception {
        XSSFWorkbook workbook = chart.getWorkbook();

        XSSFSheet sheet = workbook.getSheetAt(0);
        XSSFRow row = sheet.getRow(0);
        if (row == null)
            row = sheet.createRow(0);
        XSSFCell cell = row.getCell(column);
        if (cell == null)
            cell = row.createCell(column);
        cell.setCellValue(title);

        chart.setWorkbook(workbook);
        return new CellReference(sheet.getSheetName(), 0, column, true, true);
    }

    public static void main(String[] args) throws Exception {

        // create the data
        String[] categories = new String[]{"LangLang 1", "LangLang 2", "LangLang 3"};
        Double[] valuesA = new Double[]{10d, 20d, 30d};
        Double[] valuesB = new Double[]{15d, 25d, 35d};

        //包含了表格数据和图形区域的模板
        InputStream inputStream = new FileInputStream("D:\\workspace\\demos\\exceldemo\\src\\test\\resources\\temp1/chart1-template.docx");
        XWPFDocument templateDoc = new XWPFDocument(inputStream);
        XWPFChart tempChart = templateDoc.getCharts().get(0);

//			XWPFChart chart = document.createChart();
        XWPFDocument document = new XWPFDocument(templateDoc.getPackage());
        XWPFChart chart = document.getCharts().get(0);

        XDDFChartData data = chart.getChartSeries().get(0);
        for (int i = 0; i < data.getSeriesCount(); i++) {
            data.removeSeries(i);
        }

        // create data sources
        int numOfPoints = categories.length;
        String categoryDataRange = chart.formatRange(new CellRangeAddress(1, numOfPoints, 0, 0));
        String valuesDataRangeA = chart.formatRange(new CellRangeAddress(1, numOfPoints, 1, 1));
        String valuesDataRangeB = chart.formatRange(new CellRangeAddress(1, numOfPoints, 2, 2));
        XDDFDataSource<String> categoriesData = XDDFDataSourcesFactory.fromArray(categories, categoryDataRange, 0);
        XDDFNumericalDataSource<Double> valuesDataA = XDDFDataSourcesFactory.fromArray(valuesA, valuesDataRangeA, 1);
        XDDFNumericalDataSource<Double> valuesDataB = XDDFDataSourcesFactory.fromArray(valuesB, valuesDataRangeB, 2);

        // create axis
        XDDFCategoryAxis bottomAxis = chart.createCategoryAxis(AxisPosition.BOTTOM);
        XDDFValueAxis leftAxis = chart.createValueAxis(AxisPosition.LEFT);
        leftAxis.setCrosses(AxisCrosses.AUTO_ZERO);
        // Set AxisCrossBetween, so the left axis crosses the category axis between the categories.
        // Else first and last category is exactly on cross points and the bars are only half visible.
        leftAxis.setCrossBetween(AxisCrossBetween.BETWEEN);

        // create chart data

//			data = chart.createData(ChartTypes.BAR, bottomAxis, leftAxis);
//        ((XDDFLineChartData) data).setBarDirection(BarDirection.COL);

        // create series
        // if only one series do not vary colors for each bar
        data.setVaryColors(false);

        // XDDFChart.setSheetTitle is buggy. It creates a Table but only half way and incomplete.
        // Excel cannot opening the workbook after creatingg that incomplete Table.
        // So updating the chart data in Word is not possible.
        //series.setTitle("a", chart.setSheetTitle("a", 1));
        XDDFChartData.Series series = data.addSeries(categoriesData, valuesDataA);
        CellReference cellReference = setTitleInDataSheet(chart, "aaaaa", 1);
        series.setTitle("aaaaaa", cellReference);


 
			/*
			   // if more than one series do vary colors of the series
			   ((XDDFBarChartData)data).setVaryColors(true);
			   series = data.addSeries(categoriesData, valuesDataB);
			   //series.setTitle("b", chart.setSheetTitle("b", 2));
			   series.setTitle("b", setTitleInDataSheet(chart, "b", 2));
			*/

        // plot chart data
        chart.plot(data);

        // create legend
        XDDFChartLegend legend = chart.getOrAddLegend();
        legend.setPosition(LegendPosition.LEFT);
        legend.setOverlay(false);

        // Write the output to a file
        try (FileOutputStream fileOut = new FileOutputStream("d:/CreateWordXDDFChart2.docx")) {
            document.write(fileOut);
        }
    }
}