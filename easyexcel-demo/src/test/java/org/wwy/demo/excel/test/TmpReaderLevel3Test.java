package org.wwy.demo.excel.test;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.RegionUtil;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import static org.apache.poi.ss.usermodel.HorizontalAlignment.CENTER;

public class TmpReaderLevel3Test {

    public static void main(String[] args) throws IOException {
        String filePath = "d:/mytmp.xls";
        FileInputStream fileInputStream = new FileInputStream(new File(filePath));
        HSSFWorkbook workbook = new HSSFWorkbook(fileInputStream);
        HSSFSheet sheet = workbook.getSheetAt(0);
        System.out.println(sheet.getLastRowNum());

    }

    @Test
    public void testa() throws IOException {
        String data = "{\n" +
                "\t\"name\":\"一级分类\",\n" +
                "\t\"subTitles\":[\n" +
                "\t\t{\n" +
                "\t\t\t\"name\":\"二级分类1\",\n" +
                "\t\t\t\"subTitles\":[{\n" +
                "\t\t\t\t\"name\":\"三级分类11\"\n" +
                "\t\t\t},{\n" +
                "\t\t\t\t\"name\":\"三级分类12\"\n" +
                "\t\t\t},{\n" +
                "\t\t\t\t\"name\":\"三级分类13\"\n" +
                "\t\t\t}]\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"name\":\"二级分类2\",\n" +
                "\t\t\t\"subTitles\":[{\n" +
                "\t\t\t\t\"name\":\"三级分类21\"\n" +
                "\t\t\t},{\n" +
                "\t\t\t\t\"name\":\"三级分类22\"\n" +
                "\t\t\t},{\n" +
                "\t\t\t\t\"name\":\"三级分类23\"\n" +
                "\t\t\t},{\n" +
                "\t\t\t\t\"name\":\"三级分类24\"\n" +
                "\t\t\t}]\n" +
                "\t\t},\n" +
                "\t]\n" +
                "}";
        TitleEntity headDatas = JSON.parseObject(data, new TypeReference<TitleEntity>() {
        });
        HSSFWorkbook workbook = new HSSFWorkbook();
        String sheetName = "sheet";
        HSSFSheet sheet = workbook.createSheet(sheetName);
        int rowStartIndex = 5;
        int columnStartIndex = 3;
        writeHeader(headDatas, sheet, rowStartIndex, columnStartIndex);

        String bodyData =
                "[[\"1\",\"1-1\",\"1-2\",\"1-3\",\"1-4\",\"1-5\",\"1-6\",\"1-7\"],[\"2\",\"2-1\",\"2-2\",\"2-3\",\"2-4\",\"2-5\",\"2-6\",\"2-7\"]]";
        List<List<String>> bodyDataList = JSON.parseObject(bodyData, new TypeReference<List<List<String>>>() {
        });
        writeBody(sheet,bodyDataList,rowStartIndex+2,columnStartIndex);
        File file = new File("D:\\head.xls");
        FileOutputStream fout = null;
        try {
            fout = new FileOutputStream(file);
            workbook.write(fout);
        }finally {
            if(fout != null){
                fout.close();
            }
        }

    }

    /**
     *
     * @param headData 头数据
     * @param sheet 表单实例
     * @param rowStartIndex 开始行 从0开始
     * @param columnStartIndex 开始列 从0开始
     * @throws IOException
     */
    private void writeHeader(TitleEntity headData, HSSFSheet sheet, int rowStartIndex, int columnStartIndex) throws IOException {
        HSSFWorkbook workbook = sheet.getWorkbook();
        int level = 3;//级数
        int currentLevel = 1;

        //当前列索引
        int columnIndex = columnStartIndex;
        HSSFCellStyle style = workbook.createCellStyle();
        style.setAlignment(CENTER);//居中
        style.setVerticalAlignment(VerticalAlignment.CENTER);//居中
        style.setLocked(false);

        HSSFRow firstRow = sheet.createRow(rowStartIndex);
        HSSFCell firstLevelCell = firstRow.createCell(columnIndex);
        firstLevelCell.setCellStyle(style);
        firstLevelCell.setCellValue(headData.getName());
        //叶子节点统计
        int leafColCount = 0;
        HSSFRow leafRow = null;
        //二级分类
        List<TitleEntity> secondTitles = headData.getSubTitles();
        for (TitleEntity secondTitle : secondTitles) {
            currentLevel++;
            //记录开始索引
            columnIndex++;
            //开始列
            int firstColumn = columnIndex;
            //创建第二层节点
            HSSFCell secondCell = firstRow.createCell(columnIndex);
            secondCell.setCellStyle(style);
            secondCell.setCellValue(secondTitle.getName());
            if (leafRow == null) {
                leafRow = sheet.createRow(rowStartIndex + 1);
            }
            //第三层叶子节点
            List<TitleEntity> thirdTitles = secondTitle.getSubTitles();
            for (int j = 0; j < thirdTitles.size(); j++) {
                TitleEntity subTitle = thirdTitles.get(j);
                if (j > 0) {
                    columnIndex++;
                }
                //创建叶子单元格
                HSSFCell leafCell = leafRow.createCell(columnIndex);
                leafColCount++;
                HSSFCellStyle leafStyle = workbook.createCellStyle();
                leafCell.setCellStyle(leafStyle);
                String content = subTitle.getName();
                leafCell.setCellValue(content);
                //计算列宽
                sheet.setColumnWidth(columnIndex, (int) (content.getBytes().length * 1.1d * 256));
            }

            //合并二级分类
            CellRangeAddress region2 = new CellRangeAddress(rowStartIndex, rowStartIndex, firstColumn, columnIndex);
            sheet.addMergedRegion(region2);
        }
        //4个参数，分别为起始行，结束行，起始列，结束列
        // 行和列都是从0开始计数，且起始结束都会合并
        //合并第一级
        CellRangeAddress region = new CellRangeAddress(rowStartIndex, rowStartIndex + level - 2, columnStartIndex, columnStartIndex);
        sheet.addMergedRegion(region);

        //全区域设定样式
        CellRangeAddress headerRegion = new CellRangeAddress(rowStartIndex, rowStartIndex + level - 2, columnStartIndex, columnIndex);
        initRegionBorder(sheet, headerRegion);

    }

    private void writeBody(Sheet sheet,List<List<String>> body,int startrow,int startcol){
        for(int i=0;i<body.size();i++){
            List<String> list = body.get(i);
            // 在这个sheet页创建一行，从0开始
            Row row = sheet.createRow((short) i+startrow);
            for(int j=0;j<list.size();j++){
                // 在这一行创建一个单元格，在0号位
                String content = list.get(j);
                Cell cell = row.createCell(j+startcol);
                // 给这个单元格赋值为1
                cell.setCellValue(content);//todo 暂时认为是字符串
                CellStyle style = sheet.getWorkbook().createCellStyle();
                cell.setCellStyle(style);
                initBodyCellStyle(style);
            }

        }
    }

    /**
     *
     * @param sheet 表单
     * @param region 区域
     */
    private void initRegionBorder(HSSFSheet sheet, CellRangeAddress region) {
        for (int i = region.getFirstRow(); i <= region.getLastColumn(); i++) {
            HSSFRow row = sheet.getRow(i);
            for (int j = region.getFirstColumn(); j <= region.getLastColumn(); j++) {
                //为空很有可能被合并了
                if (row == null) {
                    continue;
                }
                HSSFCell cell = row.getCell(j);
                //为空很有可能被合并了
                if (cell == null) {
                    continue;
                }
                HSSFCellStyle cellStyle = cell.getCellStyle();
                //设置每个单元格的边框
                initCellStyle(cellStyle);
            }
        }
        //设置前后左右边框
        RegionUtil.setBorderBottom(BorderStyle.THICK, region, sheet);
        RegionUtil.setBorderLeft(BorderStyle.THICK, region, sheet);
        RegionUtil.setBorderTop(BorderStyle.THICK, region, sheet);
        RegionUtil.setBorderRight(BorderStyle.THICK, region, sheet);
    }

    /**
     * 设置单元格样式
     * @param style
     */
    private void initCellStyle(HSSFCellStyle style) {
        style.setBorderBottom(BorderStyle.MEDIUM); //下边框
        style.setBorderLeft(BorderStyle.MEDIUM);//左边框
        style.setBorderTop(BorderStyle.MEDIUM);//上边框
        style.setBorderRight(BorderStyle.MEDIUM);//右边框
        //设置前景色
        style.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
    }

    private void initBodyCellStyle(CellStyle style) {
        style.setBorderBottom(BorderStyle.THIN); //下边框
        style.setBorderLeft(BorderStyle.THIN);//左边框
        style.setBorderTop(BorderStyle.THIN);//上边框
        style.setBorderRight(BorderStyle.THIN);//右边框
    }
}
