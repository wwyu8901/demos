package org.wwy.demo.excel.test;

import java.util.List;

public class TitleEntity {
    /**
     * 标题名称
     */
    private String name;

    /**
     * 子标题
     */
    private List<TitleEntity> subTitles;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<TitleEntity> getSubTitles() {
        return subTitles;
    }

    public void setSubTitles(List<TitleEntity> subTitles) {
        this.subTitles = subTitles;
    }
}
