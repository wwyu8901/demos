package org.wwy.demo.excel.test;

import org.junit.Test;
import org.springframework.util.StringUtils;

public class CharTest {
    public static void main(String[] args) {
        String a =
                "{\n" +
                        "\t\t\t\t\"targetDate\": \"20200223\",\n" +
                        "\t\t\t\t\"value\": 16.4375,\n" +
                        "\t\t\t\t\"chg\": 0.0\n" +
                        "\t\t\t}";
        System.out.println(a.length());
        System.out.println(a.getBytes().length);
    }

    @Test
    public void test(){
        String str = "abc   ; \n";
        String s2 = rightTrimWithChar(str, '\n');
        str = "UPDATE T_DICT SET PARENT_CODE = 'MODEL258',PARENT_NAME = '业务类型（估值）',CODE = '146',DICT_NAME = '开基申购申请',DICT_VALUE = '146',ROOT_CODE = 'MODEL258',INNER_CODE = '25800153',SERIAL_NUM = NULL WHERE ID = 8923;\n";
        s2 = rightTrimWithChar(str, '\n');

        str = "abc   ; \n";
        s2 = rightTrimWithChar(str, '\n');
        System.out.println(s2);
        s2 = rightTrimWithChar("abc", '\n');
        System.out.println(s2);
        s2 = rightTrimWithChar("", '\n');
        System.out.println(s2);
        s2 = rightTrimWithChar(null, '\n');
        System.out.println(s2);

    }

    public static String rightTrimWithChar(String str, char trailingCharacter) {
        if (!hasLength(str)) {
            return str;
        }
        StringBuilder sb = new StringBuilder(str);
        while (sb.length() > 0 &&
                (sb.charAt(sb.length() - 1) == trailingCharacter
                        || sb.charAt(sb.length() - 1) == ' ')) {
            sb.deleteCharAt(sb.length() - 1);
        }
        return sb.toString();
    }

    public static boolean hasLength(String str) {
        return (str != null && !str.isEmpty());
    }
}
