package org.wwy.demo.excel.test;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.PropertyNamingStrategy;
import com.alibaba.fastjson.TypeReference;
import com.alibaba.fastjson.parser.Feature;
import com.alibaba.fastjson.serializer.SerializeConfig;
import org.junit.Test;

public class FastjsonTest {

    @Test
    public void test(){
        TBondPool tBondPool = new TBondPool();
        tBondPool.setBondName("aaaa");
        tBondPool.setBaseIssueAmount(2.332322);
        tBondPool.setCreateDate(1111L);
        String s = JSON.toJSONString(tBondPool);
        System.out.println(s);
        TBondPool tBondPool1 = JSON.parseObject(s, new TypeReference<TBondPool>() {
        });
        System.out.println(tBondPool1.getBondName());
    }


    @Test
    public void test2(){
        String s = "{\"EN_BASIC_RATE\":0.0,\"EN_FACE_PRICE\":0.0,\"EN_PUBLISH_PRICE\":0.0,\"EN_YEAR_RATE\":0.0,\"L_ISSUE_AMOUNT\":0.0,\"vcStockFullname\":\"aaaa\",\"baseIssueAmount\":2.332322,\"createDate\":1111,\"subjectSectionEnd\":0.0,\"subjectSectionStart\":0.0}\n";
        TBondPool tBondPool1 = JSON.parseObject(s, new TypeReference<TBondPool>() {
        });
        System.out.println(tBondPool1.getBondName());
    }
}
