package org.wwy.demo.excel.graph;

import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.wwy.demo.excel.Coordinate;

import java.util.LinkedHashMap;
import java.util.Map;

public class Table1GraphGeneratorImpl implements GraphGenerator{

    private GraphInstance graphInstance;

    private Integer dataStartRow;

    private Integer dataEndRow;

    private Integer xdataCol;

    private Integer ydataCol;

    //分组col
    private Integer groupCol;

    private XSSFSheet sheet;

    public Table1GraphGeneratorImpl(XSSFSheet sheet, CellRangeAddress graphRange, String title) {
        graphInstance = new GraphInstance(sheet, graphRange, title);
        this.sheet = sheet;
    }

    public void setDataStartRow(Integer dataStartRow) {
        this.dataStartRow = dataStartRow;
    }

    public void setDataEndRow(Integer dataEndRow) {
        this.dataEndRow = dataEndRow;
    }

    public void setXdataCol(Integer xdataCol) {
        this.xdataCol = xdataCol;
    }

    public void setYdataCol(Integer ydataCol) {
        this.ydataCol = ydataCol;
    }

    public void setGroupCol(Integer groupCol) {
        this.groupCol = groupCol;
    }

    @Override
    public void generate() {
        checkNull(xdataCol,"xdataCol未设置!");
        checkNull(ydataCol,"ydataCol未设置!");
        checkNull(groupCol,"groupCol未设置!");
        checkNull(dataStartRow,"dataStartRow未设置!");
        checkNull(dataEndRow,"dataEndRow未设置!");
/*        CellRangeAddress serialTitleRange = new CellRangeAddress(dataStartRow, dataEndRow, groupCol, groupCol);
        CellRangeAddress dateRange = new CellRangeAddress(dataStartRow, dataEndRow, xdataCol, xdataCol);
        CellRangeAddress valueRange = new CellRangeAddress(dataStartRow, dataEndRow, ydataCol, ydataCol);*/

        Map<String, Coordinate> dataCoordinates = new LinkedHashMap<>();
        for (int i = dataStartRow; i <= dataEndRow; i++) {
            int serialTitleColumn = groupCol;
            XSSFRow row = sheet.getRow(i);
            String desc = row.getCell(serialTitleColumn).getStringCellValue();
            if(dataCoordinates.containsKey(desc)){
                Coordinate coordinate = dataCoordinates.get(desc);
                coordinate.setEnd(i);
            }else{
                Coordinate coordinate = new Coordinate(i);
                dataCoordinates.put(desc,coordinate);
            }
        }

        for(String key:dataCoordinates.keySet()){
            Coordinate coordinate = dataCoordinates.get(key);
            graphInstance.drawLineChart(key,
                    new CellRangeAddress(coordinate.getStart(), coordinate.getEnd(),
                            ydataCol, ydataCol),
                    new CellRangeAddress(coordinate.getStart(), coordinate.getEnd(),
                            xdataCol, xdataCol));
        }
    }

    private void checkNull(Integer param,String errorMsg){
        if(param == null){
            throw new RuntimeException(errorMsg);
        }
    }
}
