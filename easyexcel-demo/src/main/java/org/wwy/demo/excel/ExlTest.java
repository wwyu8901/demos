package org.wwy.demo.excel;

import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class ExlTest {
    public static void main(String[] args) throws IOException {
        Workbook wb = new HSSFWorkbook();
        //导出到项目文件夹的workbook.xls文件
        FileOutputStream fileOut = new FileOutputStream("d:/workbook1.xls");

        try {
            Sheet sheet = wb.createSheet("new sheet");// 创建第一个sheet页 sheet1

            int startrow = 10;
            int startcol = 5;
            for(int i=0;i<10;i++){
                // 在这个sheet页创建一行，从0开始
                Row row = sheet.createRow((short) i+startrow);
                for(int j=0;j<15;j++){
                    // 在这一行创建一个单元格，在0号位
                    Cell cell = row.createCell(j+startcol);
                    // 给这个单元格赋值为1
                    cell.setCellValue(i+"-"+j+"-content");

                    RichTextString richString = new HSSFRichTextString(i+"-"+j+"-content"); //textValue是要设置大小的单元格数据
                    Font font = wb.createFont();
                    font.setColor(Font.COLOR_RED);//设置excel数据字体颜色
                    richString.applyFont(font);
                    cell.setCellValue(richString);

                }

            }

            wb.write(fileOut);
        }finally {
            fileOut.close();
        }

    }
}
