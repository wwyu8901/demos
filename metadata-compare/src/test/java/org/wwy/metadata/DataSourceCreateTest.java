package org.wwy.metadata;

import org.junit.Test;
import org.springframework.boot.jdbc.DataSourceBuilder;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;

public class DataSourceCreateTest {
    public DataSource getDataSource() {
        DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
        dataSourceBuilder.url("jdbc:oracle:thin:@//192.168.20.71:1521/orcl");
        dataSourceBuilder.username("cmsindicators");
        dataSourceBuilder.password("cmsindicators");
        return dataSourceBuilder.build();
    }

    @Test
    public void test() throws SQLException {
        System.out.println(new Date());
        DataSource dataSource = getDataSource();
        System.out.println(new Date());
        Connection connection = dataSource.getConnection();
        System.out.println(new Date());


    }
}
