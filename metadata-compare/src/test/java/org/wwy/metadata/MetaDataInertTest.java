package org.wwy.metadata;

import org.apache.commons.io.IOUtils;
import org.apache.commons.io.output.FileWriterWithEncoding;
import org.junit.Test;

import java.io.*;
import java.sql.*;
import java.util.*;

public class MetaDataInertTest {

    Map<String,String> replaceData = new LinkedHashMap();


    private Connection connection =
            DriverManager.getConnection("jdbc:oracle:thin:@//192.168.20.71:1521/orcl",
                    "cmsindicators", "cmsindicators");

    private Connection connection2 =
            DriverManager.getConnection("jdbc:oracle:thin:@//192.168.20.119:1521/orcl",
                    "cmsindicators", "cmsindicators");

    public MetaDataInertTest() throws SQLException {
    }

    @Test
    public void test() throws SQLException, IOException {
//        String sql = "select * from mid_l2_prd where prd_code in ('001932','005683')";
        String sql = "select * from mid_l2_prd where prd_code in ('510380','159804')";

        String tableName = "MID_L2_PRD";
//        String dealCol = "prd_typ";

        generator(sql, tableName, connection, connection2);

    }

    @Test
    public void testAst() throws SQLException, IOException {
//        String sql = "select * from mid_l2_prd where prd_code in ('001932','005683')";
//        String sql = "select * from mid_l2_prd_ast_dtl where prd_code in ('000505')";
//        String sql = "select * from mid_l2_prd_ast_dtl where prd_code in ('001932')";
//        String sql = "select * from mid_l2_prd_ast_dtl where prd_code in ('005683')";
//        String sql = "select * from mid_l2_prd_ast_dtl where prd_code in ('510380')";
        String sql = "select * from mid_l2_prd_ast_dtl where prd_code in ('159804')";
        sql += "and t_date = '20210120'";

        String tableName = "MID_L2_PRD_AST_DTL";
//        String dealCol = "prd_typ";
        generator(sql, tableName, connection, connection2);
    }

    @Test
    public void testAstTrdDtl() throws SQLException, IOException {
        String sql = "select * from mid_l2_prd_ast_trd_dtl";

        String tableName = "MID_L2_PRD_AST_TRD_DTL";
//        String dealCol = "prd_typ";
        replaceData.put("dm_created_time".toUpperCase(),"sysdate");
        replaceData.put("dm_updated_time".toUpperCase(),"sysdate");

        generator(sql, tableName, connection, connection2);
    }

    @Test
    public void testStk() throws SQLException, IOException {
        String sql = "select * from mid_l2_stk where secu_id in (select secu_id from mid_l2_prd_ast_dtl " +
                "where prd_code in(" +
                "'000505','001932'" +
                "," +
                "'005683','510380','159804'" +
                "))";

        String tableName = "MID_L2_STK";

        generator(sql, tableName, connection, connection2);
    }

    private void generator(String sql, String tableName, Connection connection, Connection connection2) throws SQLException, IOException {
        Map<String, List<String>> tableMetas = getTableMeta(connection, tableName);

        Map<String, List<String>> tableMetas2 = getTableMeta(connection2, tableName);

        List<String> skipColumns = checkColumn(tableMetas, tableMetas2, tableName);
        System.out.println("开始对比生成" + sql);
        Statement statement = connection2.createStatement();
        ResultSet resultSet = statement.executeQuery(sql);
        ResultSetMetaData rsMetadata = resultSet.getMetaData();
        int count = rsMetadata.getColumnCount();
        StringBuffer sqlPreBuffer = new StringBuffer("insert into ").append(tableName).append("(");
        List<String> targetInsertColumns = new ArrayList<>();
        for(int i=0;i<count;i++){
            String columnName = rsMetadata.getColumnName(i + 1);
//            System.out.println(columnName);
            if(skipColumns.contains(columnName)){
                continue;
            }
            targetInsertColumns.add(columnName);
            sqlPreBuffer.append(columnName);
            if(i+1<count){
                sqlPreBuffer.append(",");
            }
        }
        sqlPreBuffer.append(")values(");
        File file = new File("d:/sqls.txt");
        if(file.exists()){
            file.delete();
        }
        FileWriter fw=new FileWriter(file);
        BufferedWriter  bw=new BufferedWriter(fw);

        int a = 0;
        while (resultSet.next()){
            a++;
            if(a % 1000 == 0){
                System.out.println("当前"+a+"条数据");
            }
            StringBuffer sqlBuffer = new StringBuffer(sqlPreBuffer);

            int size = targetInsertColumns.size();
            for(int i=0;i<size;i++) {
                String columnName = targetInsertColumns.get(i);
                String value = resultSet.getString(columnName);
                if(replaceData.containsKey(columnName)){
                    sqlBuffer.append(replaceData.get(columnName));
                }else {
/*                if(dealCol.equalsIgnoreCase(columnName)){
                    value = "1";
                }*/
                    if (value != null) {
                        sqlBuffer.append("'");
                        sqlBuffer.append(value);
                        sqlBuffer.append("'");
                    } else {
                        sqlBuffer.append("NULL");
                    }
                }
                if(i+1<size){
                    sqlBuffer.append(",");
                }
//                sqlBuffer.append("  --").append(columnName).append("\n");
            }
            sqlBuffer.append(");");
//            System.out.print(a+++":");
//            System.out.println(sqlBuffer);
            System.out.println(sqlBuffer);
            bw.write(sqlBuffer.toString());
            bw.newLine();
        }
        bw.flush();
        bw.close();
        fw.close();
        connection.close();
        connection2.close();


        System.out.println("------------------------------------------");
    }

    private List<String> checkColumn(Map<String, List<String>> tableMetas,
                             Map<String, List<String>> tableMetas2, String tableName) {
        List<String> tableColumns = tableMetas.get(tableName);

        List<String> tableColumns2 = tableMetas2.get(tableName);
        List<String> skipColumns = new ArrayList<>();
        for (String column : tableColumns2) {
            boolean exists = tableColumns.contains(column);
            if(!exists){
                System.out.println(column+"字段不包含!");
                skipColumns.add(column);
            }
        }
        return skipColumns;
    }

    private Map<String, List<String>> getTableMeta(Connection connection,String table) throws SQLException {
        Map<String, List<String>> allTableColumns = new LinkedHashMap<>();

        try {
            ;
            DatabaseMetaData metaData = connection.getMetaData();
            ResultSet rs = metaData.getColumns(connection.getCatalog(), connection.getSchema(), "%", "%");

            while (rs.next()) {
                int columnCount = rs.getMetaData().getColumnCount();
                String tableName = rs.getString("TABLE_NAME");
                if(!tableName.equalsIgnoreCase(table) ){
                    continue;
                }
                List<String> columns = allTableColumns.get(tableName);
                if (columns == null) {
                    columns = new ArrayList<>();
                    allTableColumns.put(tableName, columns);
                } else {
                    columns = allTableColumns.get(tableName);
                }

                String columnName = rs.getString("COLUMN_NAME");
                columns.add(columnName);
            }
            return allTableColumns;
        } finally {
            /*if (connection != null) {
                connection.close();
            }*/
        }

    }

    @Test
    public void test2(){
        String sql  = "select * from mid_l2_prd where prd_code in(";
        for(int i=1;i<1100;i++){
            sql += i;
            sql += ",";
        }
        sql += "1100";
        sql += ")";
        System.out.println(sql);
    }


}
