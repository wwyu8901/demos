package com.example.inceptordemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InceptorDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(InceptorDemoApplication.class, args);
    }

}
