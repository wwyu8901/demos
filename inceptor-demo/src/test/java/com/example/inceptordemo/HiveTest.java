package com.example.inceptordemo;


//import com.xctech.framework.api.model.ResponseData;
import com.xctech.xds.client.service.ApiInfoClientService;
/*import com.xctech.xds.dto.ApiBaseResponse;
import com.xctech.xds.dto.ApiInfoQueryRequest;
import com.xctech.xds.dto.CustomPage;*/
import org.apache.hadoop.fs.FileSystem;
import org.junit.Test;

import java.sql.*;
import java.util.Iterator;
import java.util.Locale;
import java.util.ServiceLoader;

public class HiveTest {

    @Test
    public void test() throws ClassNotFoundException, SQLException {
        Class.forName("org.apache.hive.jdbc.HiveDriver");
        Connection conn = DriverManager.getConnection("jdbc:hive2://10.150.5.30:10000/default",
                "hive","hive");

        PreparedStatement ps = conn.prepareStatement("select * from mid_l2_prd");
        ResultSet resultSet = ps.executeQuery();
        ResultSetMetaData metaData = resultSet.getMetaData();
        int columnCount = metaData.getColumnCount();
        while (resultSet.next()){
            for(int i=0;i<columnCount;i++){
                if(metaData.getColumnType(i+1) != Types.VARCHAR){
                    continue;
                }
                System.out.println(metaData.getColumnType(i+1));
                System.out.print(metaData.getColumnName(i+1));
                System.out.println("===="+resultSet.getObject(i+1));
            }
            System.out.println("================================");
        }
    }

    @Test
    public void testGetTables() throws ClassNotFoundException, SQLException {
        Class.forName("org.apache.hive.jdbc.HiveDriver");
        Connection conn = DriverManager.getConnection("jdbc:hive2://192.168.20.91:10000/kbs","","");

        ResultSet resultSet = conn.getMetaData().getTables(conn.getCatalog(),conn.getSchema(),"mid_l2_prd",null);
        ResultSetMetaData metaData = resultSet.getMetaData();
        int columnCount = metaData.getColumnCount();
        while (resultSet.next()){
            for(int i=0;i<columnCount;i++){
                System.out.print(metaData.getColumnName(i+1));
                System.out.println("===="+resultSet.getObject(i+1));
            }
            System.out.println("================================");
        }
    }

    @Test
    public void testGetColumns() throws ClassNotFoundException, SQLException {
        Class.forName("org.apache.hive.jdbc.HiveDriver");
        Connection conn = DriverManager.getConnection("jdbc:hive2://192.168.20.91:10000/kbs","","");

        ResultSet resultSet = conn.getMetaData().getColumns(conn.getCatalog(),null,"mid_l2_prd","%");
        ResultSetMetaData metaData = resultSet.getMetaData();
        int columnCount = metaData.getColumnCount();
        while (resultSet.next()){
            if(resultSet.getString("type_name").toLowerCase().indexOf("varchar")<0){
                continue;
            }
            for(int i=0;i<columnCount;i++){

                System.out.print(metaData.getColumnName(i+1));
                System.out.println("===="+resultSet.getObject(i+1));
            }
            System.out.println("================================");
        }
    }

    @Test
    public void test2() throws ClassNotFoundException, SQLException {
        Class.forName("org.apache.hive.jdbc.HiveDriver");
        Connection conn = DriverManager.getConnection("jdbc:hive2://192.168.20.91:10000/default","","");

        PreparedStatement ps = conn.prepareStatement("select secu_id,secu_sht,pres_fund_mngr,iss_vol,par_val,oper_code,dur_bgn,dm_updated_time from agg.var_fund_bas_info_sup");
        ResultSet resultSet = ps.executeQuery();
        ResultSetMetaData metaData = resultSet.getMetaData();
        int columnCount = metaData.getColumnCount();
        while (resultSet.next()){
            for(int i=0;i<columnCount;i++){
                System.out.print(metaData.getColumnName(i+1));
                System.out.println("===="+metaData.getColumnType(i+1));
                System.out.println("===="+resultSet.getObject(i+1));
            }
            System.out.println("================================");
        }
        ApiInfoClientService apiInfoClientService = null;
/*        ApiInfoQueryRequest request = null;
        ResponseData<CustomPage<ApiBaseResponse>> list = apiInfoClientService.list(request);*/
    }

    @Test
    public void test4(){
        ServiceLoader<FileSystem> serviceLoader = ServiceLoader.load(FileSystem.class);
        Iterator<FileSystem> it = serviceLoader.iterator();
        while (it.hasNext()){
            FileSystem next = it.next();
            FileSystem fs = (FileSystem)next;
            System.out.println(fs.getClass());
        }
    }
}
