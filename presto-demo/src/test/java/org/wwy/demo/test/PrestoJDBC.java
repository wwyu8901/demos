package org.wwy.demo.test;

import org.junit.Test;

import java.sql.*;

public class PrestoJDBC {
   public static void main(String[] args) throws SQLException, ClassNotFoundException {
//      Class.forName("com.facebook.presto.jdbc.PrestoDriver");
      Connection connection =
              DriverManager.getConnection("jdbc:presto://192.168.0.106:8080/hive/default","hive",null);  ;
      DatabaseMetaData metaData = connection.getMetaData();
      ResultSet rs = metaData.getTables(connection.getCatalog(), connection.getSchema(), "%", null);
      while (rs.next()) {
         System.out.println(rs.getString(1));
         System.out.println(rs.getString(2));
         System.out.println(rs.getString(3));
         System.out.println("=============");
      }
      rs.close();
      connection.close();

   }

   @Test
   public void test() throws ClassNotFoundException, SQLException {
//      ClassLoader.getSystemClassLoader().loadClass("com.facebook.presto.jdbc.PrestoDriver");
      Class.forName("com.facebook.presto.jdbc.PrestoDriver");
      Connection conn =
              DriverManager.getConnection("jdbc:presto://192.168.0.106:8080/hive/default","hive",null);  ;
      PreparedStatement ps = conn.prepareStatement("select * from assets_classify_fl limit 10");
      ResultSet resultSet = ps.executeQuery();
      ResultSetMetaData metaData = resultSet.getMetaData();
      int columnCount = metaData.getColumnCount();
      while (resultSet.next()){
         for(int i=0;i<columnCount;i++){
            System.out.print(metaData.getColumnName(i+1));
            System.out.println("===="+resultSet.getObject(i+1));
         }
         System.out.println("================================");
      }
   }

}
