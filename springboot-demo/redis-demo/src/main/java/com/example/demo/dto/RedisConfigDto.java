package com.example.demo.dto;

import lombok.Data;

import java.util.TreeSet;


@Data
public class RedisConfigDto {

    private TreeSet<String> sentinels;

    private String master;

    private String pwd;

    private String cmd;
}
