//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package com.example.demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SpringUtils implements ApplicationContextAware {
    private static final Logger log = LoggerFactory.getLogger(SpringUtils.class);
    private static ApplicationContext applicationContext = null;

    public SpringUtils() {
    }

    public static Object getBean(String beanName) {
        if (applicationContext != null) {
            try {
                return applicationContext.getBean(beanName);
            } catch (Exception var2) {
                log.error("获取Bean失败！", var2.getMessage());
                return null;
            }
        } else {
            return null;
        }
    }

    public static <T> T getBean(String beanName, Class<T> requiredType) {
        T result = null;
        if (applicationContext != null) {
            try {
                result = applicationContext.getBean(beanName, requiredType);
            } catch (Exception var4) {
                log.error("获取Bean失败！", var4);
            }
        }

        return result;
    }

    public static <T> T getBean(Class<T> requiredType) {
        T result = null;
        if (applicationContext != null) {
            try {
                result = applicationContext.getBean(requiredType);
            } catch (Exception var3) {
                log.error("获取Bean失败！", var3);
            }
        }

        return result;
    }

    public static ApplicationContext getApplicationContext() {
        return applicationContext;
    }

    public void setApplicationContext(ApplicationContext context) throws BeansException {
        applicationContext = context;
    }
}
