package com.example.demo.configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.converter.HttpMessageConversionException;
import org.springframework.validation.BindException;
import org.springframework.web.HttpMediaTypeException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.NoHandlerFoundException;

/**
 * 异常处理器
 * 
 */
@RestControllerAdvice()
public class MyExceptionHandler {
	private Logger logger = LoggerFactory.getLogger(getClass());



	@ExceptionHandler(Exception.class)
	public String handleException(Exception e){
		logger.error("发生异常", e);
		return "发生异常:"+e;
	}


}
