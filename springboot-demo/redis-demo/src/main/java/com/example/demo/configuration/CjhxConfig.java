package com.example.demo.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @description: 创金配置类
 * @author Rio
 * @date 2022/8/8 13:30
 */
@Configuration("cjhxConfig")
@ConfigurationProperties(ignoreInvalidFields = true, prefix = "cjhx")
public class CjhxConfig {

    private String serviceName;
    private String sendMsgUri;
    private String sendMsgRoleId;
    private String sendMailUri;
    private String senderAddr;
    private String senderUser;
    private String senderPwd;

    public CjhxConfig() {
    }

    public String getSendMsgRoleId() {
        return sendMsgRoleId;
    }

    public void setSendMsgRoleId(String sendMsgRoleId) {
        this.sendMsgRoleId = sendMsgRoleId;
    }

    public String getSendMsgUri() {
        return sendMsgUri;
    }

    public void setSendMsgUri(String sendMsgUri) {
        this.sendMsgUri = sendMsgUri;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String spliceSendMsgUrl() {
        return "http://" + this.serviceName + this.sendMsgUri;
    }

    public String spliceSendMailUrl() {
        return "http://" + this.serviceName + this.sendMailUri;
    }

    public String getSendMailUri() {
        return sendMailUri;
    }

    public void setSendMailUri(String sendMailUri) {
        this.sendMailUri = sendMailUri;
    }

    public String getSenderAddr() {
        return senderAddr;
    }

    public void setSenderAddr(String senderAddr) {
        this.senderAddr = senderAddr;
    }

    public String getSenderUser() {
        return senderUser;
    }

    public void setSenderUser(String senderUser) {
        this.senderUser = senderUser;
    }

    public String getSenderPwd() {
        return senderPwd;
    }

    public void setSenderPwd(String senderPwd) {
        this.senderPwd = senderPwd;
    }
}
