package com.example.demo.dto;

import com.example.demo.configuration.inter.CustomShowFormat;
import com.example.demo.configuration.inter.CustomStringFormat;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.sql.Timestamp;
@Getter
@Setter
public class UserDto implements Serializable {

    @CustomStringFormat()
    private String userName;

    @CustomShowFormat()
    private BigDecimal rate;

    @CustomShowFormat()
    private Integer type;

    @CustomShowFormat()
    private int age;

    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss", timezone="GMT+8")
    private Date date;

    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss", timezone="GMT+8")
    private Timestamp timestamp;

    private String typCode;

    private String tVal;

    private List<Map<String,Object>> datas;

    @JsonSerialize(using = ToStringSerializer.class)
    private Long length = 123456L;

}
