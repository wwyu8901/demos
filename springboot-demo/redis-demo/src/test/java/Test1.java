import org.junit.Test;
import org.springframework.data.redis.connection.RedisNode;
import org.springframework.data.redis.connection.RedisSentinelConfiguration;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisSentinelPool;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Test1 {

    @Test
    public void test(){
        RedisSentinelConfiguration config = new RedisSentinelConfiguration();
        config.setMaster("");
        List<RedisNode> sentinelNode=new ArrayList<RedisNode>();
        String sentinelUrl = "xc-vone-redis1:26379,xc-vone-redis2:26379,xc-vone-redis3:26379";
        String[] splits = sentinelUrl.split(",");
        for(String sen : splits) {
            String[] arr = sen.split(":");
            sentinelNode.add(new RedisNode(arr[0],Integer.parseInt(arr[1])));
        }
        config.setSentinels(sentinelNode);
        config.setPassword("intel.com");
        config.setSentinelPassword("intel.com");
        JedisConnectionFactory connectionFactory = new JedisConnectionFactory(config);
        RedisTemplate<String, Object> redisTemplate = new RedisTemplate<>();
        redisTemplate.setConnectionFactory(connectionFactory);
        Set<String> keys = redisTemplate.keys("*");
        System.out.println(keys);
    }

    @Test
    public void test2(){
        Set<String> sentinels = new HashSet<>();
        sentinels.add("xc-vone-redis1:26379");
        sentinels.add("xc-vone-redis2:26379");
        sentinels.add("xc-vone-redis3:26379");
        JedisSentinelPool pool = new JedisSentinelPool("mymaster", sentinels,"intel.com");
// 永远返回的都是 Master 连接
        Jedis jedis = pool.getResource();
        Set<String> keys = jedis.keys("*");

        if (keys.iterator().hasNext()){
            System.out.println("result = " + keys.iterator().next());
        }
// 读取数据

    }
}
