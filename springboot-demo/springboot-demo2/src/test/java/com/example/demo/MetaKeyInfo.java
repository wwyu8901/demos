
package com.example.demo;


import java.io.Serializable;

/**
  * <pre>
  * @description: 数据表键信息实体类
  * @copyright: Copyright (c) 2022 迅策科技
  * @author: zgh    
  * @version: 1.0 
  * @date: 2022-3-24 
  * @time: 14:51:07
  * </pre>
  */
public class MetaKeyInfo implements Serializable {
    
    private static final long serialVersionUID = 1L;
    

    /**
     * 字段名称：键类型(P,U,F)
     * 
     * 数据库字段信息:TYPE VARCHAR2(4)
     */
    private String type;


    private String name;
    /**
     * 字段名称：数据表id
     * 
     * 数据库字段信息:TABLE_ID NUMBER(20)
     */
    private Long tableId;

    /**
     * 字段名称：键名称
     * 
     * 数据库字段信息:NAME VARCHAR2(128)
     */

    /**
     * 字段名称：字段(多个用逗号隔开)
     * 
     * 数据库字段信息:COLUMNS VARCHAR2(256)
     */
    private String columns;

    private String tableName;

    private String datasourceName;

    //库中是否已定义 0没有 1已定义
    private String exsit;

    //0新增 1更新
    private String operate;

    private String updateDateV1;


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getTableId() {
        return tableId;
    }

    public void setTableId(Long tableId) {
        this.tableId = tableId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColumns() {
        return columns;
    }

    public void setColumns(String columns) {
        this.columns = columns;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getDatasourceName() {
        return datasourceName;
    }

    public void setDatasourceName(String datasourceName) {
        this.datasourceName = datasourceName;
    }

    public String getExsit() {
        return exsit;
    }

    public void setExsit(String exsit) {
        this.exsit = exsit;
    }

    public String getOperate() {
        return operate;
    }

    public void setOperate(String operate) {
        this.operate = operate;
    }

    public String getUpdateDateV1() {
        return updateDateV1;
    }

    public void setUpdateDateV1(String updateDateV1) {
        this.updateDateV1 = updateDateV1;
    }


}