package com.example.demo.filter;

import java.util.ArrayList;
import java.util.List;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;


/**
 * @description: Properties for LogUtilsProperties
 * @copyright: Copyright (c) 2021 迅策科技
 * @author: chasel
 * @version: 1.0 
 * @date: 2021年4月6日 
 * @time: 下午3:17:47
 */
@Component
@ConfigurationProperties(LogUtilsProperties.PREFIX)
public class LogUtilsProperties {

    /**
     * Prefix for the Inet Utils properties.
     */
    public static final String PREFIX = "logging.custom";

    /**
     * 单次最大响应日志大小, 单位M
     */
    public static final Integer RESPONSE_LOG_MAX_SIZE = 50;

    /**
     * 单次响应日志大小, 单位M
     */
    private Integer responseLogSize = 10;

    private List<String> excludePaths = new ArrayList<>();

    public List<String> getExcludePaths() {
        return excludePaths;
    }

    public void setExcludePaths(List<String> excludePaths) {
        this.excludePaths = excludePaths;
    }

    public Integer getResponseLogSize() {
        return responseLogSize;
    }

    public void setResponseLogSize(Integer responseLogSize) {
        if (responseLogSize == null) {
            responseLogSize = 10;
        } else {
            if (responseLogSize > RESPONSE_LOG_MAX_SIZE) {
                responseLogSize = RESPONSE_LOG_MAX_SIZE;
            }
        }
        this.responseLogSize = responseLogSize;
    }

}