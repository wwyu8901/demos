package com.example.demo.filter;

import org.springframework.util.CollectionUtils;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @description: 请求过滤工具类
 * @copyright: Copyright (c) 2020 迅策科技
 * @author: chasel
 * @version: 1.0 
 * @date: 2020年8月21日 
 * @time: 下午3:14:47
 */
public class TraceFilterUtils {

    /**
     * 请求方法Get
     */
    public final static String METHOD_GET = "GET";
    
    /**
     * 请求方法POST
     */
    public final static String METHOD_POST = "POST";
    
    /**
     * 请求头类型
     */
    public final static String CONTENT_TYPE = "Content-Type";

    /**
     * 响应内容过大
     */
    public final static String RESPONSE_LARGE = "response content is too large";
    
    /**
     * 请求时可以接受的日志输出的请求头 
     */
    public static Set<String> requestAcceptContentTypeSet = new HashSet<>();
    
    /**
     * 响应时可以接受的日志输出的请求头
     */
    public static Set<String> responseAcceptContentTypeSet = new HashSet<>();
    
    
    static {      
        if (CollectionUtils.isEmpty(requestAcceptContentTypeSet)) {
            requestAcceptContentTypeSet.add("application/x-www-form-urlencoded");
            requestAcceptContentTypeSet.add("application/json");
            requestAcceptContentTypeSet.add("text/xml");
        }

        if (CollectionUtils.isEmpty(responseAcceptContentTypeSet)) {
            responseAcceptContentTypeSet.add("text/xml");
            responseAcceptContentTypeSet.add("text/plain");
            responseAcceptContentTypeSet.add("application/json");
            responseAcceptContentTypeSet.add("application/vnd.spring-boot.actuator.v2+json");
        }
    }
    
    /**
     * 判断是否打印Request请求日志
     * 
     * 当Request时,  只打印请求头为application/x-www-form-urlencoded,application/json,text/xml
     * 
     * @param request
     * @return
     */
    public static boolean shouldPrintRequestLog(HttpServletRequest request) {
        if (request != null) { // Request
            if (METHOD_GET.equalsIgnoreCase(request.getMethod())) {
                return true;
            }

            String contentType = formatContentType(request.getHeader(CONTENT_TYPE));
            if (requestAcceptContentTypeSet.contains(contentType)) {
                return true;
            }
        }      
        return false;
    }
    
    /**
     * 
     * 判断是否打印Response请求日志
     * 
     * 当Response时, 只打印请求头为text/xml,text/plain,application/json
     * @param response
     * @return
     */
    public static boolean shouldPrintResponseLog(HttpServletResponse response) {
        if (response != null) { // Response
            String contentType = formatContentType(response.getContentType());
            if (responseAcceptContentTypeSet.contains(contentType)) {
                return true;
            }
        }       
        return false;
    }
    
    
    /**
     * 格式化处理ContentType
     * 
     * @param contentType
     * @return
     */
    public static String formatContentType(String contentType){
        contentType = Objects.toString(contentType, "").toLowerCase();
        if (contentType.indexOf(";") > -1){
            contentType = contentType.split(";")[0];
        }
        return contentType;
    }
}