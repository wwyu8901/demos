package com.example.demo.controller;

import org.springframework.http.MediaType;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.Map;

public class HttpResponseTypes {
    private static Map<String,String> HTTP_RESPONSE_TYPES = new HashMap<>();

    private static final String DEFAULT_CODE = "json";

    public static final String FORM_CODE = "x-www-form-urlencoded";

    static {
        HTTP_RESPONSE_TYPES.put("json", MediaType.APPLICATION_JSON_UTF8_VALUE);
        HTTP_RESPONSE_TYPES.put(FORM_CODE,MediaType.APPLICATION_FORM_URLENCODED_VALUE);
    }

    public static String getMediaType(String code){
        if(StringUtils.isEmpty(code)){
           code = DEFAULT_CODE;
        }        String s = HTTP_RESPONSE_TYPES.get(code.toLowerCase());
        if(s==null){
            s =  HTTP_RESPONSE_TYPES.get(DEFAULT_CODE);
        }
        return s;
    }

}
