package com.example.demo.controller;

import com.example.demo.MathGame;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.TimeUnit;

@RestController
public class GameController {
    private Thread thread;


    @GetMapping("/game/run")
    public String game() throws InterruptedException {
        if(thread!=null){
            return thread.getName()+"状态:"+thread.getState();
        }
        thread = new Thread(() -> {
            try {
                MathGame game = new MathGame();
                while (true) {
                    /*16*/
                    game.run();
                    TimeUnit.SECONDS.sleep(1L);
                }
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        });
        thread.start();
        return "已开启";
    }

    @GetMapping("/game/stop")
    public String gameStop(){
        if(thread != null){
            thread.stop();
            return "停止完成";
        }
        return "没有运行得线程";
    }
}
