package com.example.demo.filter;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

/**
 * @description: TraceServletResponseWrapper
 * @copyright: Copyright (c) 2019 迅策科技
 * @author: chasel
 * @version: 1.0 
 * @date: 2019年2月1日 
 * @time: 下午12:27:53
 */
public class TraceServletResponseWrapper extends HttpServletResponseWrapper {
   
    private TraceServletOutputStream traceOutputStream;

    public TraceServletResponseWrapper(HttpServletResponse response) {
        super(response); 
    }

    @Override
    public PrintWriter getWriter() throws IOException {
        return super.getWriter();
    }

    @Override
    public ServletOutputStream getOutputStream() throws IOException {
        // 拦截响应流,获取响应body
        if (null == traceOutputStream) {
            traceOutputStream = new TraceServletOutputStream(super.getOutputStream());
        }
        return traceOutputStream;
    }

    /**
     * @return the traceOutputStream
     */
    public TraceServletOutputStream getTraceOutputStream() {
        return traceOutputStream;
    }

}