package com.example.demo.config;

import lombok.extern.java.Log;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

@ControllerAdvice
@Log
public class GlobalDataBinder {
    @ModelAttribute
    public void globalAttributes(Model model){
        log.info(model.toString());
    }
}
