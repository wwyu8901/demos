package com.example.demo.controller;

import com.example.demo.configuration.CjhxConfig;
import com.example.demo.configuration.impl.ShowRuleUtil;
import com.example.demo.dto.UserDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.*;

@RestController
@RequestMapping("/user")
public class UserController {

    private Logger logger = LoggerFactory.getLogger(UserController.class);
    private ThreadLocal<String> fundTypeStrLocal = new ThreadLocal<>();

    @Autowired
    private CjhxConfig cjhxConfig;

    @RequestMapping("list")
    public List<UserDto> getAllUser(
            @RequestParam(value = "overwrite", required = false,defaultValue = "0") int overwrite,
            @RequestParam(required = false) Integer socketTimeout
    ) throws InterruptedException {
        logger.info("fundTypeStrLocal="+fundTypeStrLocal.get());
        String senderUser = cjhxConfig.getSenderUser();
        logger.info(senderUser);
        if(fundTypeStrLocal.get() == null){
            fundTypeStrLocal.set("aaaaaa");
        }
        List<UserDto> userDtos = new ArrayList<>();
        UserDto userDto = new UserDto();
//        userDto.setUserName("张三");
        userDto.setRate(new BigDecimal("20.023456"));
        userDto.setDate(new Date());
        userDto.setTypCode("STK");
        userDto.setTimestamp(new Timestamp(System.currentTimeMillis()));
        userDto.setType(3);
        userDto.setTVal("ddddddd");
        userDtos.add(userDto);

        UserDto userDto2 = new UserDto();
        userDto2.setUserName("李四");
        userDto2.setRate(new BigDecimal("120.0256"));
        userDto2.setDate(new Date());
        userDto2.setType(null);
//        userDto2.setTypCode("BOND");
//        userDto2.setType(new BigDecimal(4.444));
        userDtos.add(userDto2);
        List<Map<String, Object>> datas = new ArrayList<>();
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("abc",7);
        map.put("typCode","STK");
        map.put("userName","ddd");
        map.put("rate","1222");
        map.put("abcd",null);
        datas.add(map);

        Map<String, Object> map1 = new LinkedHashMap<>();
        map1.put("abc",7);
        map1.put("typCode","BOND");
        map1.put("userName","ddd");
        map1.put("rate",20.22);
        map1.put("abcd","22");
        datas.add(map1);

        Map<String, Object> map2 = new LinkedHashMap<>();
        map2.put("abc",7);
        map2.put("typCode","STK2");
        map2.put("userName","ddd");
        map2.put("rate",20.22);
        map2.put("abcd","22");
        datas.add(map2);

        ShowRuleUtil.handle(datas,"STK");
        userDto2.setDatas(datas);
        for(int i = 0;i<2;i++){
            UserDto userDto3 = new UserDto();
            userDto3.setUserName("李四"+i);
            userDto3.setRate(new BigDecimal("120.0256"));
            userDto3.setDate(new Date());
            userDto3.setTypCode("aaa");
            userDtos.add(userDto3);
        }
//        userDtos.add(userDto2);
        logger.info("aaaaa");
        logger.warn("bbbbb");
        if(socketTimeout != null){
            Thread.sleep(socketTimeout);
        }
        return userDtos;
    }
}
