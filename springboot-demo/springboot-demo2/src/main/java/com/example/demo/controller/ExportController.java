package com.example.demo.controller;

import com.example.demo.excel.FinInsCodeMppg;
import com.example.demo.excel.StdTable;
import com.xctech.xcexcel.config.TableConfig;
import jdk.nashorn.internal.ir.debug.ObjectSizeCalculator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
public class ExportController {
    Logger logger = LoggerFactory.getLogger(ExportController.class);
    @GetMapping("/export")
    public void exportExl(Integer count) throws IOException {
        StdTable<FinInsCodeMppg> stdTable = new StdTable<>(FinInsCodeMppg.class);
        TableConfig tableConfig = new TableConfig();
        tableConfig.setNullDisplayChar("");
        stdTable.setTableConfig(tableConfig);
        stdTable.setExcelCellValue(null);

        String fileName = "机构映射维护";
        List<FinInsCodeMppg> exportList = new ArrayList<>();
        if(count == null){
            count = 1000000;
        }
        for(int i=0;i<count;i++){
            FinInsCodeMppg finInsCodeMppg = new FinInsCodeMppg();
            finInsCodeMppg.setSrcInsId(10000278+i+"");
            finInsCodeMppg.setSrcInsName("武汉长江通信产业集团股份有限公司");
            finInsCodeMppg.setTgtInsId(10000278+i+"");
            finInsCodeMppg.setDataStat("0");
            finInsCodeMppg.setSrcTabCol(null);
            finInsCodeMppg.setSysRsId("长江通信");
            finInsCodeMppg.setBusiDate(20230506);
            finInsCodeMppg.setDmCreatedTime(new Date());
            finInsCodeMppg.setTgtInsName("福建发展高速公路股份有限公司");
            exportList.add(finInsCodeMppg);
        }

        logger.info("开始处理");
        OutputStream outputStream = null;
        try {
            outputStream = new FileOutputStream(new File("d:/" + fileName + ".xlsx"));
            stdTable.export(outputStream, exportList);
            logger.info("对象大小：{}",ObjectSizeCalculator.getObjectSize(outputStream));
        }finally {
            if(outputStream !=null){
                outputStream.close();
            }
        }
        logger.info("导出成功");
    }
}
