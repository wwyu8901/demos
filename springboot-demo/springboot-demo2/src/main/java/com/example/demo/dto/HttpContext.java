package com.example.demo.dto;

import lombok.Data;

import java.util.Map;

@Data
public class HttpContext {

    private HttpInfo httpInfo;

    private Map<String, ?> params;
}
