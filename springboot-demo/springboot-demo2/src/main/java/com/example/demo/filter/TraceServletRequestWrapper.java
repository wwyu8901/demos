package com.example.demo.filter;

import java.io.IOException;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

/**
 * @description: TraceServletRequestWrapper
 * @copyright: Copyright (c) 2019 迅策科技
 * @author: chasel
 * @version: 1.0 
 * @date: 2019年2月1日 
 * @time: 下午12:28:07
 */
public class TraceServletRequestWrapper extends HttpServletRequestWrapper {

    TraceServletInputStream traceInputStream;

    public TraceServletRequestWrapper(HttpServletRequest request) {
        super(request);
    }

    @Override
    public ServletInputStream getInputStream() throws IOException {
        if (traceInputStream == null) {
            traceInputStream = new TraceServletInputStream(super.getInputStream());
        }
        return traceInputStream;
    }

    public TraceServletInputStream getTraceInputStream() {
        return traceInputStream;
    }
}