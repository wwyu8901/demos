package com.example.demo.filter;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


/**
 * @description: WebAutoConfiguration
 * @copyright: Copyright (c) 2019 迅策科技
 * @author: chasel
 * @version: 1.0 
 * @date: 2019年2月1日 
 * @time: 下午12:27:39
 */
@Configuration
public class WebAutoConfiguration {

    @Bean
    public FilterRegistrationBean<CustomTraceFilter> indexFilterRegistration() {
        StringBuffer excludedUriStr = new StringBuffer();
        excludedUriStr.append("/favicon.ico");

        // 排除swagger
        excludedUriStr.append(",");
        excludedUriStr.append("/swagger-ui.html,/swagger-ui.html/.*");

        excludedUriStr.append(",");
        excludedUriStr.append("/webjars/.*");

        excludedUriStr.append(",");
        excludedUriStr.append("/swagger-resources,/swagger-resources/.*");

        excludedUriStr.append(",");
        excludedUriStr.append("/v2/api-docs");
        
        // 排除心跳包
        excludedUriStr.append(",");
        excludedUriStr.append("/actuator/health");

        FilterRegistrationBean<CustomTraceFilter> registration = new FilterRegistrationBean<>();
        registration.setFilter(new CustomTraceFilter());
        registration.addUrlPatterns("/*");
        registration.addInitParameter("excludedUrls", excludedUriStr.toString());
        return registration;
    }
}