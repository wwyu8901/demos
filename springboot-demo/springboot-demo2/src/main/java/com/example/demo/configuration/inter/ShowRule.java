package com.example.demo.configuration.inter;

import java.util.List;

public class ShowRule {
    private String typCode;
    private List<String> fields;

    public String getTypCode() {
        return typCode;
    }

    public void setTypCode(String typCode) {
        this.typCode = typCode;
    }

    public List<String> getFields() {
        return fields;
    }

    public void setFields(List<String> fields) {
        this.fields = fields;
    }
}
