package com.example.demo.dto;

import lombok.Data;

@Data
public class HttpInfo {

    private String url;

    /**
     * http请求方式,get/post
     */
    private String httpMethod;

    private String contentType;

    /**
     * 是否返回文件 1-是，0-否
     *
     */
    private String retuFile;


}
