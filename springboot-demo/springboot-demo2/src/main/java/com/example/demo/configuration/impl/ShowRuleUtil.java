package com.example.demo.configuration.impl;


import com.example.demo.SpringUtils;
import com.example.demo.configuration.inter.ShowRuleHandler;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public class ShowRuleUtil {

    public static final String TYPE_CODE_NAME = "typCode";

    /**
     * 手动处理多条数据
     *
     * @param data
     * @param typCode 如果指定了typCode值，以指定的为准，不指定请传null
     */
    public static void handle(List<Map<String, Object>> data, String typCode) {
        ShowRuleHandler showRuleHandler = getBean();
        if (!showRuleHandler.isEnable()) {
            return;
        }
        data.stream().forEach(t -> {
            handleOne(t,typCode);
        });
    }

    private static ShowRuleHandler getBean() {
        return SpringUtils.getBean(ShowRuleHandler.class);
    }

    /**
     * 手动处理单条数据
     *
     * @param map
     * @param typCode 如果指定了typCode值，以指定的为准，不指定请传null
     */
    public static void handle(Map<String, Object> map, String typCode) {
        ShowRuleHandler showRuleHandler = getBean();
        if (!showRuleHandler.isEnable()) {
            return;
        }
        handleOne(map, typCode);
    }

    /**
     * 如果指定了typCode，以指定的为准，不指定请传null
     *
     * @param t
     * @param typCode
     */
    private static void handleOne(Map<String, Object> t, String typCode) {
        Object typCodeObj = t.get(TYPE_CODE_NAME);
        ShowRuleHandler showRuleHandler = getBean();
        String finalTypCode = typCode;
        //传入为空且数据中有值则取数据中的typCode
        if (finalTypCode == null && typCodeObj != null && typCodeObj instanceof String) {
            finalTypCode = (String) typCodeObj;
        }
        //获取不到typCode则不处理
        if (finalTypCode == null) {
            return;
        }
        String fTypCode = finalTypCode;
        t.forEach((fieldName, value) -> {
            //无意义值展示处理
            String invalidValue = showRuleHandler.getInvalidValue(fTypCode, fieldName);
            if (invalidValue != null) {
                t.put(fieldName, invalidValue);
                return;
            }
            //默认值处理
            BigDecimal defaultZeroValue = showRuleHandler.getDefaultZeroValue(fieldName, value);
            if (defaultZeroValue != null) {
                t.put(fieldName, defaultZeroValue);
                return;
            }

        });
    }

    /**
     * 处理单个对象的方法
     * 如果指定了typCode，以指定的为准，不指定请传null
     * @param objValue 对象值
     * @param fieldName 当前字段名称
     * @param fieldValue 当前字段值
     * @return
     */
    public static Object handle(Object objValue, String fieldName, Object fieldValue) {
        ShowRuleHandler showRuleHandler = getBean();
        if (!showRuleHandler.isEnable()) {
            return fieldValue;
        }
        //获取当前typCode
        String typCode = showRuleHandler.getTypeCodeValue(objValue);
        //获取不到typCode则不处理
        if (typCode == null) {
            return fieldValue;
        }
        //无意义值展示处理
        String invalidValue = showRuleHandler.getInvalidValue(typCode, fieldName);
        if (invalidValue != null) {
            return invalidValue;
        }
        //默认值处理
        BigDecimal defaultZeroValue = showRuleHandler.getDefaultZeroValue(fieldName, fieldValue);
        if (defaultZeroValue != null) {
            return defaultZeroValue;
        }
        //默认返回原值
        return fieldValue;

    }

}
