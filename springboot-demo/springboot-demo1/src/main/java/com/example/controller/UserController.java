package com.example.controller;

import com.example.dto.UserDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@RestController
public class UserController {

    private static Logger LOGGER = LoggerFactory.getLogger(UserController.class);

    @RequestMapping("/user/list")
    public List<UserDto> getAllUser(){
        LOGGER.info("aaaaa");
        List<UserDto> userDtos = new ArrayList<>();
        UserDto userDto = new UserDto();
        userDto.setUserName("aaa");
        userDto.setRate(new BigDecimal(20.023456));
        userDtos.add(userDto);
        LOGGER.warn("bbbbb");

        return userDtos;
    }
}
