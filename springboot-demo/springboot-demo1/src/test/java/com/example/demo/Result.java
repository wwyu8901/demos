package com.example.demo;

import java.util.ArrayList;
import java.util.List;

public class Result {
    private int count = 0;

    private List result = new ArrayList();

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public List getResult() {
        return result;
    }

    public void setResult(List result) {
        this.result = result;
    }
}
