package com.example.demo;

import com.example.demo.configuration.impl.ShowRuleHandlerImpl;
import com.example.demo.configuration.impl.ShowRuleUtil;
import com.example.demo.dto.UserDto;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.Date;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringbootDemo2Application.class)
public class UserDtoTest {

    @Autowired
    private ShowRuleHandlerImpl showRuleHandler;

    @Test
    public void test(){
//        showRuleHandler.setEnable(false);
        UserDto userDto = new UserDto();
//        userDto.setUserName("张三");
        userDto.setRate(new BigDecimal("20.023456"));
        userDto.setDate(new Date());
//        userDto.setTypCode("BOND");
        userDto.setType(3);
        userDto.setTVal("ddddddd");
        Object type = ShowRuleUtil.handle(userDto, "type", 3);
        Assert.assertEquals("--",type);
        userDto.setTypCode("STK");
        type = ShowRuleUtil.handle(userDto, "type", 3);
        Assert.assertEquals(3,type);
        type = ShowRuleUtil.handle(userDto, "type", null);
        Assert.assertEquals(BigDecimal.ZERO,type);
        Object name = ShowRuleUtil.handle(userDto, "name", null);
        Assert.assertEquals(null,name);
    }

    @Test
    public void testKafka(){
        System.out.println("");
    }


}
