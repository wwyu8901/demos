package com.example.demo;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.util.FileUtil;
import org.json.JSONObject;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import com.alibaba.fastjson.TypeReference;

@Slf4j
public class Test1 {

    @Test
    public void test(){
        try{
            Integer.parseInt("aaaaa");
        }catch (Exception e){
            log.error("{}转换异常","aaaaa",e);
        }
    }

    @Test
    public void test2() throws IOException {
        ClassPathResource classPathResource = new ClassPathResource("1.json");
        String s = FileUtil.readAsString(classPathResource.getFile());
        List<Map<String, Object>> dataList = JSON.parseObject(s,
                new TypeReference<List<Map<String, Object>>>(){});
        String secuId = "000001.FUND.NONE";
        String FUND_MR_NAME = "FUNDMR";
        Map<String, BigDecimal> fundMrMap = dataList.stream()
                .filter(t -> secuId.equals(t.get("代码")))
                .collect(Collectors.toMap(t ->(String) t.get("日期"), t -> new BigDecimal(t.get(FUND_MR_NAME) == null?"0":t.get(FUND_MR_NAME).toString()), (k1, k2) -> k2));
        System.out.println(fundMrMap);
    }
}
