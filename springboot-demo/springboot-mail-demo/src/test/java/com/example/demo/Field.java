package com.example.demo;

public class Field {
    private String dataIndex;
    private String title;
    private Boolean checked;
    /**
     * 是否判断颜色
     */
    private Boolean colorChk;

    public Boolean getColorChk() {
        return colorChk;
    }

    public void setColorChk(Boolean colorChk) {
        this.colorChk = colorChk;
    }

    public Boolean getChecked() {
        return checked;
    }

    public void setChecked(Boolean checked) {
        this.checked = checked;
    }

    public String getDataIndex() {
        return dataIndex;
    }

    public void setDataIndex(String dataIndex) {
        this.dataIndex = dataIndex;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
