package com.example.demo;

import org.apache.commons.lang3.time.FastDateFormat;
import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.Locale;

public class RestTemplateTest {

    @Test
    public void test(){
        // 创建 RestTemplate
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        // 调用下载接口进行下载
        // id 为String类型，为被调用接口参数
        ResponseEntity<byte[]> entity = restTemplate.exchange(
                "https://cloud.gildata.com/queryservice/bulletin/attachment/fund/181316843124",
                HttpMethod.GET,new HttpEntity<>(headers), byte[].class);
        System.out.println(entity.getHeaders());

        entity = restTemplate.exchange(
                "https://cloud.gildata.com/queryservice/bulletin/attachment/fund/183801908688",
                HttpMethod.GET,new HttpEntity<>(headers), byte[].class);
        System.out.println(entity.getHeaders());

        StringBuffer sb = new StringBuffer();
        sb.append("eddd");
    }

    @Test
    public void test2(){
        System.out.println("aa华夏5000年a".toLowerCase(Locale.ENGLISH));
        System.out.println("aa华夏5000年a".toUpperCase(Locale.ENGLISH));

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); //日期格式化,默认yyyy-MM-dd HH:mm:ss
        System.out.println(sdf.format(new Date()));

        FastDateFormat sdf2 = FastDateFormat.getInstance("yyyy-MM-dd HH:mm:ss"); //日期格式化,默认yyyy-MM-dd HH:mm:ss
        System.out.println(sdf2.format(new Date()));

        NumberFormat nf = NumberFormat.getInstance();

        StringBuilder sb = new StringBuilder(10);
        sb.append("aaaaarrrrrrrrrrrrrrrrrrrrrrr");
        sb.append("bbbbbbdsfsdfsdfsdfsdfsdfasdfsadfasdfasdfasdfasdfas");
        System.out.println(sb.toString());
    }

    @Test
    public void test3(){
        LocalDateTime obj = LocalDateTime.now();
        System.out.println(Date.from(((LocalDateTime)obj).atZone( ZoneId.systemDefault()).toInstant()));
    }
}
