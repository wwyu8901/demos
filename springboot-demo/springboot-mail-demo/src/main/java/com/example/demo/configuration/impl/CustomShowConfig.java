package com.example.demo.configuration.impl;

import com.example.demo.configuration.YamlPropertySourceFactory;
import com.example.demo.configuration.inter.ShowRule;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@PropertySource(value = "classpath:config/customShow.yml", factory = YamlPropertySourceFactory.class)
@ConfigurationProperties(prefix = "custom-show")
public class CustomShowConfig {

    private List<ShowRule> showRules = new ArrayList<>();

    private List<String> emptyZeros = new ArrayList<>();

    public List<ShowRule> getShowRules() {
        return showRules;
    }

    public void setShowRules(List<ShowRule> showRules) {
        this.showRules = showRules;
    }

    public List<String> getEmptyZeros() {
        return emptyZeros;
    }

    public void setEmptyZeros(List<String> emptyZeros) {
        this.emptyZeros = emptyZeros;
    }
}