package com.example.demo.configuration.impl;

import com.example.demo.configuration.inter.ShowRule;
import com.example.demo.configuration.inter.ShowRuleHandler;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.beans.NotReadablePropertyException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;

import static com.example.demo.configuration.impl.ShowRuleUtil.TYPE_CODE_NAME;


@Component
public class ShowRuleHandlerImpl implements ShowRuleHandler {

    //无意义指标需要展示的值
    public static final String INVALID_FILED_VAL = "--";

    //空值需要展示的默认值
    public static final BigDecimal SHOW_DEFAULT_VAL = BigDecimal.ZERO;

    @Autowired
    private CustomShowConfig customShowConfig;

    @Value("${customShowEnable:false}")
    private boolean enable;

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    /**
     * 根据对象值获取typCode
     * @param objValue
     * @return
     */
    public String getTypeCodeValue(Object objValue) {
        BeanWrapper beanWrapper = new BeanWrapperImpl(objValue);
        try{
            String typCode = (String)beanWrapper.getPropertyValue(TYPE_CODE_NAME);
            return typCode;
        }catch (NotReadablePropertyException notReadablePropertyException){
            //获取失败
            return null;
        }
    }

    /**
     * 获取无意义指标值
     * 处理不了，将返回null
     * @param typCode
     * @param fieldName
     * @return
     */
    public String getInvalidValue(String typCode,String fieldName){
        if(matchInvalid(typCode,fieldName)){
            return INVALID_FILED_VAL;
        }
        return null;
    }

    /**
     * 如果可以处理则处理为0，否则返回null
     * @param fieldName
     * @param fieldValue
     * @return
     */
    public BigDecimal getDefaultZeroValue(String fieldName, Object fieldValue){
        if(matchZeros(fieldName,fieldValue)){
            return SHOW_DEFAULT_VAL;
        }
        return null;
    }

    /**
     *
     * @param typCode
     * @param fieldName
     * @return
     */
    private boolean matchInvalid(String typCode,String fieldName) {

        if(!enable || typCode == null){
            return false;
        }
        List<ShowRule> showRules = customShowConfig.getShowRules();
        for (ShowRule showRule : showRules) {
            List<String> fields = showRule.getFields();
            if(showRule.getTypCode().equalsIgnoreCase(typCode)
                    && fields.contains(fieldName)
            ){
                return true;
            }
        }
        return false;
    }

    private boolean matchZeros(String fieldName,Object fieldValue){
        //值不为null，不处理
        if(!enable || fieldValue != null){
            return false;
        }
        List<String> emptyZeros = customShowConfig.getEmptyZeros();
        for (String emptyZero : emptyZeros) {
            if(fieldName.equalsIgnoreCase(emptyZero)){
                return true;
            }
        }
        return false;
    }
}
