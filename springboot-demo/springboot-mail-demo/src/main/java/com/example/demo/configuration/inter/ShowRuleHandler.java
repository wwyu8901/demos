package com.example.demo.configuration.inter;

import java.math.BigDecimal;

public interface ShowRuleHandler {
    String getTypeCodeValue(Object objValue);

    String getInvalidValue(String typCode,String fieldName);

    BigDecimal getDefaultZeroValue(String fieldName, Object fieldValue);

    boolean isEnable();
}
