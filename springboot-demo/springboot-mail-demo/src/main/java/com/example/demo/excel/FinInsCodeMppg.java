package com.example.demo.excel;

import com.xctech.xcexcel.annotaion.ExcelColumn;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @TableName FIN_INS_CODE_MPPG
 */
@Data
public class FinInsCodeMppg implements Serializable{
    /**
     * 源机构代码
     */
    @ExcelColumn(value = "源机构编码")
    private String srcInsId;

    /**
     * 源机构名称
     */
    @ExcelColumn(value = "源机构名称")
    private String srcInsName;

    /**
     * 目标机构代码
     */
    @ExcelColumn(value = "目标机构编码")
    private String tgtInsId;
    /**
     * 目标机构名称
     */
    @ExcelColumn("目标机构名称")
    private String tgtInsName;

    /**
     * 数据状态(0待维护，1自动映射，2已维护)
     */
    @ExcelColumn("数据状态")
    private String dataStat;

    /**
     * 来源表字段
     */
    @ExcelColumn("来源表字段")

    private String srcTabCol;
    /**
     * 系统来源标识
     */
    @ExcelColumn(value = "系统来源标识")
    private String sysRsId;

    /**
     * 业务日期
     */
    @ExcelColumn(value = "业务日期")
    private Integer busiDate;

    /**
     * 任务来源标识
     */
    @ExcelColumn(value = "任务来源标识")

    private String taskRsId;

    /**
     * 数据中台创建时间
     */
    @ExcelColumn(value = "数据中台创建时间",format="yyyy-MM-dd HH:mm:ss")
    private Date dmCreatedTime;

    public FinInsCodeMppg(String tgtInsId){
        this.tgtInsId=tgtInsId;
    }
    public FinInsCodeMppg(){

    }

}