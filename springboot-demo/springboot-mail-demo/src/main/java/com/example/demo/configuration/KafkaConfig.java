package com.example.demo.configuration;

import com.alibaba.fastjson.JSONObject;
import com.example.demo.dto.UserDto;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.serialization.Serializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.annotation.TopicPartition;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class KafkaConfig {
    private Logger logger = LoggerFactory.getLogger(KafkaConfig.class);

//    @KafkaListener(topics ={ "kevin" })
    public void listen(List<UserDto> recordList) {
        logger.info("监听..");
        Serializer serializer;
        recordList.forEach((record)->{
            logger.info("message: " + JSONObject.toJSON(record));
        });
    }
}
