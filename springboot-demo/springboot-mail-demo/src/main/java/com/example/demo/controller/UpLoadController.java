package com.example.demo.controller;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import java.io.File;
import java.io.IOException;

@RestController
public class UpLoadController {
    @GetMapping("/begin")
    public String login(){
        return "upload";
    }

    @PostMapping("/upload")
    public void upload(@RequestParam("email") String email,
                         @RequestParam("name") String name,
                         @RequestPart("file") MultipartFile file,
                         @RequestPart("files") MultipartFile[] files) throws IOException {
        System.out.println(email + "," + name);
        if(!file.isEmpty()){
            //文件名
            String originalFilename = file.getOriginalFilename();
            //springboot独特的写出操作，很方便
            file.transferTo(new File("D:\\"+originalFilename));
        }
        if(files.length > 0){
            //遍历文件
            for (MultipartFile multipartFile : files) {
                if(!multipartFile.isEmpty()){
                    String originalFilename = multipartFile.getOriginalFilename();
                    multipartFile.transferTo(new File("D:\\"+originalFilename));
                }
            }
        }
    }

}
