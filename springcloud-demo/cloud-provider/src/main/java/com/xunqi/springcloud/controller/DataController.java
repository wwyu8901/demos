package com.xunqi.springcloud.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@RestController
public class DataController {

    @Value("classpath:data/tableData.json")
    private Resource jsonResource;

    @RequestMapping(value = "/getData")
    public Map<String, List<Map>> getList() throws IOException {
        InputStream inputStream2 = new FileInputStream(jsonResource.getFile());
        String data = IOUtils.toString(inputStream2,"utf8");
        inputStream2.close();
        //先要构建数据对象
        return JSON.parseObject(data,new TypeReference<LinkedHashMap<String,List<Map>>>(){});
    }
}
