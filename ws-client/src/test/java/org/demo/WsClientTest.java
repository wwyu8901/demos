package org.demo;

import org.demo.webservice.WeatherService;
import org.demo.webservice.WeatherServiceService;

public class WsClientTest {
    public static void main(String[] args) {
        WeatherServiceService factory = new WeatherServiceService();
        WeatherService servicePort = factory.getWeatherServicePort();
        String weather = servicePort.getWeatherByCityname("深圳");
        System.out.println(weather);
    }
}
