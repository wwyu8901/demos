package org.wwy;



import com.alibaba.fastjson.JSON;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.security.UserGroupInformation;
import org.junit.Test;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import java.io.IOException;
import java.util.Date;
import java.sql.*;
import java.util.List;
import java.util.Map;

public class HiveTest {

    @Test
    public void test() throws ClassNotFoundException, SQLException, IOException {
        System.setProperty("java.security.krb5.conf", "D:/kbs/krb5.conf");
        System.setProperty("javax.security.auth.useSubjectCredsOnly", "false");
//        System.setProperty("sun.security.krb5.debug", "true");

        Configuration configuration = new Configuration();
        configuration.set("hadoop.security.authentication", "Kerberos");
//        configuration.set("kerberos.principal", "hive/pone-cdh-mgr01@XUNCE.COM");
        UserGroupInformation.setConfiguration(configuration);

        UserGroupInformation.loginUserFromKeytabAndReturnUGI("hive/pone-cdh-mgr01@XUNCE.COM", "D:/kbs/xunce.keytab");



        Class.forName("org.apache.hive.jdbc.HiveDriver");
        System.out.println(new Date());
        Connection conn =
                DriverManager.getConnection("jdbc:hive2://10.150.8.26:10000/default;principal=hive/pone-cdh-mgr01@XUNCE.COM;sasl.qop=auth-conf;auth=KERBEROS",null,null);  ;
        System.out.println(new Date());

        System.out.println(conn.getClass());
        PreparedStatement ps = conn.prepareStatement("select * from (SELECT *  from `xc_dwd`.QL_SQLSERVER_ASHAREFINANCIALDERIVATIVE where 1=1  ) T where ( not ((T.`ORPS`!=0.0276)  ) )  AND  ORPS IS NOT NULL");
        System.out.println(ps.getClass());
        ResultSet resultSet = ps.executeQuery();
        System.out.println(resultSet.getClass());
        System.out.println(new Date());
        ResultSetMetaData metaData = resultSet.getMetaData();
        int columnCount = metaData.getColumnCount();
        while (resultSet.next()){
            for(int i=0;i<columnCount;i++){
                System.out.print(metaData.getColumnName(i+1));
                System.out.println("===="+resultSet.getObject(i+1));
            }
            System.out.println("================================");
        }
        System.out.println(new Date());

    }


    @Test
    public void test2() throws SQLException {
        System.out.println(new Date());
        DriverManagerDataSource driverManagerDataSource = new
                DriverManagerDataSource("jdbc:hive2://192.168.0.105:10000/xc_dwd","hive","hive");
        System.out.println(new Date());
        JdbcTemplate jdbcTemplate = new JdbcTemplate(driverManagerDataSource);
        System.out.println(new Date());
        List<Map<String, Object>> maps = jdbcTemplate.queryForList(" select * from (SELECT *  from `xc_dwd`.QL_SQLSERVER_ASHAREFINANCIALDERIVATIVE where 1=1  ) T where ( not ((T.`ORPS`!=0.0276)  ) )  AND  ORPS IS NOT NULL");
        System.out.println(new Date());
        System.out.println(JSON.toJSONString(maps));

    }

    @Test
    public void test4() throws ClassNotFoundException {
        String connectionUrl = "jdbc:impala://10.150.8.27:25004";
        Class.forName("com.cloudera.impala.jdbc.Driver");
        try (Connection con = DriverManager.getConnection(connectionUrl)) {
            Statement statement = con.createStatement();
            ResultSet rs = statement.executeQuery("SELECT count(1) FROM norm_demo");
            while (rs.next()) {
                rs.getInt(1);
            }
            statement.close();
            con.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
