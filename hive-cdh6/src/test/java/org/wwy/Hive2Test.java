package org.wwy;

import org.junit.Test;

import java.sql.*;

public class Hive2Test {

    private static final String HIVE_DRIVER = "org.apache.hive.jdbc.HiveDriver";

    @Test
    public void test() throws ClassNotFoundException, SQLException {
        Class.forName(HIVE_DRIVER);
        Connection conn = null;
        try {
            DriverManager.getLoginTimeout();
            conn = DriverManager.getConnection("jdbc:hive2://192.168.0.171:10000/lineage_l2");
            System.out.println(conn);
            ResultSet tables = conn.getMetaData().getColumns(conn.getCatalog(), conn.getSchema(), "%","%");
            ResultSetMetaData metaData = tables.getMetaData();
            int columnCount = metaData.getColumnCount();
            while (tables.next()){
                /*for(int i=0;i<columnCount;i++){
                    System.out.print(metaData.getColumnName(i+1));
                    System.out.println("===="+tables.getObject(i+1));
                }*/
                String columnName = tables.getString("COLUMN_NAME");
                String tableName = tables.getString("TABLE_NAME");
                String typeName = tables.getString("TYPE_NAME");

//                System.out.println(tableName+":"+columnName+":"+typeName);
            }
            PreparedStatement preparedStatement = conn.prepareStatement("select 1");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                for(int i=0;i<columnCount;i++){
                    System.out.print(metaData.getColumnName(i+1));
                    System.out.println("===="+tables.getObject(i+1));
                }
            }
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    //do nothing
                }
            }
        }
    }

    @Test
    public void test4(){
        System.out.println(System.getProperty("user.dir"));
    }

    @Test
    public void test5() throws ClassNotFoundException, SQLException{
        String sql = "with t1 as (select max(d_cdate) as ctl_log_date from ods.ta_tfundday t where t.srcsys = 'XTTA')\n" +
                "select case when ? > ctl_log_date then 1 else 0 end as suc_or_fl from t1";
        Class.forName(HIVE_DRIVER);
        Connection conn = null;
        try {
            conn = DriverManager.getConnection("jdbc:hive2://192.168.0.171:10000","hive","hive");
            System.out.println(conn);
            PreparedStatement preparedStatement = conn.prepareStatement("select 1");
            ResultSet resultSet = preparedStatement.executeQuery();
            ResultSetMetaData metaData = resultSet.getMetaData();
            int columnCount = metaData.getColumnCount();
            while (resultSet.next()){
                for(int i=0;i<columnCount;i++){
                    System.out.print(metaData.getColumnName(i+1));
                    System.out.println("===="+resultSet.getObject(i+1));
                }
            }
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    //do nothing
                }
            }
        }
    }
}
