package org.wwy;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;


import java.io.IOException;
import java.net.URI;

public class HDFSUTILS {
    static FileSystem fileSystem;
   
   //获取HDFS连接对象
    static {
        Configuration conf = new Configuration();
        try {
           fileSystem = FileSystem.get(URI.create("hdfs://192.168.117.128:9000"), conf, "root");                              //参数1是链接参数，2是配置项，3是用户名
            System.out.println(fileSystem);
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

//让外部可访问到连接对象
public static FileSystem getFileSystem(){
        return fileSystem;
}

    public static void main(String[] args) {

    }
}

