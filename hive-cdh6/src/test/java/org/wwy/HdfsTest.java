package org.wwy;

import org.apache.commons.io.FileUtils;
import org.apache.hadoop.fs.*;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.hadoop.conf.Configuration;
import org.junit.Before;
import org.junit.Test;


public class HdfsTest {

    @Test
    public void test() throws URISyntaxException, IOException, InterruptedException {

//        System.setProperty("HADOOP_USER_NAME","hive");
//        System.setProperty("HADOOP_USER_PASSWORD","123123");

        /*
         *
         * 注意注意注意
         * windows上eclipse上运行，hadoop的bin文件下windows环境编译的文件和hadoop版本关系紧密
         * 如果报错，或者api无效，很可能是bin下hadoop文件问题
         *
         */

        //读取classpath下的core/hdfs-site.xml 配置文件，并解析其内容，封装到conf对象中
        //如果没有会读取hadoop里的配置文件
        Configuration conf = new Configuration();

        //也可以在代码中对conf中的配置信息进行手动设置，会覆盖掉配置文件中的读取的值
        //设置文件系统为hdfs。如果设置这个参数，那么下面的方法里的路径就不用写hdfs://hello110:9000/
        //conf.set("fs.defaultFS", "hdfs://hello110:9000/");

//        conf.set("HADOOP_USER_NAME","hive");
        //根据配置信息，去获取一个具体文件系统的客户端操作实例对象
        FileSystem fs = FileSystem.get(new URI("hdfs://192.168.20.103:8020/inceptor1/user/hive/warehouse/default.db/hive/datax_test_type"),
                conf, "hive");


        //RemoteIterator 远程迭代器
        RemoteIterator<LocatedFileStatus> listFiles = fs.listFiles(new Path("/"), true);

        while (listFiles.hasNext()) {
            LocatedFileStatus file = listFiles.next();
            Path path = file.getPath();
            //String fileName=path.getName();
            System.out.println(path.toString());
            System.out.println("权限：" + file.getPermission());
            System.out.println("组：" + file.getGroup());
            System.out.println("文件大小：" + file.getBlockSize());
            System.out.println("所属者：" + file.getOwner());
            System.out.println("副本数：" + file.getReplication());

            BlockLocation[] blockLocations = file.getBlockLocations();
            for (BlockLocation bl : blockLocations) {
                System.out.println("块起始位置：" + bl.getOffset());
                System.out.println("块长度：" + bl.getLength());
                String[] hosts = bl.getHosts();
                for (String h : hosts) {
                    System.out.println("块所在DataNode：" + h);
                }

            }

            System.out.println("*****************************************");

        }


        System.out.println("-----------------/hdfs-site.xml 文件读取结束----------------");

    }


    @Test
    public void test2() throws URISyntaxException, IOException, InterruptedException {

//        System.setProperty("HADOOP_USER_NAME","hive");
//        System.setProperty("HADOOP_USER_PASSWORD","123123");

        /*
         *
         * 注意注意注意
         * windows上eclipse上运行，hadoop的bin文件下windows环境编译的文件和hadoop版本关系紧密
         * 如果报错，或者api无效，很可能是bin下hadoop文件问题
         *
         */

        //读取classpath下的core/hdfs-site.xml 配置文件，并解析其内容，封装到conf对象中
        //如果没有会读取hadoop里的配置文件
        Configuration conf = new Configuration();
        conf.set("fs.defaultFS", "file:////");
        //也可以在代码中对conf中的配置信息进行手动设置，会覆盖掉配置文件中的读取的值
        //设置文件系统为hdfs。如果设置这个参数，那么下面的方法里的路径就不用写hdfs://hello110:9000/
        //conf.set("fs.defaultFS", "hdfs://hello110:9000/");

//        conf.set("HADOOP_USER_NAME","hive");
        //根据配置信息，去获取一个具体文件系统的客户端操作实例对象
        FileSystem fs = FileSystem.get(conf);


        //RemoteIterator 远程迭代器
//        RemoteIterator<LocatedFileStatus> listFiles = fs.listFiles(new Path("e:/"), true);

        try (FSDataInputStream in = fs.open(new Path("e:/oracle.sql"))) {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(in));
            StringBuilder result = new StringBuilder();
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                result.append(line);
            }
            System.out.print(result);


        }


        System.out.println("-----------------/hdfs-site.xml 文件读取结束----------------");

    }

    @Test
    public void test3() throws URISyntaxException, IOException, InterruptedException {

//        System.setProperty("HADOOP_USER_NAME","hive");
//        System.setProperty("HADOOP_USER_PASSWORD","123123");

        /*
         *
         * 注意注意注意
         * windows上eclipse上运行，hadoop的bin文件下windows环境编译的文件和hadoop版本关系紧密
         * 如果报错，或者api无效，很可能是bin下hadoop文件问题
         *
         */

        //读取classpath下的core/hdfs-site.xml 配置文件，并解析其内容，封装到conf对象中
        //如果没有会读取hadoop里的配置文件
        Configuration conf = new Configuration();

        //也可以在代码中对conf中的配置信息进行手动设置，会覆盖掉配置文件中的读取的值
        //设置文件系统为hdfs。如果设置这个参数，那么下面的方法里的路径就不用写hdfs://hello110:9000/
        //conf.set("fs.defaultFS", "hdfs://hello110:9000/");

//        conf.set("HADOOP_USER_NAME","hive");
        //根据配置信息，去获取一个具体文件系统的客户端操作实例对象
        FileSystem fs = FileSystem.get(new URI("file:////"),
                conf, "hive");


        //RemoteIterator 远程迭代器
//        RemoteIterator<LocatedFileStatus> listFiles = fs.listFiles(new Path("e:/"), true);

        RemoteIterator<LocatedFileStatus> listFiles = fs.listFiles(new Path("e:/"), true);
        while (listFiles.hasNext()){
            LocatedFileStatus file = listFiles.next();
            Path path = file.getPath();
            //String fileName=path.getName();
            System.out.println(path.toString());
            System.out.println("权限：" + file.getPermission());
            System.out.println("组：" + file.getGroup());
            System.out.println("文件大小：" + file.getBlockSize());
            System.out.println("所属者：" + file.getOwner());
            System.out.println("副本数：" + file.getReplication());

            BlockLocation[] blockLocations = file.getBlockLocations();
            for (BlockLocation bl : blockLocations) {
                System.out.println("块起始位置：" + bl.getOffset());
                System.out.println("块长度：" + bl.getLength());
                String[] hosts = bl.getHosts();
                for (String h : hosts) {
                    System.out.println("块所在DataNode：" + h);
                }

            }

            System.out.println("*****************************************");
        }


        System.out.println("-----------------/hdfs-site.xml 文件读取结束----------------");

    }
}
