package org.wwy.demo.excel;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 估值表模板配置
 * </p>
 *
 * @author WILL
 * @since 2022-05-13
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class FinValuConfTmpl implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * 主键
     */
//    @TableId(value = "ID")
//    private Long id;

    /**
     * 模板编码
     */
    private String tmplId;

    /**
     * 模板名称
     */
    private String tmplName;

    /**
     * 起始行
     */
    private Integer begnRow;

    /**
     * 科目代码列
     */
    private Integer colCrseCode;

    /**
     * 科目名称列
     */
    private Integer colCrseName;

    /**
     * 数量列
     */
    private Integer colVol;

    /**
     * 成本列
     */
    private Integer colCost;

    /**
     * 市值列
     */
    private Integer colVal;

    /**
     * 估增列
     */
    private Integer colValuAdd;

    /**
     * 是否默认模板(1:是,0:否)
     */
    private String isDefTmpl;

    /**
     * 更新人
     */
    private String dmUpdatedBy;

    /**
     * 更新时间
     */
    private Date dmUpdatedTime;


}
