package org.wwy.demo.excel;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import jxl.write.Label;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

import java.io.*;

public class JxlReader {
    public void readByJxl(int startRow,int startColumn) throws IOException, BiffException, WriteException {
        FileInputStream fileInputStream = new FileInputStream("D:\\workspace\\demos\\poi3demo\\src\\test\\resources\\010969_华夏安阳6个月持有期混合型证券投资基金_港股通风控资金账户调整单_20241121.xls");//开启文件读取流
        Workbook book = Workbook.getWorkbook(fileInputStream);
        fileInputStream.close();
        Sheet sheet = book.getSheet(0);
        String contents = sheet.getCell(1, 10).getContents();
        System.out.println(contents);
        book.close();
        /*ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        WritableWorkbook writeBook=Workbook.createWorkbook(outputStream,book);
        writeBook.write();
        writeBook.close();
        TestExcel testExcel = new TestExcel();
        testExcel.import3(outputStream);*/

        /*//获得第一个工作表对象，0表示第一个
        Sheet sheet = book.getSheet(0);
        Label label;
        for (int row = startRow; row < sheet.getRows(); row ++){//行数
            for (int column = startColumn; column < sheet.getColumns(); column ++){//列数
                Cell cell = sheet.getCell(column,row);
                System.out.print(cell.getContents()+" ");
            }
            System.out.println();
        }*/
    }
}
