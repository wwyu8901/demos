package org.wwy.demo.excel;


import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 估值表模板关联
 * </p>
 *
 * @author WILL
 * @since 2022-05-13
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class FinValuPrdTmplRela implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * 主键
     */
//    @TableId(value = "ID")
//    private Long id;

    /**
     * 产品代码
     */
    private String prdCode;

    /**
     * 文件名
     */
    private String fileName;

    /**
     * 估值方法(1,市值法,2摊余成本法)
     */
    private String evMeth;

    /**
     * 模板编码
     */
    private String tmplId;

    /**
     * 更新时间
     */
    private Date dmUpdatedTime;

    /**
     * 更新人
     */
    private String dmUpdatedBy;

    private String matchNm;
    private FinValuConfTmpl tmpl;

    /**
     * 产品名称
     */
    private String prdName;

    /**
     * 成立日期
     */
    private BigDecimal foundDt;
    /**
     * 到期日期
     *
     */
    private BigDecimal endDt;

}
