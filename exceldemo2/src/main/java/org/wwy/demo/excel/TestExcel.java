package org.wwy.demo.excel;

import org.apache.poi.hssf.record.RecordInputStream;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.*;
import java.util.List;
public class TestExcel {


   /* public void importFromExcel() throws Exception {
        ImportParams params = new ImportParams();
        params.setTitleRows(1);//标题占几行,从哪行开始读取
        params.setHeadRows(1);//header占几行
        FileInputStream fileInputStream = new FileInputStream("d:/xxx估值表_2023-03-13.xls");
        List<OdsValuFile> list = ExcelImportUtil.importExcel(fileInputStream, OdsValuFile.class, params);
        System.out.println(list);
    }*/

    public void import2() throws IOException {
        FileInputStream fileInputStream = new FileInputStream("d:/xxx估值表_2023-03-13.xls");//开启文件读取流
        try {
            HSSFWorkbook workbook = new HSSFWorkbook(fileInputStream);//读取文件
            //获取sheet
            HSSFSheet sheet = workbook.getSheetAt(0);
            System.out.println(sheet);
        }catch (RecordInputStream.LeftoverDataException e){
            throw e;
        }

    }

    public void import3(ByteArrayOutputStream outputStream) throws IOException {
        InputStream inputStream = new ByteArrayInputStream(outputStream.toByteArray());
        HSSFWorkbook workbook = new HSSFWorkbook(inputStream);//读取文件
        //获取sheet
        HSSFSheet sheet = workbook.getSheetAt(0);
        System.out.println(sheet);
    }



}