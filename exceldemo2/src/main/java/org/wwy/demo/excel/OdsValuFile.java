package org.wwy.demo.excel;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.File;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 源估值表文件
 * </p>
 *
 * @author WILL
 * @since 2022-05-13
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class OdsValuFile implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * 主键
     */
//    @TableId(value = "ID")
//    private Long id;

    /**
     * 文件名
     */
    private String fileName;

    /**
     * 估值日期
     */
    private Long valuDt;

    /**
     * 产品代码
     */
    private String prdCode;

    /**
     * 文件行号
     */
    private Integer rowNo;

    /**
     * 科目代码
     */
    private String crseCode;

    /**
     * 科目名称
     */
    private String crseName;

    /**
     * 持仓数量
     */
    private BigDecimal hldVol;

    /**
     * 持仓成本
     */
    private BigDecimal hldCost;

    /**
     * 持仓市值
     */
    private BigDecimal hldVal;

    /**
     * 持仓估值增值
     */
    private BigDecimal hldValuAdd;

    /**
     * 更新时间
     */
    
    private Date dmUpdatedTime;

    /**
     * 更新人
     */
    private String dmUpdatedBy;


    private File file;


    private FinValuPrdTmplRela finValuPrdTmplRela;


}
