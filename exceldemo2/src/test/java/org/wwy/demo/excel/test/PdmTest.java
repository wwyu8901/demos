package org.wwy.demo.excel.test;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class PdmTest {

    public static void main(String[] args) throws IOException {
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet testModel = workbook.createSheet("testModel");
        XSSFRow titleRow = testModel.createRow(0);
        titleRow.createCell(0).setCellValue("Database Name");
        titleRow.createCell(1).setCellValue("Table Name");
        titleRow.createCell(2).setCellValue("Column Name");

        XSSFRow dataRow = testModel.createRow(1);
        dataRow.createCell(0).setCellValue("MyDatabase");
        dataRow.createCell(1).setCellValue("MyTable");
        dataRow.createCell(2).setCellValue("ID");

        FileOutputStream outputStream = new FileOutputStream("e:/myPdm.pdm");
        workbook.write(outputStream);
        workbook.close();
        System.out.println("生成成功!");
    }
}
