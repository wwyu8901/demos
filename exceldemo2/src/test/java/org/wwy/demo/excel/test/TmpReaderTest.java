package org.wwy.demo.excel.test;


import org.junit.Test;
import org.wwy.demo.excel.JxlReader;
import org.wwy.demo.excel.TestExcel;

import java.io.IOException;

public class TmpReaderTest {

    @Test
    public void testRead() throws Exception {
        JxlReader testExcel = new JxlReader();
        testExcel.readByJxl(4,0);
    }

    @Test
    public void testRead2() throws IOException {
        TestExcel testExcel = new TestExcel();
        testExcel.import2();
    }


}
