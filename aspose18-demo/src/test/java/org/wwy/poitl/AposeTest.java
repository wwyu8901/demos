package org.wwy.poitl;

import com.aspose.words.AutoFitBehavior;
import com.aspose.words.Document;
import com.aspose.words.Table;
import com.aspose.words.TableCollection;
import org.junit.Test;

import java.io.File;
import java.io.FileOutputStream;

public class AposeTest {

    /**
     * @param wordPath 需要被转换的word全路径带文件名
     * @param pdfPath 转换之后pdf的全路径带文件名
     */
    public  void doc2pdf(String wordPath, String pdfPath) throws Exception {
        long old = System.currentTimeMillis();
        File file = new File(pdfPath); //新建一个pdf文档
        FileOutputStream os = new FileOutputStream(file);
        Document doc = new Document(wordPath); //Address是将要被转化的word文档
        TableCollection tables = doc.getFirstSection().getBody().getTables();
        for (Table table : tables) {
//            table.autoFit(AutoFitBehavior.FIXED_COLUMN_WIDTHS);
        }
        doc.save(os, com.aspose.words.SaveFormat.PDF);//全面支持DOC, DOCX, OOXML, RTF HTML, OpenDocument, PDF, EPUB, XPS, SWF 相互转换
        long now = System.currentTimeMillis();
        os.close();
        System.out.println("共耗时：" + ((now - old) / 1000.0) + "秒"); //转化用时

    }

    @Test
    public void test() throws Exception {
        doc2pdf("d:/testa.doc","d:/testaaa.pdf");
    }
}
