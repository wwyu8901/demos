package org.wwy.poitl;

import com.spire.pdf.FileFormat;
import com.spire.pdf.PdfDocument;
import com.spire.pdf.PdfVersion;

public class ChangePdfVersion {

    public static void main(String[] args) {

        //创建PdfDocument对象
        PdfDocument document = new PdfDocument();
        
        //加载PDF文档
        document.loadFromFile("d:\\CreateWordXDDFChart.docx");

        //更改PDF版本到1.6
        document.getFileInfo().setVersion(PdfVersion.Version_1_6);
        
        //保存文档
        document.saveToFile("d:/aa.pdf", FileFormat.PDF);
        document.close();
    }
}