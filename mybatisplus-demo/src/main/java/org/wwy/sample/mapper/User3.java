package org.wwy.sample.mapper;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;

@TableName("t_user3")
public class User3 implements Serializable {
    @TableId(type = IdType.ASSIGN_UUID)
    private String id;

    private String name;

    private Integer age;

    @TableField(exist = false,updateStrategy = FieldStrategy.IGNORED)
    private String email;

    @TableField(exist = false,updateStrategy = FieldStrategy.IGNORED)
    private String email2;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail2() {
        return email2;
    }

    public void setEmail2(String email2) {
        this.email2 = email2;
    }
}