package org.wwy.sample.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.ResultHandler;

import java.util.List;

public interface UserMapper extends BaseMapper<User> {
    Page<User> selectPageVo(Page page);

    Page<User> selectPageVoOrder(Page page, @Param("orderBy")String orderBy, @Param("ew")QueryWrapper queryWrapper);

    List<User> selectPageVoList(Page page);

    List<User> selectListSleep();


    void selectList2(ResultHandler resultHandler);
}