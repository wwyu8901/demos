package org.wwy.sample.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.cursor.Cursor;

import java.util.List;

@Mapper
public interface MidL2PrdAstDtlMapper {

    /**
     * 通过prdCode，开始时间，结束时间，secuId来查询t_wght_aprc_nav
     * @param prodCode 组合代码
     * @param startDt 开始时间
     * @param endDt 结束时间
     * @param secuIdList 证券编码集合
     */
    List<MidL2PrdAstDtl> listTwghtAprcNavByPrdCode(@Param("prdCode") String prodCode,
                                                   @Param("startDt") String startDt,
                                                   @Param("endDt") String endDt,
                                                   @Param("secuIdList") List<String> secuIdList);

    Cursor<MidL2PrdAstDtl> cursorTwghtAprcNavByPrdCode(@Param("prdCode") String prodCode,
                                                       @Param("startDt") String startDt,
                                                       @Param("endDt") String endDt,
                                                       @Param("secuIdList") List<String> secuIdList);
}
