/**************************************************************************/
/*                                                                        */
/* Copyright (c) 2019 XunceTech Company                                   */
/* 深圳迅策科技有限公司版权所有                                                                                                                            */
/*                                                                        */
/* PROPRIETARY RIGHTS of XunceTech Company are involved in the            */
/* subject matter of this material. All manufacturing, reproduction, use, */
/* and sales rights pertaining to this subject matter are governed by the */
/* license agreement. The recipient of this software implicitly accepts   */
/* the terms of the license.                                              */
/* 本软件文档资料是深圳迅策科技有限公司的资产，任何人士阅读和                                                                    */
/* 使用本资料必须获得相应的书面授权，承担保密责任和接受相应的法律约束。                                                  */
/*                                                                        */
/**************************************************************************/

package org.wwy.sample.mapper;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;

/**
  * <pre>
  * @description: 产品树节点表实体类
  * @copyright: Copyright (c) 2019 迅策科技
  * @author: Chasel    
  * @version: 1.0 
  * @date: 2019-12-12 
  * @time: 21:53:32
  * </pre>
  */  
@TableName("PRD_TREE_NODE")
public class PrdTreeNode implements Serializable {
    
    private static final long serialVersionUID = 1L;
    

    /**
     * 
     * 
     * 数据库字段信息:CODE VARCHAR(64)
     */
    @TableField(value="code")
    private String code;

    /**
     * 
     * 
     * 数据库字段信息:NAME VARCHAR(100)
     */
    @TableField(value="NAME")
    private String name;

    /**
     * 字段名称：父包id
     * 
     * 数据库字段信息:PARENT_ID NUMERIC(19)
     */
    @TableField(value="PARENT_ID")
    private Long parentId;
    
    /**
     * 字段名称：树ID
     * 
     * 数据库字段信息:TREE_ID NUMERIC(19)
     */
    @TableField(value="TREE_ID")
    private Long treeId;

    /**
     * 字段名称：叶子节点标识  false-否 true-是
     * 
     * 数据库字段信息:TYPE CHAR(1)
     */
    
    @TableField(value="LEAF")
    private Boolean leaf;

    /**
     * 
     * 
     * 数据库字段信息:LEVEL NUMERIC(1)
     */
    @TableField(value="LVL")
    private Integer lvl;
    
    /**
     * 
     * 
     * 数据库字段信息:PARAMETERS VARCHAR2(255)
     */
    private String parameters;

    public PrdTreeNode() {
    }

    public String getCode() {
        return this.code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getParentId() {
        return this.parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public Boolean getLeaf() {
		return leaf;
	}

	public void setLeaf(Boolean leaf) {
		this.leaf = leaf;
	}

	public Integer getLvl() {
		return lvl;
	}

	public void setLvl(Integer lvl) {
		this.lvl = lvl;
	}

	public Long getTreeId() {
		return treeId;
	}

	public void setTreeId(Long treeId) {
		this.treeId = treeId;
	}

	public String getParameters() {
		return parameters;
	}

	public void setParameters(String parameters) {
		this.parameters = parameters;
	}
    
}