package org.wwy.sample.mapper;

import com.baomidou.mybatisplus.annotation.*;
import org.apache.ibatis.type.JdbcType;

import java.io.Serializable;

@TableName("t_user2")
public class User implements Serializable {

    @TableId(type = IdType.ASSIGN_ID)
    private Long id;

    private String name;

    private Integer age;

//    @TableField(exist = false,updateStrategy = FieldStrategy.IGNORED)
    private String email;

    @TableField(exist = false,updateStrategy = FieldStrategy.IGNORED)
    private String email2;

    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private TaskTimeoutStrategy taskTimeoutStrategy;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail2() {
        return email2;
    }

    public void setEmail2(String email2) {
        this.email2 = email2;
    }

    public TaskTimeoutStrategy getTaskTimeoutStrategy() {
        return taskTimeoutStrategy;
    }

    public void setTaskTimeoutStrategy(TaskTimeoutStrategy taskTimeoutStrategy) {
        this.taskTimeoutStrategy = taskTimeoutStrategy;
    }
}