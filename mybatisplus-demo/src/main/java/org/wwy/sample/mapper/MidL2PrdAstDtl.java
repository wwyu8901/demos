package org.wwy.sample.mapper;


import java.math.BigDecimal;


public class MidL2PrdAstDtl {

    private String prdCode;        // 组合代码
    private String prdName;        // 组合名称

    /**
     * T日权重（全价）_占总净值
     */
    private BigDecimal wghtAprcNav;

    public String getPrdCode() {
        return prdCode;
    }

    public void setPrdCode(String prdCode) {
        this.prdCode = prdCode;
    }

    public String getPrdName() {
        return prdName;
    }

    public void setPrdName(String prdName) {
        this.prdName = prdName;
    }

    public BigDecimal getWghtAprcNav() {
        return wghtAprcNav;
    }

    public void setWghtAprcNav(BigDecimal wghtAprcNav) {
        this.wghtAprcNav = wghtAprcNav;
    }
}
