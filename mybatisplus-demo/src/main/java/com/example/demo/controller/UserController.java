package com.example.demo.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zaxxer.hikari.HikariDataSource;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.wwy.sample.mapper.User;
import org.wwy.sample.mapper.UserMapper;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@RequestMapping("/user")
@RestController
public class UserController {
    private Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserMapper userMapper;

//    @Autowired
//    private DruidDataSource dataSource;

    @Autowired
    private HikariDataSource dataSource;

    @RequestMapping("list")
    public List<User> getAllUser(){
        Page<User> userPage = new Page<>(1,10);
        Page<User> result = userMapper.selectPage(userPage,null);
        List<User> records = result.getRecords();
        for (User record : records) {
            System.out.printf("record==="+record);
        }
        return records;
    }

    @RequestMapping("list2")
    public List<User> getAllUser2(){
        Page<User> userPage = new Page<>(1,10);
        List<User> records = userMapper.selectListSleep();
        for (User record : records) {
            System.out.printf("record==="+record);
        }
        return records;
    }


    /*@GetMapping("/pool")
    public Map getHikariPool(){
        int activeCount = dataSource.getActiveCount();
        int maxActive = dataSource.getMaxActive();
        int waitThreadCount = dataSource.getWaitThreadCount();
        logger.info("活跃连接数: {}" , activeCount);
        Map map = new HashMap();
        map.put("活跃连接数",activeCount);
        map.put("最大连接数",maxActive);
        map.put("等待线程数",waitThreadCount);
        return map;
    }*/

    @GetMapping("/hikari-pool")
    public Map getHikariPool(){
        int activeCount = dataSource.getHikariPoolMXBean().getActiveConnections();
        int maxActive = dataSource.getMaximumPoolSize();
        int waitThreadCount = dataSource.getHikariPoolMXBean().getThreadsAwaitingConnection();
        logger.info("活跃连接数: {}" , activeCount);
        Map map = new HashMap();
        map.put("活跃连接数",activeCount);
        map.put("最大连接数",maxActive);
        map.put("等待线程数",waitThreadCount);
        return map;
    }


}
