package com.example.demo.test;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.toolkit.SqlRunner;
import org.apache.ibatis.cursor.Cursor;
import org.apache.ibatis.parsing.GenericTokenParser;
import org.apache.ibatis.parsing.PropertyParser;
import org.apache.ibatis.parsing.TokenHandler;
import org.apache.ibatis.session.ResultContext;
import org.apache.ibatis.session.ResultHandler;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.test.annotation.Commit;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StopWatch;
import org.wwy.sample.mapper.*;

import javax.sql.DataSource;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.*;
import java.util.*;

import static org.springframework.transaction.TransactionDefinition.PROPAGATION_REQUIRES_NEW;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SampleTest {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private User3Mapper user3Mapper;

    @Autowired
    private User4Mapper user4Mapper;

    @Autowired
    private MidL2PrdAstDtlMapper midL2PrdAstDtlMapper;

    @Autowired
    private DataSource datasource;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Test
    public void testSelect() {
        System.out.println(("----- selectAll method test ------"));
        List<User> users = new ArrayList<>();
        userMapper.selectList2((ResultHandler<User>) resultContext -> {
            System.out.println("================================");
            User resultObject = resultContext.getResultObject();
            int age = resultObject.getAge();
            if(age > 11){
                users.add(resultObject);
            }
        });
        users.forEach(System.out::println);
        List<Map<String, Object>> maps = jdbcTemplate.queryForList("select 1 from dual");
        System.out.println(maps);
    }

    @Test
    public void testSelectPage() {
        System.out.println(("----- selectAll method test ------"));
        Page<User> userPage = new Page<>(2,2);
        Page<User> result = userMapper.selectPage(userPage,null);
        List<User> records = result.getRecords();
        for (User record : records) {
            System.out.printf("record==="+record);
        }


    }



    @Test
    public void testSelectPage2() {
        System.out.println(("----- selectAll method test ------"));
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.like("email","一");
//        queryWrapper.le("id",3);
        Page page = new Page(1,2);


        String orderBy = "name";
        List<User> users = userMapper.selectList(queryWrapper);
        System.out.println(JSON.toJSON(users));

    }


    @Test
    @Transactional()
    @Commit
    public void testInsert() {
        System.out.println(("----- selectAll method test ------"));
        User user = new User();
        user.setAge(20);
        user.setEmail("一二三四五六七八九零一二三四五六七八九零一二三四五六七八九零一二三四五六七八九零一二三四五六七八九零一二三四五六七八九零一二三四五六七八九零一二三四五六七八九零一二三四五六七八九零一二三四五六七八九零");
//        user.setId(3L);
        user.setName("abcd");
        userMapper.insert(user);
    }

    @Test
    @Transactional()
    @Commit
    public void testSimpleInsert() {
        System.out.println(("----- selectAll method test ------"));
        User user = new User();
        user.setAge(10);
        user.setEmail("1234567");
//        user.setId(4L);
        user.setName("a12");
        TaskTimeoutStrategy strategy = TaskTimeoutStrategy.FAILED;
        user.setTaskTimeoutStrategy(strategy);
        userMapper.insert(user);
    }

    @Test
    @Transactional()
    @Commit
    public void testSimpleInsert4() {
        System.out.println(("----- selectAll method test ------"));
        User4 user = new User4();
        user.setAge(10);
        user.setEmail("1234567");
//        user.setId(3L);
        user.setName("a12");
        user4Mapper.insert(user);
    }

    @Test
    @Transactional()
    @Commit
    public void testUpdate() {
        System.out.println(("----- selectAll method test ------"));
        User user = new User();
        user.setId(1797548188356734977L);
        user.setAge(10);
        user.setEmail("1234567");
//        user.setId(3L);
        user.setName("a1");
//        TaskTimeoutStrategy strategy = TaskTimeoutStrategy.FAILED;
//        user.setTaskTimeoutStrategy(strategy);
        userMapper.updateById(user);
    }


    @Test
    @Transactional()
    @Commit
    public void testInsert3() {
        User3 user = new User3();
        user.setAge(11);
        user.setEmail("123456");
//        user.setId(3L);
        user.setName("aa");
        user3Mapper.insert(user);
    }

    @Test
    @Transactional(readOnly = true)
    public void testSelectMidL2() {
        System.out.println(("----- selectAll method test ------"));
//        PageHelper.startPage(2,2);
        StopWatch stopWatch = new StopWatch("aaaa");
        stopWatch.start("aaa");
        BigDecimal total = new BigDecimal(0);
        try (Cursor<MidL2PrdAstDtl> cursor = midL2PrdAstDtlMapper.cursorTwghtAprcNavByPrdCode("882603", "20180101", "20220630", null)) {  // 1
            cursor.forEach(foo -> {
                BigDecimal cashIn = foo.getWghtAprcNav();
                if(cashIn != null){
                    total.add(cashIn.abs());
                }

            });                      // 2
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        System.out.println("求和结果:"+total);
        stopWatch.stop();
        stopWatch.prettyPrint();
    }

    @Test
    @Transactional(readOnly = true)
    public void testSelectMidL3() {
        System.out.println(("----- selectAll method test ------"));
//        PageHelper.startPage(2,2);
        StopWatch stopWatch = new StopWatch("aaaa");
        stopWatch.start("aaa");
        BigDecimal total = new BigDecimal(0);

        List<MidL2PrdAstDtl> midL2PrdAstDtls = midL2PrdAstDtlMapper.listTwghtAprcNavByPrdCode("882603", "20180101", "20220630", null);
        for(MidL2PrdAstDtl midL2PrdAstDtl : midL2PrdAstDtls){
            BigDecimal cashIn = midL2PrdAstDtl.getWghtAprcNav();
            if(cashIn != null){
                total.add(cashIn.abs());
            }
        }
        System.out.println("求和结果:"+total);
        stopWatch.stop();
        stopWatch.prettyPrint();
    }


    @Test
    public void testJdbc() throws SQLException {
        Connection connection = datasource.getConnection();
        StopWatch stopWatch = new StopWatch("aaaa");

        try {
            stopWatch.start("111");
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT T_WGHT_APRC_NAV WGHT_APRC_NAV, SECU_ID, T_DATE FROM MID_L2_PRD_AST_DTL WHERE prd_code = ? and t_date>=? and rownum<=10000");
            preparedStatement.setObject(1, "882603");
            preparedStatement.setObject(2, "20180101");
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.setFetchSize(1000);
            stopWatch.stop();
            stopWatch.start("222");
            int count = 0;
            while (resultSet.next()) {
                resultSet.getBigDecimal("WGHT_APRC_NAV");
                count++;
            }
            System.out.println(count);
            stopWatch.stop();
        }finally {
            connection.close();
        }
        System.out.println(stopWatch.getTotalTimeMillis());
        System.out.println(stopWatch.prettyPrint());
    }

    @Test
    public void testUser(){
        List<User> users = userMapper.selectList(null);
        System.out.println(JSON.toJSONString(users));
    }

    @Test
    public void testWrapper(){
        QueryWrapper w = new QueryWrapper();
        w.like("name","2");
        w.orderByAsc("name");
        List list = userMapper.selectList(w);
        System.out.println(JSON.toJSON(list));
    }

    @Test
    public void test(){
        Map<String,String> params = new HashMap<>();
        params.put("aaa","bbb");
        String startToken = "@{";
        String endToken = "}";
        GenericTokenParser parser = new GenericTokenParser(startToken, endToken, content -> {
            String value = params.get(content);
            if(value != null){
                return value;
            }
            return startToken+content+endToken;
        });
        String str = parser.parse("select * from @{aaa} where cc=@{ert} and 1=2");
        System.out.println(str);
    }


    @Test
    public void testSelect2() {
        System.out.println(("----- selectAll method test ------"));
        Page<User> userPage = new Page<>(2,2);
        Page<User> userList = userMapper.selectPageVo(userPage);
        System.out.println(JSON.toJSON(userList));
    }


}