package org.wwy.metadata;

import org.junit.Ignore;
import org.junit.Test;

import java.sql.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Ignore
public class MetaData2ParamGeneratorTest {

    private static Pattern LINE_PATTERN = Pattern.compile("_(\\w)");


    //自动生成java属性的方法
    @Test
    public void test() throws SQLException {
        Connection connection =
                DriverManager.getConnection("jdbc:oracle:thin:@//192.168.20.71:1521/orcl",
                        "cmsindicators_f72", "cmsindicators_f72");
        String result = getParamScript(connection, "select secu_id,typ_code,inv_typ,upd_time,busi_date,t_date tdate from mid_l2_prd_thr_ast_dtl");
        System.out.println(result);
    }


    /**
     *
     * @param connection 连接
     * @param sql
     * @throws SQLException
     */
    private String getParamScript(Connection connection, String sql) throws SQLException {
        StringBuffer result = new StringBuffer();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            ResultSet rs = preparedStatement.executeQuery();


            ResultSetMetaData metaData = rs.getMetaData();
            int columnCount = metaData.getColumnCount();
            //遍历字段
            for (int i=0;i<columnCount;i++){
                //字段类型
                String columnTypeFullName = metaData.getColumnClassName(i + 1);
                int dotIndex = columnTypeFullName.lastIndexOf(".");
                //字段类型简称(不含package)
                String simpleType;
                if(dotIndex>=0){
                    simpleType = columnTypeFullName.substring(dotIndex+1);
                }else{
                    simpleType = columnTypeFullName;
                }
                String columnName = metaData.getColumnName(i + 1);
                result.append("private "+simpleType + " " + snakeToCamel(columnName) +";")
                        .append("\n\n");
            }

        } finally {
            //关闭连接
            if (connection != null) {
                connection.close();
            }
        }
        return result.toString();

    }

    /** 下划线转驼峰 */
    private static String snakeToCamel(String str) {
        str = str.toLowerCase();
        Matcher matcher = LINE_PATTERN.matcher(str);
        StringBuffer sb = new StringBuffer();
        while (matcher.find()) {
            matcher.appendReplacement(sb, matcher.group(1).toUpperCase());
        }
        matcher.appendTail(sb);
        return sb.toString();
    }

}
