package org.test;

import com.alibaba.druid.pool.DruidDataSource;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DruidTest {

    //数据源配置
    private DruidDataSource dataSource = new DruidDataSource();

    public DruidTest() throws SQLException {
        dataSource.setUrl("jdbc:clickhouse://192.168.0.171:8123/mid_test");
//        dataSource.setDriverClassName("com.mysql.cj.jdbc.Driver"); //这个可以缺省的，会根据url自动识别
//        dataSource.setUsername("root");
//        dataSource.setPassword("abcd");

        dataSource.setTestOnBorrow(false);
        dataSource.setTestWhileIdle(false);
        //下面都是可选的配置
        dataSource.setInitialSize(20);  //初始连接数，默认0
        dataSource.setMaxActive(30);  //最大连接数，默认8
        dataSource.setMinIdle(10);  //最小闲置数
        dataSource.setMaxWait(2000);  //获取连接的最大等待时间，单位毫秒
        dataSource.setPoolPreparedStatements(true); //缓存PreparedStatement，默认false
        dataSource.setMaxOpenPreparedStatements(20); //缓存PreparedStatement的最大数量，默认-1（不缓存）。大于0时会自动开启缓存PreparedStatement，所以可以省略上一句代码
        dataSource.init();
    }

    public static void main(String[] args) throws SQLException, InterruptedException {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                DruidTest druidTest = null;
                try {
                    druidTest = new DruidTest();
                    for(int i=0;i<2000;i++){
                        druidTest.execute("select T_DATE,PONT_RAT,BASI_CODE from MID_L2_BASI WHERE BASI_CODE='119655' OR BASI_CODE='119525'");
                    }
                } catch (SQLException sqlException) {
                    sqlException.printStackTrace();
                }

            }
        };
        Thread thread=new Thread(runnable);

        for(int i=0;i<20;i++) {
            thread.start();

        }
        Thread.sleep(2000000L);

    }

    public void execute(String sql){
        Connection connection = null;
        try {
            long start = System.currentTimeMillis();
            //获取连接
            connection = dataSource.getConnection();

            long end = System.currentTimeMillis();
            System.out.println(Thread.currentThread()+"====连接耗时===="+(end-start));
            //PreparedStatement接口
            long start2 = System.currentTimeMillis();
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            ResultSet resultSet = preparedStatement.executeQuery();
            long end2 = System.currentTimeMillis();
            System.out.println("=====查询耗时========"+(end2-start2));

        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        } finally {
            if(connection != null){
                //关闭连接
                try {
                    connection.close();
                } catch (SQLException sqlException) {

                }
            }

        }

    }

    public Connection getConnection(){
        Connection connection = null;
        try {
            long start = System.currentTimeMillis();
            //获取连接
            connection = dataSource.getConnection();

            long end = System.currentTimeMillis();
            System.out.println(Thread.currentThread()+"====连接耗时===="+(end-start));
            return connection;

        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
        return null;

    }
}
