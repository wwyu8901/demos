package org.test;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Instant;
import org.joda.time.format.DateTimeFormat;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author junfeng.chen@xuncetech.com
 * @date 2019-07-30
 */
public class JodaUtils {

    /**
     * 初始化日期（测试环境日期全为20090109）
     */
    private static final String DEFAULT_DATE = "20090109";

    private static final String DEFAULT_DATE_TIME_FORMAT = "yyyy/MM/dd HH:mm:ss";

    private static final String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm";

    private static final String DATE_TIME_MS_FORMAT = "yyyy/MM/dd HH:mm:ss.SSS";

    private static final String DATE_FORMAT = "yyyy/MM/dd";

    private static final String DATE_FORMAT2 = "yyyyMMdd";

    private static final String DATE_FORMAT2_1 = "yyyyMM";

    private static final Integer DATE_FORMAT2_LENGTH = 8;

    private static final String DATE_FORMAT3 = "yyyy-MM-dd";

    private static final String DATE_FORMAT_YYMMDD = "yyMMdd";

    private static final String TIME_FORMAT = "HHmmss";

    private static final String TIME_FORMAT2 = "HHmm";

    private static final String TIME_FORMAT3 = "HH";

    private static final String TIME_FORMAT4 = "HH:mm:ss";

    private static final String DATE_TIME_FORMAT2 = "yyyyMMddHHmmss";

    private static final String DATE_TIME_FORMAT3 = "yyyyMMddhhmmssSSS";

    private static final String DATE_TIME_FORMAT4 = "yyMMddhhmmssSSS";

    private static final String DATE_TIME_FORMAT5 = "yyyy-MM-dd HH:mm:ss";


    /**
     * 获取当前系统毫秒数
     */
    public static long getMillis() {
        return new Instant().getMillis();
    }

    /**
     * 按照 默认时间格式 获取当前系统时间 yyyy/MM/dd HH:mm:ss
     */
    public static String getCurrentDefaultDateTimeFormat() {
        return DateTimeFormat.forPattern(DEFAULT_DATE_TIME_FORMAT).print(getMillis());

    }

    public static String getCurrentDefaultDateTimeFormat5() {
        return DateTimeFormat.forPattern(DATE_TIME_FORMAT5).print(getMillis());

    }

    /**
     * 按照 传入毫秒数 返回格式化日期时间 yyyy/MM/dd HH:mm:ss
     */
    public static String getDefaultDateTimeFormat(long millis) {
        return DateTimeFormat.forPattern(DEFAULT_DATE_TIME_FORMAT).print(millis);
    }


    /**
     * 获取当前日期 yyyyMMdd
     */
    public static Integer getCurrentDateFormat() {
        return Integer.valueOf(DateTimeFormat.forPattern(DATE_FORMAT2).print(getMillis()));
    }

    /**
     * 获取当前日期 yyyyMMdd
     */
    public static String getCurrentDateFormat2() {
        return DateTimeFormat.forPattern(DATE_FORMAT2).print(getMillis());
    }

    /**
     * 获取当前日期 yyyyMM
     */
    public static String getCurrentDateFormat2_1() {
        return DateTimeFormat.forPattern(DATE_FORMAT2_1).print(getMillis());
    }


    /**
     * 根据字符串日期 yyyy-MM-dd 获取 Date
     */
    private static Date getDateFormat3(String date) {
        return DateTimeFormat.forPattern(DATE_FORMAT3).parseDateTime(date).toDate();
    }

    /**
     * 根据字符串日期 yyyy-MM-dd 获取 String
     */
    public static String getDateFormat() {
        return DateTimeFormat.forPattern(DATE_FORMAT3).print(getMillis());
    }

    /**
     *
     * 获取当前日期 yyMMdd
     */
    public static String getCurrentDateFormat4(){
        return DateTimeFormat.forPattern(DATE_FORMAT_YYMMDD).print(getMillis());
    }

    /**
     * 获取两个日期差多少天
     */
    public static Integer getDaysBetween(String date1, String data2) {
        return Days.daysBetween(DateTime.parse(date1, DateTimeFormat.forPattern(DATE_FORMAT2)), DateTime.parse(data2, DateTimeFormat.forPattern(DATE_FORMAT2))).getDays();
    }

    /**
     * 根据日期字符串 获取 某个时间加几年 yyyy-MM-dd
     */
    public static String getDateAddYear(String date, int addYear) {
        return new DateTime(date).plusYears(addYear).toString(DATE_FORMAT3);
    }

    /**
     * 根据8位 Integer数字 20190912 获取 某个时间加几年后日期字符串 yyyy-MM-dd
     */


    /**
     * 根据日期字符串 获取 某个时间加几个月后日期字符串 yyyy-MM-dd
     */
    public static String getDateAddMonth(String date, int addMonth) {
        return new DateTime(date).plusMonths(addMonth).toString(DATE_FORMAT3);
    }

    /**
     * 根据日期字符串 获取 某个时间加几个月后日期字符串 yyyy-MM-dd
     */
    public static Integer getDateAddDay(Integer date, int addDay) {
        SimpleDateFormat df = new SimpleDateFormat(DATE_FORMAT2);
        Integer time;
        try {
            time = Integer.parseInt(df.format(new Date(df.parse(date + "").getTime() + addDay * 24 * 60 * 60 * 1000)));
            return time;
        } catch (ParseException e) {
            e.printStackTrace();
            return Integer.parseInt(getCurrentDateFormat2());
        }

    }

    /**
     * 根据8位 Integer数字 20190912 获取 某个时间加几个月后日期字符串 yyyy-MM-dd
     */




    /**
     * 根据8位 Integer数字 20190912 获取时间字符串 yyyy-MM-dd
     */
    public static String getDateFormat2(Integer dateNum) {
        String dateNumStr = String.valueOf(dateNum);
        if (dateNum == null || dateNumStr.length() != DATE_FORMAT2_LENGTH) {
            return "";
        }
        return StringUtils.join(dateNumStr.substring(0, 4), "-", dateNumStr.substring(4, 6), "-", dateNumStr.substring(6, 8));
    }

    /**
     * 获取当前时间 HHmmss
     */
    public static Integer getTimeFormat(){
        return Integer.parseInt(DateTimeFormat.forPattern(TIME_FORMAT).print(getMillis()));
    }

    /**
     * 获取当前时间 HHmm
     */
    public static String getTimeFormat2() {
        return DateTimeFormat.forPattern(TIME_FORMAT2).print(getMillis());
    }

    /**
     * 获取当前时间 HH
     */
    public static String getTimeFormat3() {
        return DateTimeFormat.forPattern(TIME_FORMAT3).print(getMillis());
    }

    /**
     * 获取当前时间 HH:mm:ss
     */
    public static String getTimeFormat4() {
        return DateTimeFormat.forPattern(TIME_FORMAT4).print(getMillis());
    }



    /**
     * 获取距离当前日期 months 个月前的日期 yyyyMMdd
     */
    public static String getMinusMonths(int months){
        return new DateTime().minusMonths(months).toString(DATE_FORMAT2);
    }

    /**
     * 获取距离当前日期 months 个月前的日期 yyyyMM
     */
    public static String getMinusMonths1(int months){
        return new DateTime().minusMonths(months).toString(DATE_FORMAT2_1);
    }

    /**
     * 获取距离当前日期 years 个年前的日期 yyyyMMdd
     */
    public static String getMinusYears(int years){
        return new DateTime().minusYears(years).toString(DATE_FORMAT2);
    }

    /**
     * 获取距离当前日期 days 个天前的日期 yyyyMMdd
     */
    public static String getMinusDays(int days){
        return new DateTime().minusDays(days).toString(DATE_FORMAT2);

    }
    /**
     * 获取距离当前日期 days 个天后的日期 yyyyMMdd
     */
    public static String getPlusDays(int days){
        return new DateTime().plusDays(days).toString(DATE_FORMAT2);

    }

    /**
     * 获取距离指定日期 days 个天后的日期 yyyyMMdd
     */
    public static String getPlusDays(String date, int days){
        return DateTime.parse(date, DateTimeFormat.forPattern(DATE_FORMAT2)).plusDays(days).toString(DATE_FORMAT2);

    }

    /**
     * 从配置文件获取计算日期，配置长度为8时返回配置日期，否则返回当前日期
     */
    public static String getDateByConf(String confDate) {
            return confDate.length()==8?confDate:getCurrentDateFormat2();
    }

    /**
     * 从配置文件获取计算时分秒，配置长度为6时返回配置时间，否则返回当前时间
     */
    public static Integer getTimeByConf(String confTime) {
        return confTime.length()==6?Integer.valueOf(confTime):getTimeFormat();
    }


    public static void main(String[] args){
//        System.out.println(JodaUtils.getMinusMonths(6));
//        System.out.println(JodaUtils.getMinusYears(1));
//
//        Integer format = getCurrentDateFormat();
////        System.out.println(format);
//        String format5 = getCurrentDefaultDateTimeFormat5();
//        System.out.println(format5);
//        String replace = getCurrentDefaultDateTimeFormat().replace("/", "-");
//        System.out.println(replace);


        System.out.println(getMinusMonths1(0));
    }

}
