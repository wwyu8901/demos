package org.test;


import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class CodeScan {
    public static void main(String[] args) throws IOException {
        /*String scanFolder = "D:\\workspace\\demos\\springboot-demo\\springboot-demo2";
        File file = new File(scanFolder);
        if(file.isDirectory()){
            File[] files = file.listFiles();
            for (File subFile : files) {
                if(subFile.getName().endsWith(".java")){
                    new S(file);
                }
            }

        }*/
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-DD HH:mm:ss");
        String format = simpleDateFormat.format(new Date());
        System.out.println(format);

        String format1 = new SimpleDateFormat("YYYY-MM-DD HH:mm:ss").format(new Date());
        System.out.println(format1);
    }
}
