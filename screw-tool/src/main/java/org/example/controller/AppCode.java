package org.example.controller;

public interface AppCode {
    
    /**
     * @description：返回业务代码
     * @return
     */
    public String getCode();

    /**
     * @description：设置业务代码
     * @param code  业务代码
     */
    public void setCode(String code);

    /**
     * @description：返回业务代码对应描述信息
     * @return
     */
    public String getMessage();

    /**
     * @description：设置业务代码对应描述信息
     * @param message
     */
    public void setMessage(String message);

    /**
     * @description：是否成功
     * @date：2019年4月19日 上午10:54:02
     * @return
     */
    default boolean isSuccess() {
        return "0".equals(getCode());
    }
}
