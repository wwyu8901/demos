package org.example.controller;

import lombok.Data;

@Data
public class DataSourceDto {

    private String url;

    private String password;

    private String userName;

}
