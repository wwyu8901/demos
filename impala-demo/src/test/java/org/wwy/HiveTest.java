package org.wwy;


import com.cloudera.impala.jdbc.Driver;
import org.junit.Test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;


public class HiveTest {

    @Test
    public void test4() throws ClassNotFoundException {
        String connectionUrl = "jdbc:impala://192.168.0.171:21050/dwd_smr";
//        Class.forName("com.cloudera.impala.jdbc.Driver");
        try (Connection con = DriverManager.getConnection(connectionUrl)) {
/*            ResultSet schemas = con.getMetaData().getSchemas();
            while (schemas.next()) {
                System.out.println(schemas.getString(1));
            }*/
            System.out.println();
            ResultSet rs = con.getMetaData().getTables(null, null, null, null);
            while (rs.next()) {
                ResultSetMetaData metaData = rs.getMetaData();
                for(int i=0;i<metaData.getColumnCount();i++){
                    Object object = rs.getObject(i + 1);
                    System.out.println(metaData.getColumnName(i+1)+":"+object+"  ");
                }
                System.out.println();
            }

            /*ResultSet columns = con.getMetaData().getColumns(con.getCatalog(), "dwd_smr", "fin_prd_hld_info", "%");
            while (columns.next()) {
                ResultSetMetaData metaData = columns.getMetaData();
                for(int i=0;i<metaData.getColumnCount();i++){
                    Object object = columns.getObject(i + 1);
                    System.out.println(metaData.getColumnName(i+1)+":"+object+"  ");
                }
                System.out.println();
            }*/
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
