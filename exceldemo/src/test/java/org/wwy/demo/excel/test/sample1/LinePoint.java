package org.wwy.demo.excel.test.sample1;


public class LinePoint {
    private String xValue;

    private Number yValue;


    public LinePoint(String xValue, Number yValue) {
        this.xValue = xValue;
        this.yValue = yValue;
    }

    public String getxValue() {
        return xValue;
    }

    public Number getyValue() {
        return yValue;
    }

}
