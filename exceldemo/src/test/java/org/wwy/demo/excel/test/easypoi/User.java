package org.wwy.demo.excel.test.easypoi;

import cn.afterturn.easypoi.excel.annotation.Excel;

import java.math.BigDecimal;

public class User {

    @Excel(name = "用户编号")
    private String id;
    @Excel(name = "用户账号")
    private String username;
    @Excel(name = "用户电话")
    private String mobile;

    @Excel(name = "年龄",type = 10)
    private int age;

    @Excel(name = "财产",numFormat = "#,###.00")
    private BigDecimal money;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }
}
