package org.wwy.demo.excel.test.easypoi;

import java.util.List;

public class DataObj {

    private List<FundInfo> records;

    public List<FundInfo> getRecords() {
        return records;
    }

    public void setRecords(List<FundInfo> records) {
        this.records = records;
    }
}
