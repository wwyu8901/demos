package org.wwy.demo.excel.test;

import cn.hutool.core.date.Quarter;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.DateUtil;
import org.junit.Test;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class StreamTest {
    public static final ThreadLocal<DateFormat> D_1 = ThreadLocal.withInitial(
            () -> new SimpleDateFormat("yyyyMMdd"));
    @Test
    public void test(){
        List<ContrastFundAllocationVO> plateList = new ArrayList<>();
        ContrastFundAllocationVO data = new ContrastFundAllocationVO();
        data.setName("a");
        plateList.add(data);
        ContrastFundAllocationVO data2 = new ContrastFundAllocationVO();
//        data2.setName("b");
        Map<String, List<ContrastFundAllocationVO>> plateMap = plateList.stream().filter(t-> StringUtils.isNotEmpty(t.getName())).collect(Collectors.groupingBy(ContrastFundAllocationVO::getName));

        System.out.println(plateMap);
    }

    @Test
    public void test2(){
        List<ContrastFundAllocationVO> plateList = new ArrayList<>();
        ContrastFundAllocationVO data = new ContrastFundAllocationVO();
        data.setName("a");
        data.setDate("123");

        ContrastFundAllocationVO data2 = new ContrastFundAllocationVO();
        data2.setName("a");
        data2.setDate("1234");
        plateList.add(data2);
        plateList.add(data);
        Map<String, ContrastFundAllocationVO> collect = plateList.stream().collect(Collectors.toMap(ContrastFundAllocationVO::getDate, item -> item));
        System.out.println(collect);
    }

}
