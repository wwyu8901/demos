package org.wwy.demo.excel.test.easypoi;

public class ResponseData<T> {
    private String code;
    private String msg;
    private T data;


    public ResponseData(String code, String message) {
        this.code = code;
        this.msg = message;
    }

    public ResponseData(String code, String message, T data) {
        this.code = code;
        this.msg = message;
        this.data = data;
    }


    public String getCode() {
        return this.code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return this.msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return this.data;
    }

    public ResponseData<T> setData(T data) {
        this.data = data;
        return this;
    }

    public void responseCode(String code, String message) {
        this.code = code;
        this.msg = message;
    }

}
