package org.wwy.demo.excel.test.graph;

import java.io.*;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Test;
import org.wwy.demo.excel.Coordinate;
import org.wwy.demo.excel.graph.GraphInstance;
import org.wwy.demo.excel.graph.Table1GraphGeneratorImpl;

public class MyBarChartTest {


    @Test
    public void test2() throws IOException {
        String filePath = "d:/mygptmp2.xlsx";
        FileInputStream fileInputStream = new FileInputStream(new File(filePath));
        XSSFWorkbook workbook = new XSSFWorkbook(fileInputStream);
        XSSFSheet sheet = workbook.getSheetAt(0);

        //图表区域
        CellRangeAddress graphRange = new CellRangeAddress(4, 20, 0, 10);
        Table1GraphGeneratorImpl table1GraphGenerator = new Table1GraphGeneratorImpl(sheet, graphRange,
                "日成交量（亿股）");
        //以下所有序列号都从0开始
        //折线图上主题所在列序列号,如上证50、中证500指数、沪深300指数
        table1GraphGenerator.setGroupCol(10);
        //x轴序列号
        table1GraphGenerator.setXdataCol(11);
        //y轴序列号
        table1GraphGenerator.setYdataCol(13);
        //数据开始行
        table1GraphGenerator.setDataStartRow(4);
        //数据结束行
        table1GraphGenerator.setDataEndRow(104);
        table1GraphGenerator.generate();

        File file = new File("D:\\result.xlsx");
        FileOutputStream fout = null;
        try {
            fout = new FileOutputStream(file);
            workbook.write(fout);
        } finally {
            if (fout != null) {
                fout.close();
            }
            fileInputStream.close();
        }

    }

}

