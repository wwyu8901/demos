package org.wwy.demo.excel.test;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

@Data
public class TBondPool {

    private Long id;
    @JSONField(name = "C_MARKET_NO")
    private Long market;
    @JSONField(name = "VC_INTER_CODE")
    private String innerCode;
    @JSONField(name = "VC_REPORT_CODE")
    private String stockCode;
    @JSONField(name = "VC_STOCK_NAME")
    private String shortName;
    @JSONField(name = "VC_STOCK_FULLNAME")
    private String bondName;
    @JSONField(name = "L_PUBLISH_DATE")
    private String issueStartDate;
    @JSONField(name = "L_LISTING_DATE2")
    private String issueEndDate;
    @JSONField(name = "L_LISTING_DATE")
    private String listingDate;
    @JSONField(name = "EN_PUBLISH_PRICE")
    private double issuePrice;
    @JSONField(name = "EN_FACE_PRICE")
    private double faceValue;
    @JSONField(name = "C_INTEREST_TYPE")
    private String interestRateType;
    @JSONField(name = "C_DRAWBENCHMRK")
    private String interestCalcType;
    @JSONField(name = "EN_PAY_INTEVAL")
    private String interestPeriod;
    @JSONField(name = "VC_APPRAISE_ORGAN")
    private String ratingOrg;
    @JSONField(name = "C_INSIDE_APPRAISE")
    private String internalBondLevel;
    @JSONField(name = "C_OUTER_APPRAISE")
    private String bondLevel;
    @JSONField(name = "issuer_level")
    private String issuerLevel;
    @JSONField(name = "internal_issuer_level")
    private String internalIssuerLevel;
    private String creditIssuerLevel;
    @JSONField(name = "issue_rating_org")
    private String issueRatingOrg;
    private String lastIssuerRating;
    @JSONField(name = "VC_FULL_NAME")
    private String issueName;
    @JSONField(name = "VC_ISSUER_NAME")
    private String issueShortName;
    private String researcher;
    @JSONField(name = "EN_YEAR_RATE")
    private double faceRate;
    @JSONField(name = "EN_BASIC_RATE")
    private double interestDiff;
    @JSONField(name = "C_BASIC_INTEREST_TYPE")
    private String benchmarkRate;
    @JSONField(name = "ENTERPRISE_TYPE")
    private String enterpriseType;
    private String status;
    @JSONField(name = "C_BID_TARGET")
    private String subject;
    @JSONField(name = "C_BID_TYPE")
    private String tenderType;
    private double subjectSectionStart;
    private double subjectSectionEnd;
    @JSONField(name = "L_BEGIN_BID_DATE")
    private String inviteBeginTime;
    @JSONField(name = "L_END_BID_DATE")
    private String inviteEndTime;
    private String accountManager;
    private String leadUnderwriter;
    private String subUnderwriter;
    private String distributionUnderwriter;
    @JSONField(name = "L_PAY_LIMIT_DATE")
    private String payTime;
    @JSONField(name = "bond_type")
    private String bondType;
    @JSONField(name = "rated")
    private Long rated;
    private Long workflowed;
    @JSONField(name = "issue_type")
    private String issueType;
    private Long rightStatus;
    private Long deleted;
    private double baseIssueAmount;
    @JSONField(name = "L_ISSUE_AMOUNT")
    private double planIssueAmount;
    private String issuerLevelChange;
    private String issuerLevelProspect;
    private String creditIncreaseType;
    private String lowestIssuerLevel;
    private String industryType;
    @JSONField(name = "VC_PROVINCECODE")
    private String issuerProvince;
    private String specialTerm;
    private String payOrder;
    private String subOrMix;
    private String callBack;
    @JSONField(name = "L_LOT_DATE")
    private String winningDate;
    private String announceDate;
    private String isListed;
    private String isMarketCross;
    private String guarantor;
    private String guarantorLevel;
    private String tenderOrg;
    @JSONField(name = "VC_MOTHER_COMPANY_IDS")
    private String originEquity;

    private String originEquityName;

    private String remark;
    private String raiseType;
    private String isPandaDebt;
    private String isOverConfig;
    @JSONField(name = "L_END_DATE")
    private String expireDate;
    @JSONField(name = "L_BEGINCAL_DATE")
    private String interestStartDate;
    @JSONField(name = "EN_EXIST_LIMITE")
    private String deadline;
    @JSONField(name = "deadline_unit")
    private String deadlineUnit;
    private String sustainable;
    private String operator;
    private Long createDate;
    private String createTime;
    private String guaranteeTerm;
    private String businessKey;
    @JSONField(name="C_TRUSTEE")
    private Integer custodian;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getInnerCode() {
        return innerCode;
    }

    public void setInnerCode(String innerCode) {
        this.innerCode = innerCode;
    }

    public Long getMarket() {
        return market;
    }

    public void setMarket(Long market) {
        this.market = market;
    }


    public String getStockCode() {
        return stockCode;
    }

    public void setStockCode(String stockCode) {
        this.stockCode = stockCode;
    }


    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }


    public String getBondName() {
        return bondName;
    }

    public void setBondName(String bondName) {
        this.bondName = bondName;
    }


    public String getIssueStartDate() {
        return issueStartDate;
    }

    public void setIssueStartDate(String issueStartDate) {
        this.issueStartDate = issueStartDate;
    }


    public String getIssueEndDate() {
        return issueEndDate;
    }

    public void setIssueEndDate(String issueEndDate) {
        this.issueEndDate = issueEndDate;
    }


    public String getListingDate() {
        return listingDate;
    }

    public void setListingDate(String listingDate) {
        this.listingDate = listingDate;
    }


    public double getIssuePrice() {
        return issuePrice;
    }

    public void setIssuePrice(double issuePrice) {
        this.issuePrice = issuePrice;
    }


    public double getFaceValue() {
        return faceValue;
    }

    public void setFaceValue(double faceValue) {
        this.faceValue = faceValue;
    }


    public String getInterestRateType() {
        return interestRateType;
    }

    public void setInterestRateType(String interestRateType) {
        this.interestRateType = interestRateType;
    }


    public String getInterestCalcType() {
        return interestCalcType;
    }

    public void setInterestCalcType(String interestCalcType) {
        this.interestCalcType = interestCalcType;
    }


    public String getInterestPeriod() {
        return interestPeriod;
    }

    public void setInterestPeriod(String interestPeriod) {
        this.interestPeriod = interestPeriod;
    }


    public String getRatingOrg() {
        return ratingOrg;
    }

    public void setRatingOrg(String ratingOrg) {
        this.ratingOrg = ratingOrg;
    }


    public String getInternalBondLevel() {
        return internalBondLevel;
    }

    public void setInternalBondLevel(String internalBondLevel) {
        this.internalBondLevel = internalBondLevel;
    }


    public String getBondLevel() {
        return bondLevel;
    }

    public void setBondLevel(String bondLevel) {
        this.bondLevel = bondLevel;
    }


    public String getIssuerLevel() {
        return issuerLevel;
    }

    public void setIssuerLevel(String issuerLevel) {
        this.issuerLevel = issuerLevel;
    }


    public String getInternalIssuerLevel() {
        return internalIssuerLevel;
    }

    public void setInternalIssuerLevel(String internalIssuerLevel) {
        this.internalIssuerLevel = internalIssuerLevel;
    }


    public String getCreditIssuerLevel() {
        return creditIssuerLevel;
    }

    public void setCreditIssuerLevel(String creditIssuerLevel) {
        this.creditIssuerLevel = creditIssuerLevel;
    }


    public String getIssueRatingOrg() {
        return issueRatingOrg;
    }

    public void setIssueRatingOrg(String issueRatingOrg) {
        this.issueRatingOrg = issueRatingOrg;
    }


    public String getLastIssuerRating() {
        return lastIssuerRating;
    }

    public void setLastIssuerRating(String lastIssuerRating) {
        this.lastIssuerRating = lastIssuerRating;
    }


    public String getIssueName() {
        return issueName;
    }

    public void setIssueName(String issueName) {
        this.issueName = issueName;
    }


    public String getIssueShortName() {
        return issueShortName;
    }

    public void setIssueShortName(String issueShortName) {
        this.issueShortName = issueShortName;
    }


    public String getResearcher() {
        return researcher;
    }

    public void setResearcher(String researcher) {
        this.researcher = researcher;
    }


    public double getFaceRate() {
        return faceRate;
    }

    public void setFaceRate(double faceRate) {
        this.faceRate = faceRate;
    }


    public double getInterestDiff() {
        return interestDiff;
    }

    public void setInterestDiff(double interestDiff) {
        this.interestDiff = interestDiff;
    }


    public String getBenchmarkRate() {
        return benchmarkRate;
    }

    public void setBenchmarkRate(String benchmarkRate) {
        this.benchmarkRate = benchmarkRate;
    }


    public String getEnterpriseType() {
        return enterpriseType;
    }

    public void setEnterpriseType(String enterpriseType) {
        this.enterpriseType = enterpriseType;
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }


    public String getTenderType() {
        return tenderType;
    }

    public void setTenderType(String tenderType) {
        this.tenderType = tenderType;
    }


    public double getSubjectSectionStart() {
        return subjectSectionStart;
    }

    public void setSubjectSectionStart(double subjectSectionStart) {
        this.subjectSectionStart = subjectSectionStart;
    }


    public double getSubjectSectionEnd() {
        return subjectSectionEnd;
    }

    public void setSubjectSectionEnd(double subjectSectionEnd) {
        this.subjectSectionEnd = subjectSectionEnd;
    }


    public String getInviteBeginTime() {
        return inviteBeginTime;
    }

    public void setInviteBeginTime(String inviteBeginTime) {
        this.inviteBeginTime = inviteBeginTime;
    }


    public String getInviteEndTime() {
        return inviteEndTime;
    }

    public void setInviteEndTime(String inviteEndTime) {
        this.inviteEndTime = inviteEndTime;
    }


    public String getAccountManager() {
        return accountManager;
    }

    public void setAccountManager(String accountManager) {
        this.accountManager = accountManager;
    }


    public String getLeadUnderwriter() {
        return leadUnderwriter;
    }

    public void setLeadUnderwriter(String leadUnderwriter) {
        this.leadUnderwriter = leadUnderwriter;
    }


    public String getSubUnderwriter() {
        return subUnderwriter;
    }

    public void setSubUnderwriter(String subUnderwriter) {
        this.subUnderwriter = subUnderwriter;
    }


    public String getDistributionUnderwriter() {
        return distributionUnderwriter;
    }

    public void setDistributionUnderwriter(String distributionUnderwriter) {
        this.distributionUnderwriter = distributionUnderwriter;
    }


    public String getPayTime() {
        return payTime;
    }

    public void setPayTime(String payTime) {
        this.payTime = payTime;
    }


    public String getBondType() {
        return bondType;
    }

    public void setBondType(String bondType) {
        this.bondType = bondType;
    }


    public Long getRated() {
        return rated;
    }

    public void setRated(Long rated) {
        this.rated = rated;
    }


    public Long getWorkflowed() {
        return workflowed;
    }

    public void setWorkflowed(Long workflowed) {
        this.workflowed = workflowed;
    }


    public String getIssueType() {
        return issueType;
    }

    public void setIssueType(String issueType) {
        this.issueType = issueType;
    }


    public Long getRightStatus() {
        return rightStatus;
    }

    public void setRightStatus(Long rightStatus) {
        this.rightStatus = rightStatus;
    }


    public Long getDeleted() {
        return deleted;
    }

    public void setDeleted(Long deleted) {
        this.deleted = deleted;
    }


    public double getBaseIssueAmount() {
        return baseIssueAmount;
    }

    public void setBaseIssueAmount(double baseIssueAmount) {
        this.baseIssueAmount = baseIssueAmount;
    }


    public double getPlanIssueAmount() {
        return planIssueAmount;
    }

    public void setPlanIssueAmount(double planIssueAmount) {
        this.planIssueAmount = planIssueAmount;
    }


    public String getIssuerLevelChange() {
        return issuerLevelChange;
    }

    public void setIssuerLevelChange(String issuerLevelChange) {
        this.issuerLevelChange = issuerLevelChange;
    }


    public String getIssuerLevelProspect() {
        return issuerLevelProspect;
    }

    public void setIssuerLevelProspect(String issuerLevelProspect) {
        this.issuerLevelProspect = issuerLevelProspect;
    }


    public String getCreditIncreaseType() {
        return creditIncreaseType;
    }

    public void setCreditIncreaseType(String creditIncreaseType) {
        this.creditIncreaseType = creditIncreaseType;
    }


    public String getLowestIssuerLevel() {
        return lowestIssuerLevel;
    }

    public void setLowestIssuerLevel(String lowestIssuerLevel) {
        this.lowestIssuerLevel = lowestIssuerLevel;
    }


    public String getIndustryType() {
        return industryType;
    }

    public void setIndustryType(String industryType) {
        this.industryType = industryType;
    }


    public String getIssuerProvince() {
        return issuerProvince;
    }

    public void setIssuerProvince(String issuerProvince) {
        this.issuerProvince = issuerProvince;
    }


    public String getSpecialTerm() {
        return specialTerm;
    }

    public void setSpecialTerm(String specialTerm) {
        this.specialTerm = specialTerm;
    }


    public String getPayOrder() {
        return payOrder;
    }

    public void setPayOrder(String payOrder) {
        this.payOrder = payOrder;
    }


    public String getSubOrMix() {
        return subOrMix;
    }

    public void setSubOrMix(String subOrMix) {
        this.subOrMix = subOrMix;
    }


    public String getCallBack() {
        return callBack;
    }

    public void setCallBack(String callBack) {
        this.callBack = callBack;
    }


    public String getWinningDate() {
        return winningDate;
    }

    public void setWinningDate(String winningDate) {
        this.winningDate = winningDate;
    }


    public String getAnnounceDate() {
        return announceDate;
    }

    public void setAnnounceDate(String announceDate) {
        this.announceDate = announceDate;
    }


    public String getIsListed() {
        return isListed;
    }

    public void setIsListed(String isListed) {
        this.isListed = isListed;
    }


    public String getIsMarketCross() {
        return isMarketCross;
    }

    public void setIsMarketCross(String isMarketCross) {
        this.isMarketCross = isMarketCross;
    }


    public String getGuarantor() {
        return guarantor;
    }

    public void setGuarantor(String guarantor) {
        this.guarantor = guarantor;
    }


    public String getGuarantorLevel() {
        return guarantorLevel;
    }

    public void setGuarantorLevel(String guarantorLevel) {
        this.guarantorLevel = guarantorLevel;
    }


    public String getTenderOrg() {
        return tenderOrg;
    }

    public void setTenderOrg(String tenderOrg) {
        this.tenderOrg = tenderOrg;
    }


    public String getOriginEquity() {
        return originEquity;
    }

    public void setOriginEquity(String originEquity) {
        this.originEquity = originEquity;
    }


    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }


    public String getRaiseType() {
        return raiseType;
    }

    public void setRaiseType(String raiseType) {
        this.raiseType = raiseType;
    }


    public String getIsPandaDebt() {
        return isPandaDebt;
    }

    public void setIsPandaDebt(String isPandaDebt) {
        this.isPandaDebt = isPandaDebt;
    }


    public String getIsOverConfig() {
        return isOverConfig;
    }

    public void setIsOverConfig(String isOverConfig) {
        this.isOverConfig = isOverConfig;
    }


    public String getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(String expireDate) {
        this.expireDate = expireDate;
    }


    public String getInterestStartDate() {
        return interestStartDate;
    }

    public void setInterestStartDate(String interestStartDate) {
        this.interestStartDate = interestStartDate;
    }


    public String getDeadline() {
        return deadline;
    }

    public void setDeadline(String deadline) {
        this.deadline = deadline;
    }


    public String getDeadlineUnit() {
        return deadlineUnit;
    }

    public void setDeadlineUnit(String deadlineUnit) {
        this.deadlineUnit = deadlineUnit;
    }


    public String getSustainable() {
        return sustainable;
    }

    public void setSustainable(String sustainable) {
        this.sustainable = sustainable;
    }


    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }


    public Long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Long createDate) {
        this.createDate = createDate;
    }


    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }


    public String getGuaranteeTerm() {
        return guaranteeTerm;
    }

    public void setGuaranteeTerm(String guaranteeTerm) {
        this.guaranteeTerm = guaranteeTerm;
    }


    public String getBusinessKey() {
        return businessKey;
    }

    public void setBusinessKey(String businessKey) {
        this.businessKey = businessKey;
    }

}
