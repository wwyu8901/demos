package org.wwy.demo.excel.test;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Level;
import org.junit.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class HiveTest {

    private static final String HIVE_DRIVER = "org.apache.hive.jdbc.HiveDriver";


    public static void main(String[] args) {
        BigDecimal bigDecimal = new
                BigDecimal(0.314159);
        BigDecimal multiply = bigDecimal.multiply(BigDecimal.valueOf(100));
        BigDecimal resultValue = multiply.setScale(2, RoundingMode.HALF_UP);
        System.out.println(resultValue+"%");
    }
    @Test
    public void test() throws ClassNotFoundException, SQLException {
        Class.forName(HIVE_DRIVER);
        Connection conn = null;
        try {
            conn = DriverManager.getConnection("jdbc:hive2://192.168.0.171:10000/default");
            System.out.println(conn);
//            conn.getMetaData().getTables(conn.getCatalog(),conn.getSchema(),"%");

        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    //do nothing
                }
            }
        }
    }

    @Test
    public void test4(){
        System.out.println(System.getProperty("user.dir"));
    }

    @Test
    public void test5(){
        Level level;
    }
}
