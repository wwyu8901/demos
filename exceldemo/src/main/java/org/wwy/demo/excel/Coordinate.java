package org.wwy.demo.excel;

public class Coordinate {
    private Integer start;

    private Integer end;

    public Coordinate(Integer start) {
        this.start = start;
    }

    public void setEnd(Integer end) {
        this.end = end;
    }


    public Integer getStart() {
        return start;
    }

    public Integer getEnd() {
        return end;
    }
}
