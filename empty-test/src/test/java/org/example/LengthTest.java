package org.example;

import com.alibaba.fastjson.JSONObject;

import java.util.Arrays;
import java.util.concurrent.CompletableFuture;

public class LengthTest {
    public static void main(String[] args) throws Exception {
        CompletableFuture<String> task1 = CompletableFuture.supplyAsync(() -> {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return "Task 1 Complete";
        });
        CompletableFuture<String> task2 = CompletableFuture.supplyAsync(() -> {
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return "Task 2 Complete";
        });
        CompletableFuture<String> result = task1.thenCombine(task2, (r1,r2) -> String.format("All tasks completed:[%s,%s]",r1,r2));


        System.out.println(result.get());
// 应输出: "All tasks completed: [Task 1 Complete,Task 2 Complete]"
    }
}
