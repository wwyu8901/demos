package org.example;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.alibaba.fastjson.util.IOUtils;
import org.apache.commons.io.FileUtils;
import org.junit.Test;
import com.alibaba.fastjson.parser.ParserConfig;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class JsonPare {
    @Test
    public void test() throws IOException {
        File file = new File("D:\\workspace\\demos\\empty-test\\src\\test\\resources\\1.json");
        String s = FileUtils.readFileToString(file);
        JSONObject.parse(s);

    }

    @Test
    public void test2(){
        ParserConfig.getGlobalInstance().setAutoTypeSupport(true);
        ParserConfig.getGlobalInstance().addAccept("org.example");
        ParserConfig.getGlobalInstance().setSafeMode(false);
//        ParserConfig.getGlobalInstance().addAutoTypeCheckHandler(new XcAutoTypeCheckHandler());
//        String jsonString = "{\"data\":{\"age\":10,\"name\":\"121232\"}}\n";
        String jsonString = "{\"@type\":\"org.example.User\",\"age\":10,\"name\":\"121232\"}";
        Object response= JSON.parseObject(jsonString,Object.class);
        System.out.println(response);

    }

    @Test
    public void test4(){
        ParserConfig.getGlobalInstance().setAutoTypeSupport(true);
        ParserConfig.getGlobalInstance().addAccept("org.example");
        ParserConfig.getGlobalInstance().setSafeMode(false);
//        String jsonString = "{\"data\":{\"age\":10,\"name\":\"121232\"}}\n";
        String jsonString = "{\"@type\":\"org.example.User\",\"age\":10,\"name\":\"121232\"}";
        Object obj= JSON.parseObject(jsonString,Object.class);
        User user = (User) obj;
        System.out.println("name:"+user.getName()+" age:"+user.getAge());

    }

    @Test
    public void test3(){
        User user = new User();
        user.setAge(10);
        user.setName("121232");
        Response<User> userResponse = new Response<>();
        userResponse.setData(user);
        ParserConfig.getGlobalInstance().setSafeMode(true);
        String jsonString = JSON.toJSONString(userResponse,SerializerFeature.WriteClassName);// 这种使用会产生@type
        System.out.println(jsonString);

    }
}
