package org.example;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
 
public class OpenFolderExample {
    public static void main(String[] args) {
        // 指定要打开的文件夹路径
        String folderPath = "D:\\qwfiles\\WXWork\\1688853227890455\\Cache\\File\\2024-06";
 
        // 创建文件夹对象
        File folder = new File(folderPath);
 
        // 检查Desktop是否支持打开URL
        if (!Desktop.isDesktopSupported() || !Desktop.getDesktop().isSupported(Desktop.Action.BROWSE)) {
            System.out.println("Desktop 不支持打开文件夹");
            return;
        }
 
        // 使用Desktop打开文件夹
        Desktop desktop = Desktop.getDesktop();
        try {
            desktop.open(folder);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}