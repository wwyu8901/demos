package org.example;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.security.SecureRandom;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Slf4j
public class MatchTest {

    @Test
    public void test(){
        Pattern r = Pattern.compile("test12.*.xls");
        Matcher m = r.matcher("test123.xls");
        System.out.println(m.matches());
        m = r.matcher("test12.xls");
        System.out.println(m.matches());

    }

    @Test
    public void test2(){
        double v = new SecureRandom().nextDouble();
        System.out.println(v);
        System.out.println(v*9);
        int i = (int) ((v * 9 + 1) * 100000);
        System.out.println(i);
    }


    @Test
    public void test3(){
        String index = "aaaa{abcdef}";
        String patternString = "\\{([^}]*)\\}";
        Pattern pattern = Pattern.compile(patternString);
        Matcher matcher = pattern.matcher(index);
        while (matcher.find()){
            log.info("indexname={}",matcher.group(1));
        }
    }

    @Test
    public void test4() throws IOException {
        String patternString = "([\\S ]*\\s+){2}[\\S ]*(\\s+[-\\d. ]+){5}\\s+([-\\d., ]+)(\\s+[-\\d., ]+){3}[-\\d., ]+";
        System.out.println(patternString);
//        patternString = "[\\S ]*\\s+[\\S ]*\\s+[\\S ]*\\s+[-\\d. ]+\\s+[-\\d. ]+\\s+[-\\d. ]+\\s+[-\\d. ]+\\s+[-\\d., ]+\\s+([-\\d., ]+)\\s+[-\\d., ]+\\s+[-\\d., ]+\\s+[-\\d.,]+[-\\d., ]+";
        File file = new File("D:\\workspace\\demos\\empty-test\\src\\test\\resources\\未结束.txt");
        String s = FileUtils.readFileToString(file);
        log.info("开始计算");
        Pattern pattern = Pattern.compile(patternString);
        Matcher matcher = pattern.matcher(s);
        while (matcher.find()){
            log.info("indexname={}",matcher.group());
        }
    }
}
