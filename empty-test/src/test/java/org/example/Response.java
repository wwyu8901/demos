package org.example;

public class Response<T> {
    private T data;

    // getters and setters
    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Response{data=" + data + '}';
    }
}