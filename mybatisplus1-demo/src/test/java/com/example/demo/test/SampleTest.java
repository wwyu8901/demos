package com.example.demo.test;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.toolkit.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.StopWatch;
import org.wwy.sample.mapper.User;
import org.wwy.sample.mapper.UserMapper;

import javax.sql.DataSource;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SampleTest {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private DataSource datasource;

    @Test
    public void testSelect() {
        System.out.println(("----- selectAll method test ------"));

        List<User> userList = userMapper.selectList(null);
        userList.forEach(System.out::println);
    }


    @Test
    public void testSelect2() {
        System.out.println(("----- selectAll method test ------"));
        Page<User> userPage = new Page<>(2,2);
        List<User> userList = userMapper.selectPageVo(userPage);
        System.out.println(JSON.toJSON(userList));
    }


    @Test
    public void testInsert() {
        System.out.println(("----- selectAll method test ------"));
        User user = new User();
        user.setAge(10);
        user.setEmail("123456");
        user.setId(1L);
        user.setName("abc");
        userMapper.insert(user);
    }





    @Test
    public void testJdbc() throws SQLException {
        Connection connection = datasource.getConnection();
        StopWatch stopWatch = new StopWatch("aaaa");

        try {
            stopWatch.start("111");
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT T_WGHT_APRC_NAV WGHT_APRC_NAV, SECU_ID, T_DATE FROM MID_L2_PRD_AST_DTL WHERE prd_code = ? and t_date>=? and rownum<=10000");
            preparedStatement.setObject(1, "882603");
            preparedStatement.setObject(2, "20180101");
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.setFetchSize(1000);
            stopWatch.stop();
            stopWatch.start("222");
            int count = 0;
            while (resultSet.next()) {
                resultSet.getBigDecimal("WGHT_APRC_NAV");
                count++;
            }
            System.out.println(count);
            stopWatch.stop();
        }finally {
            connection.close();
        }
        System.out.println(stopWatch.getTotalTimeMillis());
        System.out.println(stopWatch.prettyPrint());
    }

    @Test
    public void testWrapper(){
        EntityWrapper w = new EntityWrapper();
        w.like("name","2");
        w.orderBy("name");
        List list = userMapper.selectList(w);
        System.out.println(JSON.toJSON(list));
    }

    public void test3(){

    }

    public static void wrapperIn(Wrapper<?> wrapper, List<?> list, String fieldName) {
        if ( wrapper == null || CollectionUtils.isEmpty(list) || StringUtils.isBlank(fieldName)) {
            return;
        }
        int size = list.size();
        int limit = 1000;
        wrapper.andNew();
        if (size >= limit) {
            int remainder = size % limit;
            int segments = size / limit;
            for (int i = 0; i < segments; i++) {
                wrapper.in(fieldName, list.subList(i * limit, (i + 1) * limit));
                if (i != segments - 1) {
                    wrapper.or();
                }
            }
            if (remainder > 0) {
                wrapper.or().in(fieldName, list.subList(segments * limit, size));
            }
        } else {
            wrapper.in(fieldName, list);
        }
    }


}