package org.wwy.sample.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;

import java.util.List;

public interface UserMapper extends BaseMapper<User> {
    List<User> selectPageVo(Page page);


}